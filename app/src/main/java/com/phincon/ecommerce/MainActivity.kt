package com.phincon.ecommerce

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.navigation.fragment.NavHostFragment
import com.phincon.ecommerce.core.PrefManager
import com.phincon.ecommerce.core.network.model.TransactionModel
import com.phincon.ecommerce.pages.main.home.view_model.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission(),
    ) { isGranted: Boolean ->
        if (isGranted) {
            Toast.makeText(this, "Notifications permission granted", Toast.LENGTH_SHORT)
                .show()
        } else {
            Toast.makeText(
                this,
                "FCM can't post notifications without POST_NOTIFICATIONS permission",
                Toast.LENGTH_LONG,
            ).show()
        }
    }
    private val homeViewModel: HomeViewModel by viewModels()

    private val navHostFragment: NavHostFragment by lazy {
        supportFragmentManager.findFragmentById(R.id.fragment_container_main) as NavHostFragment
    }
    private val navController by lazy {
        navHostFragment.navController
    }

    @Inject
    lateinit var prefManager: PrefManager
    override fun onCreate(savedInstanceState: Bundle?) {
        installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        askNotificationPermission()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = getString(R.string.channel_id)
            val channelName = getString(R.string.channel_name)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(
                NotificationChannel(
                    channelId,
                    channelName,
                    NotificationManager.IMPORTANCE_LOW,
                ),
            )
        }

        val intent = intent
        val id = intent.getExtras()?.get("id").toString()
        if (id == "notification") {
            navigateToNotif()
        }

        homeViewModel.sessionExpired.observe(this@MainActivity) { session ->

            if (session != null && session == true) {
                logout()
                homeViewModel.resetSession()
                homeViewModel.deleteAllData()
                Toast.makeText(this, this.getString(R.string.session_habis), Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    fun logout() {
        navController.navigate(R.id.action_main_to_prelogin)
    }

    fun navigateToProductDetail(idProduct: String) {
        val bundle = Bundle()
        bundle.putString("id_product", idProduct)
        navController.navigate(R.id.action_main_to_product, bundle)
    }

    fun navigateToCart() {
        navController.navigate(R.id.action_main_to_payment)
    }

    fun navigateToNotif() {
        navController.navigate(R.id.action_main_to_notification)
    }

    fun navigateToCheckoutStatus(transactionResponse: TransactionModel, fromTransaction: Boolean) {
        val bundle = Bundle()
        bundle.putBoolean("from_transaction", fromTransaction)
        bundle.putParcelable("transaction_response", transactionResponse)
        navController.navigate(R.id.action_main_to_status, bundle)
    }

    fun navigateDetailToCheckout(checkoutModelList: List<com.phincon.ecommerce.core.network.model.CheckoutModel>) {
        val bundle = Bundle()
        bundle.putParcelableArrayList("list_checkout", ArrayList(checkoutModelList))
        bundle.putBoolean("is_from_detail", true)
        navController.navigate(R.id.action_detail_to_checkout, bundle)
    }

    fun navigateToModular() {
        navController.navigate(R.id.action_to_modular)
    }

    private fun askNotificationPermission() {
//        requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) ==
                PackageManager.PERMISSION_GRANTED
            ) {
            } else {
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }
}
