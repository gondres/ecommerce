package com.phincon.ecommerce.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.bumptech.glide.Glide
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.repository.database_repository.NotificationRepository
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@AndroidEntryPoint
class AppFirebaseMessagingService : FirebaseMessagingService() {

    @Inject
    lateinit var notificationRepository: NotificationRepository

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val data = remoteMessage.data
        val title = remoteMessage.data["title"]
        val body = remoteMessage.data["body"]
        val image = remoteMessage.data["image"]
        val time = remoteMessage.data["time"]
        val date = remoteMessage.data["date"]
        val type = remoteMessage.data["type"]
        Log.d("Messaging Service", data.toString())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                "my_channel_id",
                "My Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        val notificationBody = NotificationEntity(
            title = title,
            time = time,
            body = body,
            date = date,
            image = image,
            read = false,
            type = type,
            id = null,
        )

        runBlocking {
            notificationRepository.insertNotification(notificationBody)
        }

        sendNotification(title!!, body!!, image!!)
    }

    private fun sendNotification(title: String, body: String, image: String) {
        val requestCode = 1000

        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("id", "notification")
        intent.addFlags(
            Intent.FLAG_ACTIVITY_NEW_TASK
                    or Intent.FLAG_ACTIVITY_CLEAR_TASK
        )

//        val pendingIntent = NavDeepLinkBuilder(this)
//            .setGraph(R.navigation.nav_application)
//            .addDestination(R.id.notificationFragment2,null)
//            .createPendingIntent()
        val pendingIntent = PendingIntent.getActivity(
            this,
            requestCode,
            intent,
            PendingIntent.FLAG_IMMUTABLE,
        )

        val channelId = getString(R.string.channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_load_image)
            .setContentTitle(title)
            .setContentText(body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val futureTarget = Glide.with(this)
            .asBitmap()
            .load(image)
            .submit()

        val bitmap = futureTarget.get()
        notificationBuilder.setLargeIcon(bitmap)
        Glide.with(this).clear(futureTarget)
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                getString(R.string.channel_name),
                NotificationManager.IMPORTANCE_DEFAULT,
            )
            notificationManager.createNotificationChannel(channel)
        }

        val notificationId = System.currentTimeMillis().toInt()
        notificationManager.notify(notificationId, notificationBuilder.build())
    }
}
