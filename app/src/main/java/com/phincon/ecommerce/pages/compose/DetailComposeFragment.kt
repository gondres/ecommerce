package com.phincon.ecommerce.pages.compose

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material.icons.filled.Share
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.Button
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.FilterChip
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import coil.compose.AsyncImage
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.toCheckoutModel
import com.phincon.ecommerce.core.network.model.toWishlistEntity
import com.phincon.ecommerce.pages.compose.ui.theme.EcommerceTheme
import com.phincon.ecommerce.pages.main.cart.view_model.CartViewModel
import com.phincon.ecommerce.pages.main.product.view_model.ProductViewModel
import com.phincon.ecommerce.pages.main.wishlist.view_model.WishlistViewModel
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class DetailComposeFragment : Fragment() {

    private val productViewModel: ProductViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private val wishlistViewModel: WishlistViewModel by viewModels()

    private var idProduct: String? = null

    private var isWishlist: Boolean = false

    val args by lazy {
        arguments
    }

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        idProduct = args?.getString("id_product")
        viewLifecycleOwner.lifecycleScope.launch {
            wishlistViewModel.getWishlistById(idProduct!!).collect {
                if (it == null) {
                    isWishlist = false
                } else {
                    isWishlist = true
                }
            }
        }

        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                EcommerceTheme {
                    ContentDetail(
                        productViewModel,
                        wishlistViewModel,
                        cartViewModel,
                        firebaseAnalytics,
                        { navigateBack() },
                        { navigateToBuyerReview() },
                        { list -> buttonBuyNow(list) },
                        { productName, productPrice -> buttonShare(productName, productPrice) },
                        { bundle -> sendLogEventViewItem(bundle)},
                        { bundle -> sendLogEventViewCart(bundle)}
                    )
                }
            }
        }
    }

     fun sendLogEventViewItem(

        bundle: Bundle
    ) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM) {
            param(FirebaseAnalytics.Param.CURRENCY, "IDR")
            param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundle))
        }

    }

    fun sendLogEventViewCart(

        bundle: Bundle
    ) {
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
            param(FirebaseAnalytics.Param.CURRENCY, "IDR")
            param(FirebaseAnalytics.Param.ITEMS, arrayOf(bundle))
        }

    }
    fun navigateBack() {
        findNavController().navigateUp()
    }

    fun navigateToBuyerReview() {
        val bundle = Bundle()
        bundle.putString("id_product", idProduct)

        findNavController().navigate(
            R.id.action_detailComposeFragment_to_ratingBuyerComposeFragment,
            bundle
        )
    }

    fun buttonBuyNow(checkoutModelList: List<com.phincon.ecommerce.core.network.model.CheckoutModel>) {
        (requireActivity() as MainActivity).navigateDetailToCheckout(
            checkoutModelList
        )
    }

    fun buttonShare(productName: String, productPrice: Int) {
        val share: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(
                Intent.EXTRA_TEXT,
                "${productName}\n" + "${formatCurrency(productPrice)}\n" + "http://andre.tokophincon.com/product/$idProduct"
            )
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(share, null)
        startActivity(shareIntent)
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Preview
@Composable
fun ContentDetail(
    productViewModel: ProductViewModel,
    wishlistViewModel: WishlistViewModel,
    cartViewModel: CartViewModel,
    firebaseAnalytics: FirebaseAnalytics,
    navigateBack: () -> Unit,
    navigateToBuyerReview: () -> Unit,
    navigateToBuyNow: (List<com.phincon.ecommerce.core.network.model.CheckoutModel>) -> Unit,
    buttonShare: (String, Int) -> Unit,
    sendLogViewItem: (Bundle) -> Unit,
    sendLogViewItemCart: (Bundle) -> Unit
) {
    val productDetailResponse by productViewModel.responseDetailProduct.observeAsState()
    val productState by productViewModel.stateDetail.observeAsState()

    var checkoutModelList by remember {
        mutableStateOf(
            (mutableListOf<com.phincon.ecommerce.core.network.model.CheckoutModel>())
        )
    }
    var isLoading by remember { mutableStateOf(true) }
    var isSuccess by remember { mutableStateOf(false) }
    var isError by remember { mutableStateOf(false) }
    var selectedVariant by rememberSaveable { mutableStateOf(0) }
    var selectedNameVariant by remember { mutableStateOf("") }
    var selectedPrice by rememberSaveable { mutableStateOf(0) }
    var price by remember { mutableStateOf(0) }
    var isExist by remember { mutableStateOf(false) }
    var stock by remember { mutableStateOf(0) }
    var quantity by remember { mutableStateOf(0) }

    val scope = rememberCoroutineScope()
    val snackbarHostState = remember { SnackbarHostState() }
    val context = LocalContext.current

    productState.let { state ->
        when (state) {
            is MainStateEvent.Loading -> {
                isLoading = true
            }

            is MainStateEvent.Success -> {
                isLoading = false
                isSuccess = true
            }

            is MainStateEvent.Error -> {
                isError = true
            }

            is MainStateEvent.Exception -> {
            }

            else -> {}
        }
    }

    Scaffold(
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        },
        topBar = {
            Column {
                TopAppBar(
                    title = {
                        Text(
                            text = stringResource(id = R.string.detail_produk),
                            fontFamily = FontFamily(
                                Font(R.font.poppins_regular_400)
                            )
                        )
                    },
                    navigationIcon = {
                        IconButton(onClick = { navigateBack() }) {
                            Icon(Icons.Filled.ArrowBack, "backIcon")
                        }
                    },
                )
                Divider()
            }
        },
        bottomBar = {
            if (isSuccess) {
                productDetailResponse.let { product ->
                    if (product != null) {
                        Divider()
                        Row(
                            modifier = Modifier.padding(8.dp),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            OutlinedButton(
                                onClick = {
                                    val itemProduct = Bundle().apply {
                                        putString(FirebaseAnalytics.Param.ITEM_ID, product.data.productId)
                                        putString(
                                            FirebaseAnalytics.Param.ITEM_NAME,
                                            product.data.productName
                                        )
                                        putString(
                                            FirebaseAnalytics.Param.ITEM_VARIANT,
                                            selectedNameVariant
                                        )
                                        putString(FirebaseAnalytics.Param.ITEM_BRAND, product.data.brand)
                                        putDouble(
                                            FirebaseAnalytics.Param.PRICE,
                                            (price).toDouble()
                                        )
                                    }
                                    sendLogViewItem(itemProduct)
                                    var product = product.data.copy(
                                        productPrice = price,

                                        productVariant = product.data.productVariant.map { variant ->
                                            if (selectedNameVariant.isEmpty()) {
                                                selectedNameVariant =
                                                    product.data.productVariant.firstOrNull()?.variantName
                                                        ?: ""
                                            } else {
                                                selectedNameVariant
                                            }
                                            variant.copy(variantName = selectedNameVariant)
                                        }
                                    ).toCheckoutModel()
                                    checkoutModelList.add(product)
                                    navigateToBuyNow(checkoutModelList)
                                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
                                        param(FirebaseAnalytics.Param.CURRENCY, "IDR")
                                    }
                                },
                                modifier = Modifier.width(180.dp)
                            ) { Text(text = stringResource(R.string.beli_lansung)) }
                            Spacer(modifier = Modifier.padding(5.dp))
                            val cartState by cartViewModel.getProductById(productId = product.data.productId)
                                .collectAsState(
                                    initial = null
                                )
                            cartState.let { cartEntity ->
                                if (cartEntity != null) {
                                    isExist = true
                                    stock = cartEntity.stock
                                    quantity = cartEntity.quantity
                                }
                                Button(
                                    onClick = {
                                        if (isExist) {
                                            if (stock > quantity) {
                                                quantity = quantity + 1
                                                cartViewModel.updateQuantity(
                                                    product.data.productId,
                                                    quantity
                                                )

                                                scope.launch {
                                                    snackbarHostState.showSnackbar(
                                                        context.resources.getString(
                                                            R.string.berhasil_tambah_produk
                                                        )
                                                    )
                                                }
                                            } else {
                                                scope.launch {
                                                    snackbarHostState.showSnackbar(
                                                        context.resources.getString(
                                                            R.string.stok_habis
                                                        )
                                                    )
                                                }
                                            }
                                        }
                                        val itemProduct = Bundle().apply {
                                            putString(FirebaseAnalytics.Param.ITEM_ID, product.data.productId)
                                            putString(
                                                FirebaseAnalytics.Param.ITEM_NAME,
                                                product.data.productName
                                            )
                                            putString(
                                                FirebaseAnalytics.Param.ITEM_VARIANT,
                                                selectedNameVariant
                                            )
                                            putString(FirebaseAnalytics.Param.ITEM_BRAND, product.data.brand)
                                            putDouble(
                                                FirebaseAnalytics.Param.PRICE,
                                                (price).toDouble()
                                            )
                                        }

                                        val body = product.data.copy(
                                            productPrice = price ?: 0,
                                            productVariant = product.data.productVariant.map { variant ->
                                                if (selectedNameVariant.isEmpty()) {
                                                    selectedNameVariant =
                                                        product.data.productVariant.firstOrNull()?.variantName
                                                            ?: ""
                                                } else {
                                                    selectedNameVariant
                                                }
                                                variant.copy(variantName = selectedNameVariant)
                                            }
                                        )
                                        if (!isExist) {
                                            cartViewModel.insertToCart(body)
                                            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
                                                param(FirebaseAnalytics.Param.CURRENCY, "IDR")
                                            }
                                            sendLogViewItemCart(itemProduct)
                                            scope.launch {
                                                snackbarHostState.showSnackbar(
                                                    context.resources.getString(
                                                        R.string.berhasil_keranjang
                                                    )
                                                )
                                            }
                                        }
                                    },

                                    modifier = Modifier.fillMaxWidth()
                                ) { Text(text = stringResource(R.string.keranjang)) }
                            }
                        }
                    }
                }
            }
        }

    ) { paddingValues ->
        if (isLoading) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(paddingValues),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                CircularProgressIndicator()
            }
        }
        if (isSuccess) {
            productDetailResponse.let { product ->
                if (product != null) {
                    price = product.data.productPrice + selectedPrice
                    Column(
                        modifier = Modifier
                            .padding(paddingValues = paddingValues)
                            .fillMaxSize()
                            .verticalScroll(rememberScrollState())

                    ) {
                        ViewPager(product.data.image)
                        Column(
                            modifier = Modifier.padding(start = 16.dp, end = 16.dp)
                        ) {
                            val wishlistState by wishlistViewModel.getWishlistById(product.data.productId)
                                .collectAsState(initial = null)
                            wishlistState.let {
                                RowPrice(
                                    wishlistEntity = product.data.toWishlistEntity(),
                                    price = (price),
                                    checkedState = it != null,
                                    product = productDetailResponse!!.data,
                                    wishlistViewModel = wishlistViewModel,
                                    funcShare = buttonShare,
                                    firebaseAnalytics = firebaseAnalytics,
                                    scope = scope,
                                    snackbarHostState = snackbarHostState
                                )
                            }

                            TextTitleProduct(text = product.data.productName)
                            RowSales(
                                product.data.sale,
                                product.data.productRating,
                                product.data.totalSatisfaction
                            )
                        }
                        Divider(modifier = Modifier.padding(top = 12.dp))
                        Column(
                            modifier = Modifier.padding(
                                start = 16.dp,
                                end = 16.dp,
                                top = 12.dp
                            )
                        ) {
                            TextTitle(text = stringResource(id = R.string.pilih_varian))
                            LazyRow {
                                items(product.data.productVariant.size) {
                                    FilterChip(
                                        modifier = Modifier.padding(end = 5.dp),
                                        selected = if (it == selectedVariant) true else false,
                                        onClick = {
                                            selectedVariant = it
                                            selectedNameVariant =
                                                product.data.productVariant[it].variantName
                                            selectedPrice =
                                                product.data.productVariant[it].variantPrice
                                            Log.d("Detail Compose", selectedNameVariant)
                                        },
                                        label = { Text(product.data.productVariant[it].variantName) }
                                    )
                                }
                            }
                        }
                        Divider(modifier = Modifier.padding(top = 12.dp))
                        Column(
                            modifier = Modifier.padding(
                                start = 16.dp,
                                end = 16.dp,
                                top = 12.dp
                            )
                        ) {
                            TextTitle(text = stringResource(id = R.string.deskripsi_produk))
                            TextDescription(text = product.data.description)
                        }
                        Divider(modifier = Modifier.padding(top = 12.dp))
                        BuyerReviewContent(
                            totalSatisfaction = product.data.totalSatisfaction,
                            productRating = product.data.productRating,
                            rating = product.data.totalRating,
                            review = product.data.totalReview,
                            navigateToBuyerReview
                        )
                    }
                }
            }
        }

        if (isError) {
            ErrorState()
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalFoundationApi::class)
@Composable
fun ViewPager(image: List<String>) {
    Box(
        modifier = Modifier
            .background(Color.White),
    ) {
        val pageCount = image.size
        val pagerState = rememberPagerState()
        HorizontalPager(
            state = pagerState,
            pageCount = pageCount

        ) { page ->
            // Our page content
            AsyncImage(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(309.dp),
                model = image[page],
                placeholder = painterResource(id = R.drawable.ic_load_image),
                contentDescription = "$page"
            )
        }
        Box(
            Modifier
                .align(Alignment.BottomCenter)
                .fillMaxWidth()
        ) {
            Row(
                Modifier
                    .height(50.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                repeat(pageCount) { iteration ->
                    val color =
                        if (pagerState.currentPage == iteration) MaterialTheme.colorScheme.primary else Color.LightGray
                    Box(
                        modifier = Modifier
                            .padding(2.dp)
                            .clip(CircleShape)
                            .background(color)
                            .size(8.dp)

                    )
                }
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun RowPrice(
    product: ProductDetailResponse.Data,
    wishlistEntity: WishlistEntity,
    price: Int,
    checkedState: Boolean,
    wishlistViewModel: WishlistViewModel,
    funcShare: (String, Int) -> Unit,
    firebaseAnalytics: FirebaseAnalytics,
    scope : CoroutineScope,
    snackbarHostState: SnackbarHostState
) {
    val context = LocalContext.current

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 12.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(
            text = formatCurrency(price),
            fontWeight = FontWeight.Normal,
            fontSize = 20.sp,
            fontFamily = FontFamily(
                Font(R.font.poppins_semibold_600)
            )
        )
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.End,

            ) {
            IconButton(
                onClick = {
                    funcShare(product.productName, product.productPrice)
                },
            ) {
                Icon(
                    imageVector = Icons.Default.Share,
                    contentDescription = "",
                )
            }
            IconButton(
                onClick = {
                    if (checkedState == false) {
                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_WISHLIST) {
                            param(FirebaseAnalytics.Param.ITEMS, product.toString())
                        }
                        val body = product.copy(
                            productPrice = price
                        )
                        wishlistViewModel.insertToWishlist(body)
                        scope.launch {
                            snackbarHostState.showSnackbar(
                                context.resources.getString(
                                    R.string.berhasil_favorite
                                )
                            )
                        }
                    } else {
                        wishlistViewModel.deleteWishlist(wishlistEntity)
                        scope.launch {
                            snackbarHostState.showSnackbar(
                                context.resources.getString(
                                    R.string.hapus_favorite
                                )
                            )
                        }
                    }
                },

                ) {
                Icon(
                    tint = if (checkedState) MaterialTheme.colorScheme.primary else MaterialTheme.colorScheme.onSurfaceVariant,
                    imageVector = if (checkedState) Icons.Default.Favorite else Icons.Default.FavoriteBorder,
                    contentDescription = "",

                    )
            }
        }
    }
}

@Composable
fun RowSales(sales: Int, rate: Double, satisfaction: Int) {
    Row(
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        TextLight(text = "${stringResource(id = R.string.terjual)} $sales")
        Row(
            modifier = Modifier
                .wrapContentWidth()
                .wrapContentHeight()
                .padding(top = 8.dp, start = 8.dp)
                .border(
                    1.dp,
                    MaterialTheme.colorScheme.onSurface,
                    shape = RoundedCornerShape(4.dp)
                ),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_star),
                contentDescription = "Star Icon",
                modifier = Modifier
                    .size(24.dp)
                    .padding(start = 4.dp, end = 4.dp)
            )
            Text(
                text = "$rate ($satisfaction)",
                fontSize = 12.sp,
                fontWeight = FontWeight.Normal,
                modifier = Modifier.padding(top = 2.dp, bottom = 2.dp, end = 8.dp),
            )
        }
    }
}

@Composable
fun TextTitleProduct(text: String) {
    Text(
        modifier = Modifier.padding(top = 8.dp),
        text = text,
        fontFamily = FontFamily(
            Font(R.font.poppins_regular_400)
        ),
        fontSize = 14.sp,
        maxLines = 2
    )
}

@Composable
fun TextRating(text: String) {
    Text(
        text = text,
        fontWeight = FontWeight.SemiBold,
        fontSize = 20.sp,
        maxLines = 2
    )
}

@Composable
fun TextDescription(text: String) {
    Text(
        modifier = Modifier.padding(top = 8.dp),
        text = text,
        fontSize = 14.sp,
        fontFamily = FontFamily(
            Font(R.font.poppins_regular_400)
        )
    )
}

@Composable
fun TextLight(text: String) {
    Text(
        modifier = Modifier.padding(top = 8.dp),
        text = text,
        fontFamily = FontFamily(
            Font(R.font.poppins_regular_400)
        ),
        fontSize = 12.sp,
        maxLines = 2
    )
}

@Composable
fun BuyerReviewContent(
    totalSatisfaction: Int,
    productRating: Double,
    rating: Int,
    review: Int,
    navigateToBuyerReview: () -> Unit
) {
    Column(
        modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 6.dp)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            TextTitle(text = stringResource(id = R.string.ulasan_pembeli))
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                horizontalArrangement = Arrangement.End
            ) {
                TextButton(onClick = { navigateToBuyerReview() }) {
                    Text(
                        text = stringResource(id = R.string.lihat_semua),
                    )
                }
            }
        }
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(2.dp)
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Icon(
                    imageVector = Icons.Default.Star,
                    contentDescription = null,
                    modifier = Modifier
                        .size(20.dp)
                        .padding(top = 0.dp),
                )
                TextRating(text = "$productRating")
                Text(
                    "/5.0",
                    modifier = Modifier.padding(top = 5.dp),
                    fontSize = 10.sp,
                    fontWeight = FontWeight.W400
                )
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 32.dp),

                    ) {
                    Text(
                        text = "$totalSatisfaction% ${stringResource(id = R.string.pembeli_merasa)}",
                        fontWeight = FontWeight.Medium,
                        fontSize = 12.sp,
                    )
                    Text(
                        text = "$rating ${stringResource(id = R.string.rating)} . $review ${
                            stringResource(
                                id = R.string.ulasan
                            )
                        }",
                        fontWeight = FontWeight.Light,
                        fontSize = 12.sp,
                    )
                }
            }
        }
    }
}

@Composable
fun TextTitle(text: String) {
    Text(
        text = text,
        fontFamily = FontFamily(
            Font(R.font.poppins_medium_500)
        ),
        fontSize = 16.sp,
    )
}

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@OptIn(ExperimentalMaterial3Api::class)
@Composable
@Preview
fun ErrorState() {
    Column(

        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_state),
            contentDescription = "image error"
        )
        Text(
            stringResource(id = R.string.kosong),
            fontFamily = FontFamily(
                Font(R.font.poppins_medium_500)
            ),
            fontSize = 32.sp
        )
        Text(
            stringResource(id = R.string.data_kosong),
            fontFamily = FontFamily(
                Font(R.font.poppins_regular_400)
            ),
            fontSize = 16.sp
        )
        FilledTonalButton(onClick = { /*TODO*/ }) {
            Text(text = stringResource(id = R.string.muat_ulang))
        }
    }


}

