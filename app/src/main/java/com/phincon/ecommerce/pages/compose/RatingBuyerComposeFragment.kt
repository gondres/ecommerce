package com.phincon.ecommerce.pages.compose

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import coil.compose.AsyncImage
import com.phincon.ecommerce.R
import com.phincon.ecommerce.pages.compose.ui.theme.EcommerceTheme
import com.phincon.ecommerce.pages.main.product.view_model.ProductViewModel
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RatingBuyerComposeFragment : Fragment() {

    private val viewModel: ProductViewModel by viewModels()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val args = arguments
        val argValue = args?.getString("id_product")

        viewModel.getReviewProduct(argValue!!)

        return ComposeView(requireContext()).apply {
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                EcommerceTheme {
                    ContentRating(viewModel, { findNavController().navigateUp() })
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview
@Composable
fun ContentRating(viewModel: ProductViewModel, navigationBack: () -> Unit) {
    val ratingResponse by viewModel.responseReviewProduct.observeAsState()
    val reviewState by viewModel.stateDetail.observeAsState()
    var isLoading by remember { mutableStateOf(true) }
    var isSuccess by remember { mutableStateOf(false) }
    var isError by remember { mutableStateOf(false) }

    reviewState.let { state ->
        when (state) {
            is MainStateEvent.Loading -> {
                isLoading = true
            }

            is MainStateEvent.Success -> {
                isLoading = false
                isSuccess = true
            }

            is MainStateEvent.Error -> {
                isError = true
            }

            is MainStateEvent.Exception -> {
            }

            else -> {}
        }
    }

    Scaffold(topBar = {
        Column {
            TopAppBar(
                title = {
                    Text(
                        text = stringResource(R.string.ulasan_pembeli),
                        fontFamily = FontFamily(
                            Font(R.font.poppins_regular_400)
                        )
                    )
                },
                navigationIcon = {
                    IconButton(onClick = { navigationBack() }) {
                        Icon(Icons.Filled.ArrowBack, "backIcon")
                    }
                }

            )
            Divider()
        }
    }) {
        if (isLoading) {
            LoadingState()
        }
        if (isSuccess) {
            ratingResponse?.data.let { response ->
                if (response != null) {
                    Column(modifier = Modifier.padding(it)) {
                        Column {
                            LazyColumn {
                                items(ratingResponse?.data!!.size) {
                                    Row(
                                        modifier = Modifier.padding(
                                            start = 16.dp,
                                            end = 16.dp,
                                            top = 16.dp
                                        ),
                                        horizontalArrangement = Arrangement.Center
                                    ) {
                                        AsyncImage(
                                            modifier = Modifier
                                                .size(40.dp)
                                                .clip(CircleShape), // clip to the circle shape ,
                                            model = ratingResponse!!.data[it].userImage,
                                            placeholder = painterResource(id = R.drawable.ic_load_image),
                                            contentDescription = ""
                                        )
                                        Column(modifier = Modifier.padding(start = 8.dp)) {
                                            Text(
                                                text = ratingResponse!!.data[it].userName,
                                                fontSize = 12.sp,
                                                fontFamily = FontFamily(
                                                    Font(R.font.poppins_semibold_600)
                                                )
                                            )
                                            LazyRow {
                                                items(5) { index ->
                                                    if (index < ratingResponse!!.data[it].userRating) {
                                                        Image(
                                                            painter = painterResource(id = R.drawable.ic_star),
                                                            contentDescription = ""
                                                        )
                                                    } else {
                                                        Image(
                                                            painter = painterResource(id = R.drawable.ic_star_outline),
                                                            contentDescription = ""
                                                        )
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    Text(
                                        modifier = Modifier.padding(
                                            start = 16.dp,
                                            end = 16.dp,
                                            top = 8.dp
                                        ),
                                        text = ratingResponse!!.data[it].userReview,
                                        fontSize = 12.sp,
                                        fontFamily = FontFamily(
                                            Font(R.font.poppins_regular_400)
                                        )
                                    )
                                    Divider(modifier = Modifier.padding(top = 16.dp))
                                }
                            }
                        }
                    }
                }
            }
        }
    }

//    ratingResponse?.data.let {
//        if (it != null) {
//
//            Scaffold(topBar = {
//                TopAppBar(title = {
//                    Text(
//                        text = "Ulasan Pembeli", fontFamily = FontFamily(
//                            Font(R.font.poppins_regular_400)
//                        )
//                    )
//                }, navigationIcon = {
//                    IconButton(onClick = { navigationBack() }) {
//                        Icon(Icons.Filled.ArrowBack, "backIcon")
//                    }
//                }
//
//                )
//            }, content = {
//
//                Column(modifier = Modifier.padding(it)) {
//                    Column(
//
//                    ) {
//                        Divider()
//                        LazyColumn {
//                            items(ratingResponse?.data!!.size) {
//                                Row(
//                                    modifier = Modifier.padding(
//                                        start = 16.dp,
//                                        end = 16.dp,
//                                        top = 16.dp
//                                    ),
//                                    horizontalArrangement = Arrangement.Center
//                                ) {
//                                    AsyncImage(
//                                        modifier = Modifier
//                                            .size(40.dp)
//                                            .clip(CircleShape),                   // clip to the circle shape ,
//                                        model = ratingResponse!!.data[it].userImage,
//                                        placeholder = painterResource(id = R.drawable.ic_load_image),
//                                        contentDescription = ""
//                                    )
//                                    Column(modifier = Modifier.padding(start = 8.dp)) {
//
//                                        Text(
//                                            text = ratingResponse!!.data[it].userName,
//                                            fontSize = 12.sp,
//                                            fontFamily = FontFamily(
//                                                Font(R.font.poppins_semibold_600)
//                                            )
//                                        )
//                                        LazyRow {
//                                            items(5) { index ->
//                                                if (index < ratingResponse!!.data[it].userRating) {
//                                                    Image(
//                                                        painter = painterResource(id = R.drawable.ic_star),
//                                                        contentDescription = ""
//                                                    )
//                                                } else {
//                                                    Image(
//                                                        painter = painterResource(id = R.drawable.ic_star_outline),
//                                                        contentDescription = ""
//                                                    )
//                                                }
//
//                                            }
//                                        }
//                                    }
//                                }
//                                Text(
//                                    modifier = Modifier.padding(
//                                        start = 16.dp,
//                                        end = 16.dp,
//                                        top = 8.dp
//                                    ),
//                                    text = ratingResponse!!.data[it].userReview,
//                                    fontSize = 12.sp, fontFamily = FontFamily(
//                                        Font(R.font.poppins_regular_400)
//                                    )
//                                )
//                                Divider(modifier = Modifier.padding(top = 16.dp))
//                            }
//                        }
//
//                    }
//
//                }
//
//            }
//
//            )
//        }
//    }
}

@Composable
fun LoadingState() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator(
            color = MaterialTheme.colorScheme.primary,
        )
    }
}
