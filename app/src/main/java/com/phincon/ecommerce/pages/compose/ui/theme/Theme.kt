package com.phincon.ecommerce.pages.compose.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

private val DarkColorScheme = darkColorScheme(
    primary = Color(0xFFF7F2FA),
    secondary = Color(0xFFF7F2FA),
)

private val LightColorScheme = lightColorScheme(
    background = Color.White,
    surface = Color.White,
)

@Composable
fun EcommerceTheme(
    content: @Composable () -> Unit,
) {
    val isDarkTheme: Boolean = isSystemInDarkTheme()
    val colorScheme = if (isDarkTheme) DarkColorScheme else LightColorScheme

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = content
    )
}
