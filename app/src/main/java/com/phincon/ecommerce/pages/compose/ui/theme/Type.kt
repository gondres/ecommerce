package com.phincon.ecommerce.pages.compose.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import com.phincon.ecommerce.R

val poppinsFontFamily = FontFamily(
    Font(R.font.poppins_medium_500),
)

val appTyphoGraphy = Typography()
val Typography = Typography(
    bodyLarge = appTyphoGraphy.bodyLarge.copy(fontFamily = poppinsFontFamily),
    bodyMedium = appTyphoGraphy.bodyMedium.copy(fontFamily = poppinsFontFamily),
    bodySmall = appTyphoGraphy.bodySmall.copy(fontFamily = poppinsFontFamily),
    /* Other default text styles to override
    titleLarge = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 28.sp,
        letterSpacing = 0.sp
    ),
    labelSmall = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Medium,
        fontSize = 11.sp,
        lineHeight = 16.sp,
        letterSpacing = 0.5.sp
    )
     */
)
