package com.phincon.ecommerce.pages.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.badge.BadgeUtils
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.android.material.navigationrail.NavigationRailView
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.FragmentMainBinding
import com.phincon.ecommerce.pages.main.cart.view_model.CartViewModel
import com.phincon.ecommerce.pages.main.notification.view_model.NotificationViewModel
import com.phincon.ecommerce.pages.main.wishlist.view_model.WishlistViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var prefManager: com.phincon.ecommerce.core.PrefManager

    private val userName: String by lazy {
        prefManager.getUsername()
    }

    private val cartViewModel: CartViewModel by viewModels()
    private val wishlistViewModel: WishlistViewModel by viewModels()
    private val notifViewModel: NotificationViewModel by viewModels()

    private val navHostFragment: NavHostFragment by lazy {
        childFragmentManager.findFragmentById(R.id.nav_main_container) as NavHostFragment
    }
    private val navController by lazy {
        navHostFragment.navController
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkUser()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        val view = binding.root

        binding.topAppBar.title = userName

        binding.topAppBar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.notification -> {
                    (requireActivity() as MainActivity).navigateToNotif()
                    true
                }

                R.id.cart -> {
                    (requireActivity() as MainActivity).navigateToCart()
                    true
                }

                R.id.menu -> {
                    (requireActivity() as MainActivity).navigateToModular()
                    true
                }

                else -> false
            }
        }

        val navView: BottomNavigationView? = binding.bottomNav
        val railView: NavigationRailView? = binding.navigationRail
        val navigationView: NavigationView? = binding.navigationView
        railView?.setupWithNavController(navController)
        navView?.setupWithNavController(navController)
        navigationView?.setupWithNavController(navController)
        navView?.setOnItemReselectedListener {}

        var badge = navView?.getOrCreateBadge(R.id.wishlistFragment)
        var badgeRailView = railView?.getOrCreateBadge(R.id.wishlistFragment)
        wishlistViewModel.totalData().observe(viewLifecycleOwner) {
            if (it == 0) {
                badge?.isVisible = false
                badgeRailView?.isVisible = false
            } else {
                badge?.isVisible = true
                badge?.number = it
                badgeRailView?.isVisible = true
                badgeRailView?.number = it
            }
        }

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val badgesCart = context?.let { BadgeDrawable.create(it) }
        badgesCart?.let {
            BadgeUtils.attachBadgeDrawable(it, binding.topAppBar, R.id.cart)
        }
        cartViewModel.totalData().observe(viewLifecycleOwner) {
            if (it == 0) {
                badgesCart?.isVisible = false
            } else {
                badgesCart?.isVisible = true
                badgesCart?.number = it
            }
        }

        val badgesNotif = context?.let { BadgeDrawable.create(it) }
        badgesNotif?.let {
            BadgeUtils.attachBadgeDrawable(it, binding.topAppBar, R.id.notification)
        }
        notifViewModel.totalData().observe(viewLifecycleOwner) {
            if (it == 0) {
                badgesNotif?.isVisible = false
            } else {
                badgesNotif?.isVisible = true
                badgesNotif?.number = it
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun checkUser() {
        val isLoggedIn = prefManager.getIsFirstLogin()
        val token = prefManager.getAccessToken()
        val username = prefManager.getUsername()
        if (isLoggedIn) {
            if (username.isEmpty()) return findNavController().navigate(R.id.action_main_to_prelogin)
        } else {
            return findNavController().navigate(R.id.action_main_to_prelogin)
        }
    }
}
