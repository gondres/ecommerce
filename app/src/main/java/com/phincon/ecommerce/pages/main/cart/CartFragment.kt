package com.phincon.ecommerce.pages.main.cart

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.core.database.entity.toCheckoutModel
import com.phincon.ecommerce.databinding.FragmentCartBinding
import com.phincon.ecommerce.pages.main.cart.adapter.CartAdapter
import com.phincon.ecommerce.pages.main.cart.view_model.CartViewModel
import com.phincon.ecommerce.pages.main.checkout.view_model.CheckoutViewModel
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.click_listener.CartItemClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class CartFragment : Fragment(), CartItemClickListener {

    private var _binding: FragmentCartBinding? = null
    private val binding get() = _binding!!

    private val example: MutableList<com.phincon.ecommerce.core.network.model.ExampleCartModel> =
        mutableListOf()
    val adapter = CartAdapter(this)
    var totalCheckout: Int = 0
    private val viewModel: CartViewModel by viewModels()
    private val checkoutViewModel: CheckoutViewModel by viewModels()
    private var checkoutModelList: MutableList<com.phincon.ecommerce.core.network.model.CheckoutModel> =
        mutableListOf()

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCartBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
        setupEvent()
        setupAdapter()

        setFragmentResultListener("on_back") { requestKey, bundle ->
            if (requestKey == "on_back") {
                checkoutViewModel.resetListCheckout()
                checkoutModelList.clear()
                Log.d("Cart Fragment", "On Back")
            }
        }
    }

    private fun setupAdapter() {
        binding.rvCart.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvCart.adapter = adapter
    }

    private fun setupObserver() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.fetchCart().collectLatest {
                adapter.submitList(it)

                val productSelected = it.filter { it.isChecked }
                val isAnySelected = it.any { it.isChecked }
                val isAllSelected = it.all { it.isChecked }

                binding.errorState.isVisible = it.isEmpty()
                binding.buttonBottomContainer.isVisible = it.isNotEmpty()
                binding.dividerSecond.isVisible = it.isNotEmpty()
                binding.textButtonHapus.isVisible = it.isNotEmpty()
                binding.checkBoxAll.isVisible = it.isNotEmpty()

                totalCheckout = productSelected.sumOf { cartEntity ->
                    cartEntity.productPrice * cartEntity.quantity
                }

                binding.buttonBuy.setOnClickListener { view ->
                    it.forEachIndexed { index, cartEntity ->
                        if (!checkoutModelList.contains(cartEntity.toCheckoutModel()) && cartEntity.isChecked) {
                            checkoutModelList.add(
                                cartEntity.toCheckoutModel()
                            )
                        }
                        if (checkoutModelList.contains(cartEntity.toCheckoutModel()) && !cartEntity.isChecked) {
                            checkoutModelList.remove(
                                cartEntity.toCheckoutModel()
                            )
                        }
                    }
                    val bundle = Bundle()
                    bundle.putParcelableArrayList("list_checkout", ArrayList(checkoutModelList))
                    bundle.putInt("total_checkout", totalCheckout)
                    findNavController().navigate(
                        R.id.action_cartFragment_to_checkoutFragment,
                        bundle
                    )
                }

                if (isAnySelected) {
                    binding.textButtonHapus.visibility = View.VISIBLE
                } else {
                    binding.textButtonHapus.visibility = View.INVISIBLE
                }

                if (!it.isEmpty()) {
                    binding.checkBoxAll.isChecked = isAllSelected
                } else {
                    binding.checkBoxAll.isChecked = !isAllSelected
                }

                binding.checkBoxAll.setOnClickListener {
                    var data = adapter.currentList.map {
                        it.isChecked = binding.checkBoxAll.isChecked
                        it
                    }
                    viewModel.updateAll(data)
                    adapter.notifyDataSetChanged()
                }

                binding.textButtonHapus.setOnClickListener { view ->
                    for (item in it) {
                        if (item.isChecked) {
                            viewModel.deleteCart(item)
                        }
                    }
                }
                if (totalCheckout > 0) {
                    binding.tvTotalProduct.text = formatCurrency(totalCheckout)
                    binding.buttonBuy.isEnabled = true
                } else {
                    binding.tvTotalProduct.text = "Rp. 0"
                    binding.buttonBuy.isEnabled = false
                }
            }
        }
    }

    private fun setupEvent() {
        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    override fun deleteCart(cartEntity: CartEntity, position: Int) {
        viewModel.deleteCart(cartEntity)
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.REMOVE_FROM_CART) {
            param(FirebaseAnalytics.Param.ITEMS, cartEntity.toString())
        }
    }

    override fun updateChecked(cartEntity: CartEntity, isChecked: Boolean) {
        cartEntity.isChecked = isChecked
        viewModel.updateSingle(cartEntity)
    }

    override fun updateQuantity(productId: String, quantity: Int) {
        viewModel.updateQuantity(productId, quantity)
    }

    override fun navigateToProductDetail(id: String) {
        (requireActivity() as MainActivity).navigateToProductDetail(id)
    }
}
