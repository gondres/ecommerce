package com.phincon.ecommerce.pages.main.cart.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.databinding.CardCartBinding
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.click_listener.CartItemClickListener

class CartAdapter(private val itemClickListener: CartItemClickListener) :
    ListAdapter<CartEntity, CartAdapter.ItemViewHolder>(
        ItemDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            CardCartBinding.inflate(inflater, parent, false) // Replace with your item layout
        return ItemViewHolder(binding, itemClickListener, parent.context)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
            itemClickListener.navigateToProductDetail(getItem(position).productId)
        }
    }

    class ItemViewHolder(
        private val binding: CardCartBinding,
        private val itemClickListener: CartItemClickListener,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: CartEntity) {
            binding.tvTitleProduct.text = item.productName
            binding.tvPrice.text = formatCurrency(item.productPrice)
            binding.itemCheckbox.isChecked = item.isChecked
            binding.tvVariant.text = item.productVariant
            binding.itemCart.tvTotal.text = "${item.quantity}"

            if (item.stock < 10) {
                binding.tvStock.setTextColor(Color.RED)
                binding.tvStock.text = "${context.getString(R.string.sisa)} ${item.stock}"
            } else {
                binding.tvStock.text = "${context.getString(R.string.stok)} ${item.stock}"
            }
            binding.itemCheckbox.setOnCheckedChangeListener { compoundButton, isChecked ->
                itemClickListener.updateChecked(item, isChecked)
            }
            binding.ivDelete.setOnClickListener {
                itemClickListener.deleteCart(item, position)
            }
            binding.itemCart.ivPlus.setOnClickListener {
                if (item.stock > item.quantity) {
                    item.quantity++
                    binding.itemCart.tvTotal.text = "${item.quantity}"
                    itemClickListener.updateQuantity(item.productId, item.quantity)
                }
            }
            binding.itemCart.ivMinus.setOnClickListener {
                if (item.quantity > 1) {
                    item.quantity--
                    binding.itemCart.tvTotal.text = "${item.quantity}"
                    itemClickListener.updateQuantity(item.productId, item.quantity)
                }
            }

            Glide.with(itemView.context).load(item.image).placeholder(R.drawable.ic_load_image)
                .timeout(10000).into(binding.ivProduct)
        }
    }

    class ItemDiffCallback : DiffUtil.ItemCallback<CartEntity>() {
        override fun areItemsTheSame(oldItem: CartEntity, newItem: CartEntity): Boolean {
            return oldItem.productId == newItem.productId
        }

        override fun areContentsTheSame(oldItem: CartEntity, newItem: CartEntity): Boolean {
            return oldItem.productId == newItem.productId
        }
    }
}
