package com.phincon.ecommerce.pages.main.cart.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.repository.database_repository.CartRepository
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.phincon.ecommerce.core.network.model.ProductDetailResponse.Data as ProductDetail

@HiltViewModel
class CartViewModel @Inject constructor(
    private val cartRepository: CartRepository
) : ViewModel() {

    private val _cartState = MutableLiveData<DatabaseStateEvent>()
    private val _cartEntity = MutableLiveData<CartEntity>()
    val cartState: LiveData<DatabaseStateEvent> = _cartState
    val cartEntity: LiveData<CartEntity> = _cartEntity

    fun totalData() = cartRepository.getCountOfCartItems()
    fun fetchCart() = cartRepository.getCartList()
    fun getProductById(productId: String) = cartRepository.getProductById(productId)

    suspend fun getProduct(productId: String) = cartRepository.insertFromWishlist(productId = productId)

    fun insertToCart(productDetail: ProductDetail) {
        _cartState.value = DatabaseStateEvent.Loading
        viewModelScope.launch {
            val result = cartRepository.insertCart(productDetail)
            _cartState.value = result
        }
    }

    fun insertCartWishlist(cartEntity: CartEntity) {

        viewModelScope.launch {
            cartRepository.insertCartFromWishlist(cartEntity)

        }
    }
    fun insertToCartFromWishlist(cartEntity: CartEntity) {

        _cartState.value = DatabaseStateEvent.Loading
        viewModelScope.launch {
          val result = cartRepository.insertCartFromWishlist(cartEntity)
            _cartState.value = result
        }
    }

    fun updateAll(cartEntity: List<CartEntity>) {
        viewModelScope.launch {
            cartRepository.updateAll(cartEntity)
        }
    }

    fun updateSingle(cartEntity: CartEntity) {
        viewModelScope.launch {
            cartRepository.updateSingle(cartEntity)
        }
    }

    fun deleteCart(cartEntity: CartEntity) {
        viewModelScope.launch {
            cartRepository.deleteCart(cartEntity)
        }
    }

    fun updateQuantity(productId: String, quantity: Int) {
        _cartState.value = DatabaseStateEvent.Loading
        viewModelScope.launch {
            val result = cartRepository.updateQuantity(productId, quantity)
            _cartState.value = result
        }
    }
}
