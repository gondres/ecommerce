package com.phincon.ecommerce.pages.main.checkout

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.network.model.TransactionBody
import com.phincon.ecommerce.core.network.model.TransactionModel
import com.phincon.ecommerce.core.network.model.toTransactionData
import com.phincon.ecommerce.databinding.FragmentCheckoutBinding
import com.phincon.ecommerce.pages.main.checkout.adapter.CheckoutListAdapter
import com.phincon.ecommerce.pages.main.checkout.view_model.CheckoutViewModel
import com.phincon.ecommerce.utils.SnackBar
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.click_listener.CheckoutListener
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CheckoutFragment : Fragment(), CheckoutListener {

    private var _binding: FragmentCheckoutBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CheckoutViewModel by viewModels()

    private val adapter = CheckoutListAdapter(this)

    var imageValue: String = ""
    var paymentNameValue: String = ""
    var itemList = mutableListOf<TransactionBody.Item>()
    var total = 0
    val args by lazy {
        arguments
    }

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCheckoutBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupEvent()
        selectedCheckout()
        selectedPaymentMethod()
        setupObserver()
        isFromDetail()
    }

    private fun isFromDetail() {
        var fromDetail = args?.getBoolean("is_from_detail")
        binding.cardChoosePayment.setOnClickListener {
            if (fromDetail == true) {
                findNavController().navigate(R.id.action_checkoutFragmentDetail_to_paymentListFragmentDetail)
            } else {
                findNavController().navigate(R.id.action_checkoutFragment_to_paymentListFragment)
            }
        }
    }

    private fun setupObserver() {
        itemList.clear()
        viewModel.stateTransaction.observe(viewLifecycleOwner) { state ->
            when (state) {
                is MainStateEvent.Loading -> {
                    binding.buttonBuy.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                }

                is MainStateEvent.Success -> {
                    binding.progressBar.visibility = View.GONE
                    var transactionResponse: TransactionModel
                    val bundle = Bundle()

                    viewModel.responseTransaction.observe(viewLifecycleOwner) {
                        transactionResponse = TransactionModel(
                            code = it.code,
                            message = it.message,
                            data = it.data.toTransactionData()
                        )

                        bundle.putParcelable("transaction_response", transactionResponse)
                        (requireActivity() as MainActivity).navigateToCheckoutStatus(
                            transactionResponse,
                            false
                        )
                    }
                }

                is MainStateEvent.Error -> {
                    binding.buttonBuy.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    SnackBar.showSnackBar(requireView(), state.message)
                }

                is MainStateEvent.Exception -> {
                    binding.buttonBuy.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    SnackBar.showSnackBar(requireView(), state.errorMessage)
                }
            }
        }
    }

    private fun selectedCheckout() {
        if (args != null) {
            val checkoutModelList =
                args!!.getParcelableArrayList<com.phincon.ecommerce.core.network.model.CheckoutModel>(
                    "list_checkout"
                )
            if (checkoutModelList != null) {
                adapter.submitList(checkoutModelList)
                viewModel._checkout.value = checkoutModelList
            }
        }

        viewModel.checkout.observe(viewLifecycleOwner) { list ->

            list.forEachIndexed { index, checkoutModel ->
                Log.d("Checkout List", "$index, ${list.size}, $checkoutModel")
                itemList.add(
                    TransactionBody.Item(
                        productId = checkoutModel.productId,
                        variantName = checkoutModel.productVariant,
                        quantity = checkoutModel.quantity
                    )
                )
            }

            total = list.sumOf { checkout ->
                checkout.productPrice * checkout.quantity
            }

            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT) {
                param(FirebaseAnalytics.Param.VALUE, total.toString())
            }
            binding.tvTotalProduct.text = formatCurrency(total)
        }

        binding.rvListCheckout.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvListCheckout.adapter = adapter
    }

    private fun selectedPaymentMethod() {
        binding.buttonBuy.isEnabled = false
        setFragmentResultListener("payment_method") { requestKey, bundle ->
            if (requestKey == "payment_method") {
                paymentNameValue = bundle.getString("payment_name").toString()
                imageValue = bundle.getString("image").toString()
                Log.d("Fragment Result", paymentNameValue + imageValue)
                binding.tvPaymentMethod.text = paymentNameValue
                if (paymentNameValue.equals("OVO") || paymentNameValue.equals("GoPay")) {
                    Glide.with(requireContext()).load(imageValue)
                        .placeholder(R.drawable.ic_load_image)
                        .apply(RequestOptions.circleCropTransform()).into(binding.ivPaymentImage)
                    binding.ivPaymentImage.layoutParams.width = 64
                    binding.ivPaymentImage.layoutParams.height = 64
                } else {
                    Glide.with(requireContext()).load(imageValue)
                        .placeholder(R.drawable.ic_load_image).timeout(10000)
                        .into(binding.ivPaymentImage)
                    binding.ivPaymentImage.layoutParams.width = 94
                    binding.ivPaymentImage.layoutParams.height = 32
                }
                binding.buttonBuy.isEnabled = true
            }
        }
    }

    private fun setupEvent() {
        binding.topAppBar.setNavigationOnClickListener {
            val bundle = Bundle()
            setFragmentResult("on_back", bundle)
            findNavController().navigateUp()
//            viewModel.resetListCheckout()
        }

        binding.buttonBuy.setOnClickListener {
            val body = TransactionBody(
                payment = paymentNameValue,
                items = itemList
            )
            Log.d("Body Payment", body.toString())
            viewModel.postTransaction(body)
            firebaseAnalytics.logEvent(FirebaseAnalytics.Event.PURCHASE) {
                param(FirebaseAnalytics.Param.ITEMS, itemList.toString())
                param(FirebaseAnalytics.Param.VALUE, total.toString())
                param(FirebaseAnalytics.Param.CURRENCY, "IDR")
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun sendPaymentMethod(image: String, paymentName: String) {
    }

    override fun updateTotalCheckout(id: String, quantity: Int) {
        itemList.clear()
        val updatedList = viewModel._checkout.value?.toMutableList() ?: mutableListOf()
        updatedList.find { it.productId == id }?.quantity = quantity
        viewModel._checkout.value = updatedList
    }
}
