package com.phincon.ecommerce.pages.main.checkout

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.network.model.RatingBody
import com.phincon.ecommerce.core.network.model.TransactionModel
import com.phincon.ecommerce.databinding.FragmentCheckoutStatusBinding
import com.phincon.ecommerce.pages.main.checkout.view_model.CheckoutViewModel
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CheckoutStatusFragment : Fragment() {

    private var _binding: FragmentCheckoutStatusBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CheckoutViewModel by viewModels()

    val args by lazy {
        arguments
    }

    var invoiceId: String = ""
    var ratingValue: Int = 0
    var review: String = ""
    var fromTransaction: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCheckoutStatusBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Log.d("Checkout Status", "On Back Pressed")
                val body = RatingBody(
                    rating = ratingValue,
                    invoiceId = invoiceId,
                    review = review
                )
                viewModel.postRatingTransaction(body)
                if (!fromTransaction) {
                    findNavController().navigate(R.id.action_prelogin_to_main)
                } else {
                    findNavController().navigateUp()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
        binding.ratingProduct.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            if (fromUser) {
                ratingValue = rating.toInt()
            }
        }

        if (args != null) {
            val transactionResponse = args!!.getParcelable<TransactionModel>("transaction_response")
            fromTransaction = args!!.getBoolean("from_transaction")

            if (transactionResponse != null) {
                var status = ""
                invoiceId = transactionResponse.data.invoiceId

                if (transactionResponse.data.status == true) {
                    status = context?.getString(R.string.status_berhasil) ?: ""
                } else {
                    status = context?.getString(R.string.status_gagal) ?: ""
                }

                binding.apply {
                    tvId.text = transactionResponse.data.invoiceId
                    tvDate.text = transactionResponse.data.date
                    tvPaymentMethod.text = transactionResponse.data.payment
                    tvTotalPrice.text = transactionResponse.data.total.let { formatCurrency(it) }
                    tvStatus.text = status

                    if (transactionResponse.data.rating != null) {
                        ratingValue = transactionResponse.data.rating!!
                        ratingProduct.rating = transactionResponse.data.rating!!.toFloat()
                    }
                    if (!transactionResponse.data.review.isNullOrEmpty()) {
                        textInput.setText(transactionResponse.data.review)
                        textInput.isEnabled = false
                    }

                    buttonBuy.setOnClickListener {
                        review = textInput.text.toString()
                        val body = RatingBody(
                            rating = ratingValue,
                            invoiceId = invoiceId,
                            review = review
                        )
                        viewModel.postRatingTransaction(body)
                    }
                }
            }
        }

        viewModel.stateRating.observe(viewLifecycleOwner) { state ->
            when (state) {
                is MainStateEvent.Loading -> {
                    binding.buttonBuy.visibility = View.INVISIBLE
                    binding.progressBar.visibility = View.VISIBLE
                    binding.textField.isEnabled = false
                }

                is MainStateEvent.Success -> {
                    if (fromTransaction) {
                        findNavController().navigateUp()
                    } else {
                        findNavController().navigate(R.id.action_prelogin_to_main)
                    }
                }

                is MainStateEvent.Error -> {
                    binding.buttonBuy.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    binding.textField.isEnabled = true
                }

                is MainStateEvent.Exception -> {
                    binding.buttonBuy.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                    binding.textField.isEnabled = true
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
