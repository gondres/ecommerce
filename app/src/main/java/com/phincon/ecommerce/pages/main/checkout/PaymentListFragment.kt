package com.phincon.ecommerce.pages.main.checkout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.phincon.ecommerce.databinding.FragmentNestedPaymentListBinding
import com.phincon.ecommerce.pages.main.checkout.adapter.BankAdapter
import com.phincon.ecommerce.pages.main.checkout.adapter.VirtualAccountAdapter
import com.phincon.ecommerce.pages.main.checkout.adapter.payment_list.InstantPaymentAdapter
import com.phincon.ecommerce.pages.main.checkout.adapter.payment_list.InstantPaymentAdapterNested
import com.phincon.ecommerce.pages.main.checkout.view_model.CheckoutViewModel
import com.phincon.ecommerce.utils.click_listener.CheckoutListener
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class PaymentListFragment : Fragment(), CheckoutListener {

    private var _binding: FragmentNestedPaymentListBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CheckoutViewModel by viewModels()

    private val adapter = InstantPaymentAdapterNested(this)
    private val virtualAccountAdapter = VirtualAccountAdapter(this)
    private val bankAdapter = BankAdapter(this)
    private val instantPaymentAdapter = InstantPaymentAdapter(this)

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNestedPaymentListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAdapter()
        setupObserver()

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun setupObserver() {
        viewModel.stateTransaction.observe(viewLifecycleOwner) { state ->
            when (state) {
                is MainStateEvent.Loading -> {
                }

                is MainStateEvent.Success -> {
                }

                is MainStateEvent.Error -> {
                }

                is MainStateEvent.Exception -> {
                }
            }
        }

        viewModel.fetchPaymentList(requireActivity())
        binding.progressBar.visibility = View.VISIBLE
        viewModel.responsePaymentList.observe(viewLifecycleOwner) {
            adapter.submitList(it.data)
            if (!it.data.isEmpty()) {
                binding.progressBar.visibility = View.GONE
            }
        }
    }

    private fun setupAdapter() {
        binding.rvParent.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun sendPaymentMethod(image: String, paymentName: String) {
        val bundle = Bundle()
        bundle.putString("image", image)
        bundle.putString("payment_name", paymentName)
        setFragmentResult("payment_method", bundle)
        findNavController().navigateUp()
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_PAYMENT_INFO) {
            param(FirebaseAnalytics.Param.PAYMENT_TYPE, paymentName)
        }
    }

    override fun updateTotalCheckout(id: String, quantity: Int) {
    }
}
