package com.phincon.ecommerce.pages.main.checkout.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.CardCheckoutBinding
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.click_listener.CheckoutListener

class CheckoutListAdapter(private val itemClickListener: CheckoutListener) :
    ListAdapter<com.phincon.ecommerce.core.network.model.CheckoutModel, CheckoutListAdapter.ItemViewHolder>(
        ItemDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            CardCheckoutBinding.inflate(inflater, parent, false) // Replace with your item layout
        return ItemViewHolder(binding, itemClickListener, parent.context)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
        }
    }

    class ItemViewHolder(
        private val binding: CardCheckoutBinding,
        private val itemClickListener: CheckoutListener,
        val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: com.phincon.ecommerce.core.network.model.CheckoutModel) {
            binding.tvTitleProduct.text = item.productName
            binding.tvPrice.text = formatCurrency(item.productPrice)

            binding.tvVariant.text = item.productVariant
            binding.itemCart.tvTotal.text = "${item.quantity}"

            if (item.stock < 10) {
                binding.tvStock.setTextColor(Color.RED)
                binding.tvStock.text = "${context.getString(R.string.sisa)} ${item.stock}"
            } else {
                binding.tvStock.text = "${context.getString(R.string.stok)} ${item.stock}"
            }

            binding.itemCart.ivPlus.setOnClickListener {
                if (item.stock > item.quantity) {
                    item.quantity++
                    binding.itemCart.tvTotal.text = "${item.quantity}"

                    itemClickListener.updateTotalCheckout(item.productId, item.quantity)
                }
            }
            binding.itemCart.ivMinus.setOnClickListener {
                if (item.quantity > 1) {
                    item.quantity--
                    binding.itemCart.tvTotal.text = "${item.quantity}"

                    itemClickListener.updateTotalCheckout(item.productId, item.quantity)
                }
            }

            Glide.with(itemView.context).load(item.image).placeholder(R.drawable.ic_load_image)
                .timeout(10000).into(binding.ivProduct)
        }
    }

    class ItemDiffCallback :
        DiffUtil.ItemCallback<com.phincon.ecommerce.core.network.model.CheckoutModel>() {
        override fun areItemsTheSame(
            oldItem: com.phincon.ecommerce.core.network.model.CheckoutModel,
            newItem: com.phincon.ecommerce.core.network.model.CheckoutModel
        ): Boolean {
            return oldItem.productId == newItem.productId
        }

        override fun areContentsTheSame(
            oldItem: com.phincon.ecommerce.core.network.model.CheckoutModel,
            newItem: com.phincon.ecommerce.core.network.model.CheckoutModel
        ): Boolean {
            return oldItem.productId == newItem.productId
        }
    }
}
