package com.phincon.ecommerce.pages.main.checkout.adapter.payment_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.CardInstantPaymentListBinding
import com.phincon.ecommerce.utils.click_listener.CheckoutListener
import com.phincon.ecommerce.core.network.model.PaymentListResponse.Data.Item as Data

class InstantPaymentAdapter(private val itemClickListener: CheckoutListener) :
    ListAdapter<Data, InstantPaymentAdapter.ItemViewHolder>(
        ItemDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            CardInstantPaymentListBinding.inflate(
                inflater,
                parent,
                false
            ) // Replace with your item layout
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
            itemClickListener.sendPaymentMethod(getItem(position).image, getItem(position).label)
        }
    }

    class ItemViewHolder(private val binding: CardInstantPaymentListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Data) {
            binding.tvName.text = item.label
            Glide.with(itemView.context).load(item.image).placeholder(R.drawable.ic_load_image)
                .apply(RequestOptions.circleCropTransform())
                .timeout(10000).into(binding.ivLogoPayment)
        }
    }

    class ItemDiffCallback : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }
    }
}
