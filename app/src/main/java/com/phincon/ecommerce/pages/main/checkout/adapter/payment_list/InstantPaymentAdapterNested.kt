package com.phincon.ecommerce.pages.main.checkout.adapter.payment_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.phincon.ecommerce.core.network.model.PaymentListResponse.Data
import com.phincon.ecommerce.databinding.CardNestedPaymentListBinding
import com.phincon.ecommerce.utils.click_listener.CheckoutListener

class InstantPaymentAdapterNested(private val itemClickListener: CheckoutListener) :
    ListAdapter<Data, InstantPaymentAdapterNested.ItemViewHolder>(
        ItemDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            CardNestedPaymentListBinding.inflate(
                inflater,
                parent,
                false
            ) // Replace with your item layout
        return ItemViewHolder(binding, itemClickListener)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
//            itemClickListener.sendPaymentMethod(getItem(position).image,getItem(position).label)
        }
    }

    class ItemViewHolder(
        private val binding: CardNestedPaymentListBinding,
        private val itemClickListener: CheckoutListener
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Data) {
            binding.tvTitle.text = item.title
            val adapter = InstantPaymentAdapterNestedChild(itemClickListener)
            adapter.submitList(item.item)
            binding.rvChild.adapter = adapter
        }
    }

    class ItemDiffCallback : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }
    }
}
