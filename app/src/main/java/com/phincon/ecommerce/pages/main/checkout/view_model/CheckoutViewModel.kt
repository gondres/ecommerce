package com.phincon.ecommerce.pages.main.checkout.view_model

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.remoteconfig.ConfigUpdate
import com.google.firebase.remoteconfig.ConfigUpdateListener
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigException
import com.phincon.ecommerce.core.network.model.RatingBody
import com.phincon.ecommerce.core.network.model.RatingResponse
import com.phincon.ecommerce.core.network.model.TransactionBody
import com.phincon.ecommerce.core.network.model.TransactionResponse
import com.phincon.ecommerce.repository.service_repository.CheckoutRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CheckoutViewModel @Inject constructor(
    private val repository: CheckoutRepository,
    private val firebaseRemotConfig: FirebaseRemoteConfig
) :
    ViewModel() {

    private val _responsePaymentList =
        MutableLiveData<com.phincon.ecommerce.core.network.model.PaymentListResponse>()
    private val _responseTransaction = MutableLiveData<TransactionResponse>()
    private val _responseRating = MutableLiveData<RatingResponse>()
    private val _stateTransaction = MutableLiveData<MainStateEvent<TransactionResponse>>()
    private val _stateRating = MutableLiveData<MainStateEvent<RatingResponse>>()
    val _checkout = MutableLiveData<List<com.phincon.ecommerce.core.network.model.CheckoutModel>>()

    val responsePaymentList: LiveData<com.phincon.ecommerce.core.network.model.PaymentListResponse> =
        _responsePaymentList
    val responseTransaction: LiveData<TransactionResponse> = _responseTransaction
    val responseRating: LiveData<RatingResponse> = _responseRating
    val stateTransaction: LiveData<MainStateEvent<TransactionResponse>> = _stateTransaction
    val stateRating: LiveData<MainStateEvent<RatingResponse>> = _stateRating
    val checkout: LiveData<List<com.phincon.ecommerce.core.network.model.CheckoutModel>> = _checkout

    fun resetListCheckout() {
        _checkout.value = kotlin.collections.emptyList()
    }

    fun fetchPaymentList(activity: Activity) {
        firebaseRemotConfig.fetchAndActivate()
            .addOnCompleteListener(activity) { task ->
                if (task.isSuccessful) {
                    val response = firebaseRemotConfig.getString("payment_method")
                    val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                    val jsonAdapter = moshi.adapter(
                        com.phincon.ecommerce.core.network.model.PaymentListResponse::class.java
                    )
                    val paymentListResponse = jsonAdapter.fromJson(response)
                    _responsePaymentList.value = paymentListResponse!!
                    Log.d("Checkout view model", response)
                } else {
                    val exampleValue = firebaseRemotConfig.getString("payment_method")
                }
            }
        firebaseRemotConfig.addOnConfigUpdateListener(object : ConfigUpdateListener {
            override fun onUpdate(configUpdate: ConfigUpdate) {
                Log.d("Checkout view model", "Updated keys: " + configUpdate.updatedKeys)

                if (configUpdate.updatedKeys.contains("payment_method")) {
                    firebaseRemotConfig.activate().addOnCompleteListener {
                        val response = firebaseRemotConfig.getString("payment_method")
                        val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
                        val jsonAdapter = moshi.adapter(
                            com.phincon.ecommerce.core.network.model.PaymentListResponse::class.java
                        )
                        val paymentListResponse = jsonAdapter.fromJson(response)
                        _responsePaymentList.value = paymentListResponse!!
                        Log.d("Checkout view model", response)
                    }
                }
            }

            override fun onError(error: FirebaseRemoteConfigException) {
                Log.w("Checkout view model", "Config update error with code: " + error.code, error)
            }
        })
    }

    fun postTransaction(body: TransactionBody) {
        _stateTransaction.value = MainStateEvent.Loading(true)
        viewModelScope.launch {
            val result = repository.postTransaction(body)
            when (result) {
                is MainStateEvent.Loading -> {
                }

                is MainStateEvent.Success -> {
                    _responseTransaction.value = result.data!!
                }

                is MainStateEvent.Error -> {
                }

                is MainStateEvent.Exception -> {
                }
            }
            _stateTransaction.value = result
        }
    }

    fun postRatingTransaction(body: RatingBody) {
        _stateRating.value = MainStateEvent.Loading(true)
        viewModelScope.launch {
            val result = repository.postRatingTransaction(body)
            when (result) {
                is MainStateEvent.Loading -> {
                }

                is MainStateEvent.Success -> {
                    _responseRating.value = result.data!!
                }

                is MainStateEvent.Error -> {
                }

                is MainStateEvent.Exception -> {
                }
            }
            _stateRating.value = result
        }
    }
}
