package com.phincon.ecommerce.pages.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.LocaleListCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.databinding.FragmentHomeBinding
import com.phincon.ecommerce.pages.main.home.view_model.HomeViewModel
import com.phincon.ecommerce.pages.main.store.view_model.StoreViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val homeViewModel: HomeViewModel by viewModels()
    private val storeViewModel: StoreViewModel by activityViewModels()

    @Inject
    lateinit var prefManager: com.phincon.ecommerce.core.PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val view = binding.root

        val themeApplication = prefManager.getThemeApplication()

        binding.switchTheme.isChecked = themeApplication
        if (AppCompatDelegate.getApplicationLocales().get(0)?.language != null) {
            binding.switchLanguage.isChecked =
                AppCompatDelegate.getApplicationLocales().get(0)!!.language == "in"
        }

        if (themeApplication) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }

        binding.switchLanguage.apply {
            setOnClickListener {
                val locale = if (binding.switchLanguage.isChecked) "in" else "en"
                val appLocale: LocaleListCompat = LocaleListCompat.forLanguageTags(locale)
                AppCompatDelegate.setApplicationLocales(appLocale)
            }
        }

        binding.switchTheme.setOnCheckedChangeListener { compoundButton, switch ->
            if (switch) {
                prefManager.setThemeApplication(true)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                prefManager.setThemeApplication(false)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonLogout.setOnClickListener {
            (requireActivity() as MainActivity).logout()
            homeViewModel.deleteAllData()
            storeViewModel.resetChips()
            storeViewModel.resetQuery()
        }
    }

    override fun onResume() {
        super.onResume()
        if (AppCompatDelegate.getApplicationLocales().get(0)?.language != null) {
            binding.switchLanguage.isChecked =
                AppCompatDelegate.getApplicationLocales().get(0)!!.language == "in"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
