package com.phincon.ecommerce.pages.main.home.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.module.SessionManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val prefManager: com.phincon.ecommerce.core.PrefManager,
    private val sessionManager: SessionManager,
    private val appDatabase: AppDatabase
) : ViewModel() {

    val sessionExpired: LiveData<Boolean?> = sessionManager.tokenExpired

    fun resetSession() {
        sessionManager.resetSession()
    }

    fun deleteAllData() {
        prefManager.deletePref()
        GlobalScope.launch {
            appDatabase.clearAllTables()
        }
    }
}
