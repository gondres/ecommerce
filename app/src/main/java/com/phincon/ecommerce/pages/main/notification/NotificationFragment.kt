package com.phincon.ecommerce.pages.main.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.databinding.FragmentNotificationBinding
import com.phincon.ecommerce.pages.main.notification.adapter.NotificationAdapter
import com.phincon.ecommerce.pages.main.notification.view_model.NotificationViewModel
import com.phincon.ecommerce.utils.click_listener.NotificationClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class NotificationFragment : Fragment(), NotificationClickListener {

    private var _binding: FragmentNotificationBinding? = null
    private val binding get() = _binding!!
    private val viewModel: NotificationViewModel by viewModels()
    val adapter = NotificationAdapter(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNotificationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvNotif.adapter = adapter

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.fetchNotificationlist().collect {
                if (it.isEmpty()) {
                    binding.rvNotif.visibility = View.GONE
                    binding.errorState.errorState.visibility = View.VISIBLE
                    binding.errorState.tvStatusCode.text =
                        requireContext().getString(R.string.kosong)
                    binding.errorState.tvStatusMessage.text =
                        requireContext().getString(R.string.data_kosong)
                    binding.errorState.buttonErrorState.visibility = View.GONE
                }
                adapter.submitList(it)
            }
        }
    }

    override fun updateNotification(notifcationEntity: NotificationEntity) {
        val body = notifcationEntity.copy(
            read = true
        )
        viewModel.updateNotification(body)
    }
}
