package com.phincon.ecommerce.pages.main.notification.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.databinding.CardNotificationNewBinding
import com.phincon.ecommerce.utils.click_listener.NotificationClickListener

class NotificationAdapter(private val itemClickListener: NotificationClickListener) :
    ListAdapter<NotificationEntity, NotificationAdapter.ItemViewHolder>(
        ItemDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            CardNotificationNewBinding.inflate(
                inflater,
                parent,
                false
            ) // Replace with your item layout
        return ItemViewHolder(binding, itemClickListener)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class ItemViewHolder(
        private val binding: CardNotificationNewBinding,
        private val itemClickListener: NotificationClickListener
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NotificationEntity) {
            binding.tvTitleNotif.text = item.title
            binding.tvBody.text = item.body
            binding.tvType.text = item.type
            binding.tvDate.text = "${item.date}, ${item.time}"
            binding.backgroundLayout.setOnClickListener {
                if (item.read == false) {
                    itemClickListener.updateNotification(item)
                    binding.backgroundLayout.setBackgroundResource(R.color.white)
                }
            }
            if (item.read == false) {
                binding.backgroundLayout.setBackgroundResource(R.color.purple)
            }
            if (item.read == true) {
                binding.backgroundLayout.setBackgroundResource(R.color.white)
            }
            Glide.with(itemView.context).load(item.image).placeholder(R.drawable.ic_load_image)
                .timeout(10000).into(binding.ivNotifImage)
        }
    }

    class ItemDiffCallback : DiffUtil.ItemCallback<NotificationEntity>() {
        override fun areItemsTheSame(
            oldItem: NotificationEntity,
            newItem: NotificationEntity
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: NotificationEntity,
            newItem: NotificationEntity
        ): Boolean {
            return oldItem.id == newItem.id
        }
    }
}
