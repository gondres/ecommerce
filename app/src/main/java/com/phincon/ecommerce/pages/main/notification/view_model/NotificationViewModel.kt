package com.phincon.ecommerce.pages.main.notification.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.repository.database_repository.NotificationRepository
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(private val notificationRepository: NotificationRepository) :
    ViewModel() {

    private val _state = MutableLiveData<DatabaseStateEvent>()
    val state: LiveData<DatabaseStateEvent> = _state

    fun totalData() = notificationRepository.getCountOfNotification()
    fun fetchNotificationlist() = notificationRepository.getNotifList()
    fun updateNotification(notificationEntity: NotificationEntity) {
        _state.value = DatabaseStateEvent.Loading
        viewModelScope.launch {
            val result = notificationRepository.updateNotification(notificationEntity)
            _state.value = result
        }
    }
}
