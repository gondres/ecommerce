package com.phincon.ecommerce.pages.main.product

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.chip.Chip
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.network.model.toCheckoutModel
import com.phincon.ecommerce.core.network.model.toWishlistEntity
import com.phincon.ecommerce.databinding.FragmentProductDetailBinding
import com.phincon.ecommerce.pages.main.cart.view_model.CartViewModel
import com.phincon.ecommerce.pages.main.product.adapter.ProductImageAdapter
import com.phincon.ecommerce.pages.main.product.view_model.ProductViewModel
import com.phincon.ecommerce.pages.main.wishlist.view_model.WishlistViewModel
import com.phincon.ecommerce.utils.SnackBar.showSnackBar
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import com.phincon.ecommerce.core.network.model.ProductDetailResponse.Data as ProductDetail

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {

    private var _binding: FragmentProductDetailBinding? = null
    private val binding get() = _binding!!

    private val productViewModel: ProductViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()
    private val wishlistViewModel: WishlistViewModel by viewModels()

    private val imageList = mutableListOf<String>()
    private val priceList = mutableListOf<Int>()
    private val productList = mutableListOf<String>()
    private var checkoutModelList =
        mutableListOf<com.phincon.ecommerce.core.network.model.CheckoutModel>()

    private var idProduct: String? = null
    private var basePrice: Int? = null
    private var price: Int? = null
    private var selectedVariant: String? = null

    private var isExist: Boolean = false
    private var stock: Int = 0
    private var quantity: Int = 0

    lateinit var productDetail: ProductDetail

    val args by lazy {
        arguments
    }

    val wishlistEntity by lazy {
        productDetail.toWishlistEntity()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        idProduct = args?.getString("id_product")
        productViewModel.getDetailProduct(idProduct ?: "")

        setupObserver()
        setupChipGroup()
        setupStateEvent()

        binding.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.textButtonReview.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("id_product", idProduct)
            imageList.clear()
            productList.clear()
            priceList.clear()

            findNavController().navigate(
                R.id.action_productDetailFragment_to_reviewBuyerFragment,
                bundle
            )
        }

        binding.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.buttonErrorState.setOnClickListener {
            productViewModel.getDetailProduct(idProduct ?: "")
        }

        viewLifecycleOwner.lifecycleScope.launch {
            wishlistViewModel.getWishlistById(idProduct!!).collect {
                if (it == null) {
                    binding.favorite.isChecked = false
                } else {
                    binding.favorite.isChecked = true
                }
            }
        }

        binding.favorite.setOnClickListener {
            if (binding.favorite.isChecked) {
                wishlistViewModel.insertToWishlist(productDetail)
            } else {
                wishlistViewModel.deleteWishlist(wishlistEntity)
            }
        }

        cartViewModel.cartState.observe(viewLifecycleOwner) { state ->
            when (state) {
                is DatabaseStateEvent.Loading -> {
                }

                is DatabaseStateEvent.Success -> {
                    val message = state.data.toString()
                    showSnackBar(view, message)
                }

                is DatabaseStateEvent.Error -> {
                    val message = state.message
                    if (message.contains("PRIMARYKEY")) {
                        showSnackBar(view, message)
                    }
                }

                else -> {}
            }
        }
    }

    private fun setupStateEvent() {
        productViewModel.stateDetail.observe(viewLifecycleOwner) { state ->
            binding.progressBar.isVisible = true
            when (state) {
                is MainStateEvent.Loading -> {
                    binding.containerFragment.visibility = View.GONE
                    binding.buttonBottomContainer.visibility = View.GONE
                }

                is MainStateEvent.Success -> {
                    binding.progressBar.isVisible = false
                    binding.containerFragment.visibility = View.VISIBLE
                    binding.buttonBottomContainer.visibility = View.VISIBLE
                    binding.errorState.visibility = View.GONE
                }

                is MainStateEvent.Error -> {
                    binding.tvStatusCode.text = state.statusCode
                    binding.tvStatusMessage.text = state.message
                    binding.errorState.visibility = View.VISIBLE
                }

                is MainStateEvent.Exception -> {
                    binding.errorState.visibility = View.VISIBLE
                    binding.tvStatusCode.text = "Error"
                    binding.tvStatusMessage.text = "Unknown Error"
                    binding.buttonErrorState.text = "Refresh"
                    binding.buttonErrorState.setOnClickListener {
                        productViewModel.getDetailProduct(idProduct ?: "")
                    }
                }
            }
            binding.progressBar.isVisible = false
        }
    }

    private fun setupChipGroup() {
        binding.chipGroupVariant.isSingleSelection = true
        binding.chipGroupVariant.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == View.NO_ID) {
            } else {
                val chip: Chip = group.findViewById(checkedId)
                if (chip.isChecked) {
                    selectedVariant = chip.text.toString()
                    for (i in 0 until group.childCount) {
                        if (chip.text == productList[i]) {
                            price = basePrice?.plus(priceList[i]) ?: 0
                            binding.tvPriceProduct.text = formatCurrency(price!!)
                        }
                    }
                }
            }
        }
    }

    private fun setupObserver() {
        productViewModel.responseDetailProduct.observe(viewLifecycleOwner) { response ->

            idProduct = response.data.productId
            basePrice = response.data.productPrice
            price = response.data.productPrice
            selectedVariant = response.data.productVariant.firstOrNull()!!.variantName
            productDetail = response.data
            wishlistEntity
            binding.apply {
                tvPriceProduct.text = formatCurrency(response.data.productPrice)
                tvDescriptionProduct.text = response.data.description
                tvTotalRatingTop.text = "(${response.data.totalRating})"
                tvRateTop.text = response.data.productRating.toString()
                tvRatingBottom.text = response.data.productRating.toString()
                tvSatisfaction.text = "${response.data.totalSatisfaction}% merasa puas"
                tvRatingAndReview.text =
                    "${response.data.totalRating} rating . ${response.data.totalReview} ulasan "

                setImage(response.data.image)
                binding.chipGroupVariant.removeAllViews()
                response.data.productVariant.forEachIndexed { index, it ->
                    if (index == 0) {
                        addChips(it.variantName, true)
                    } else {
                        addChips(it.variantName, false)
                    }
                    productList.add(it.variantName)
                    priceList.add(it.variantPrice)
                }
            }
            binding.ivShare.setOnClickListener {
                val share: Intent = Intent().apply {
                    action = Intent.ACTION_SEND
                    putExtra(
                        Intent.EXTRA_TEXT,
                        "${response.data.productName}\n" + "${formatCurrency(response.data.productPrice)}\n" + "http://andre.tokophincon.com/product/$idProduct"
                    )
                    type = "text/plain"
                }

                val shareIntent = Intent.createChooser(share, null)
                startActivity(shareIntent)
            }

            binding.buttonBuyNow.setOnClickListener {
                var product = productDetail.copy(
                    productPrice = price ?: 0,
                    productVariant = productDetail.productVariant.map { variant ->
                        variant.copy(variantName = selectedVariant!!)
                    }
                ).toCheckoutModel()
                checkoutModelList.add(product)
                (requireActivity() as MainActivity).navigateDetailToCheckout(
                    checkoutModelList
                )
            }
            binding.buttonAddToCart.setOnClickListener {
                if (isExist) {
                    if (stock > quantity) {
                        quantity = quantity + 1
                        cartViewModel.updateQuantity(idProduct!!, quantity)
                        Log.d("Product detail stock", stock.toString())
                    } else {
                        showSnackBar(requireView(), "Stock sudah habis")
                    }
                }

                val body = productDetail.copy(
                    productPrice = price ?: 0,
                    productVariant = productDetail.productVariant.map { variant ->
                        variant.copy(variantName = selectedVariant!!)
                    }
                )

                if (!isExist) cartViewModel.insertToCart(body)
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            cartViewModel.getProductById(idProduct!!).collect { cartEntity ->
                if (cartEntity != null) {
                    isExist = true
                    stock = cartEntity.stock
                    quantity = cartEntity.quantity
                }
            }
        }
    }

    private fun setImage(imageList: List<String>) {
        binding.apply {
            viewPager.adapter = ProductImageAdapter(imageList)
            viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
            dotsIndicator.attachTo(viewPager)
            viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                }

                override fun onPageSelected(position: Int) {
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })
        }
    }

    private fun addChips(text: String, autoCheck: Boolean) {
        val chip = Chip(requireContext())
        chip.text = text
        chip.isClickable = true
        chip.isCheckable = true
        chip.isChecked = autoCheck
        binding.chipGroupVariant.addView(chip)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
