package com.phincon.ecommerce.pages.main.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.phincon.ecommerce.databinding.FragmentReviewBuyerBinding
import com.phincon.ecommerce.pages.main.product.adapter.ProductReviewAdapter
import com.phincon.ecommerce.pages.main.product.view_model.ProductViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RatingBuyerFragment : Fragment() {

    private var _binding: FragmentReviewBuyerBinding? = null
    private val binding get() = _binding!!

    private val viewModel: ProductViewModel by viewModels()
    val adapter = ProductReviewAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentReviewBuyerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = arguments
        val argValue = args?.getString("id_product")

        viewModel.getReviewProduct(argValue!!)

        viewModel.responseReviewProduct.observe(viewLifecycleOwner) {
            adapter.submitList(it.data)
        }

        binding.rvReviewBuyer.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvReviewBuyer.adapter = adapter

        binding.ivBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }
}
