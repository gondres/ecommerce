package com.phincon.ecommerce.pages.main.product.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.ImageProductScreenBinding

class ProductImageAdapter(private var image: List<String>) :
    RecyclerView.Adapter<ProductImageAdapter.Pager2ViewHolder>() {

    class Pager2ViewHolder(val binding: ImageProductScreenBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) {
            Glide.with(itemView.context)
                .load(item)
                .placeholder(R.drawable.ic_load_image)
                .timeout(10000)
                .into(binding.ivImage)
        }
    }

    fun getItem(position: Int): String {
        return image[position]
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Pager2ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ImageProductScreenBinding.inflate(inflater, parent, false)
        return Pager2ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return image.size
    }

    override fun onBindViewHolder(holder: Pager2ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
