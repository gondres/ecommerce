package com.phincon.ecommerce.pages.main.product.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.network.model.ReviewProductResponse
import com.phincon.ecommerce.databinding.CardReviewBinding

class ProductReviewAdapter() :
    ListAdapter<ReviewProductResponse.Data, ProductReviewAdapter.ItemViewHolder>(
        ItemDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            CardReviewBinding.inflate(inflater, parent, false) // Replace with your item layout
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
        }
    }

    class ItemViewHolder(private val binding: CardReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ReviewProductResponse.Data) {
            binding.tvNameBuyer.text = item.userName
            binding.tvDescriptionBuyer.text = item.userReview
            binding.ratingBuyer.rating = item.userRating.toFloat()

            Glide.with(itemView.context)
                .load(item.userImage)
                .centerCrop()
                .placeholder(R.drawable.ic_load_image)
                .into(binding.ivProfileBuyer)
        }
    }

    class ItemDiffCallback : DiffUtil.ItemCallback<ReviewProductResponse.Data>() {
        override fun areItemsTheSame(
            oldItem: ReviewProductResponse.Data,
            newItem: ReviewProductResponse.Data
        ): Boolean {
            return oldItem.userName == newItem.userName
        }

        override fun areContentsTheSame(
            oldItem: ReviewProductResponse.Data,
            newItem: ReviewProductResponse.Data
        ): Boolean {
            return oldItem.userName == newItem.userName
        }
    }
}
