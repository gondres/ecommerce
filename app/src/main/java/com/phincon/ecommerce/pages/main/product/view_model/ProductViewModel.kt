package com.phincon.ecommerce.pages.main.product.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.ReviewProductResponse
import com.phincon.ecommerce.repository.service_repository.ProductRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val repository: ProductRepository,
    savedStateHandle: SavedStateHandle
) :
    ViewModel() {

    private val _responseDetailProduct = MutableLiveData<ProductDetailResponse>()
    private val _responseReviewProduct = MutableLiveData<ReviewProductResponse>()
    private val _stateDetail = MutableLiveData<MainStateEvent<ProductDetailResponse>>()
    private val _stateReview = MutableLiveData<MainStateEvent<ReviewProductResponse>>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _isSuccess = MutableLiveData<Boolean>()
    private val _statusCode = MutableLiveData<String>()
    private val _statusMessage = MutableLiveData<String>()

    val responseDetailProduct: LiveData<ProductDetailResponse> = _responseDetailProduct
    val responseReviewProduct: LiveData<ReviewProductResponse> = _responseReviewProduct
    val stateDetail: LiveData<MainStateEvent<ProductDetailResponse>> = _stateDetail
    val stateReview: LiveData<MainStateEvent<ReviewProductResponse>> = _stateReview

    val idProduct = savedStateHandle.get<String>("id_product") ?: ""

    init {
        getDetailProduct(idProduct)
    }

    fun getDetailProduct(idProduct: String) {
        _isLoading.postValue(true)
        _stateDetail.value = MainStateEvent.Loading(null)
        viewModelScope.launch {
            val result = repository.getDetailProduct(idProduct)
            when (result) {
                is MainStateEvent.Loading -> {
                }

                is MainStateEvent.Success -> {
                    _isSuccess.postValue(true)
                    _responseDetailProduct.postValue(result.data!!)
                }

                is MainStateEvent.Error -> {
                    _isSuccess.postValue(false)
                    _statusCode.postValue(result.statusCode)
                    _statusMessage.postValue(result.message)
                }

                is MainStateEvent.Exception -> {
                    _statusCode.postValue("Connection")
                    _statusMessage.postValue(result.errorMessage)

                    _isSuccess.postValue(false)
                }

                else -> {}
            }
            _stateDetail.value = MainStateEvent.Loading(false)
            _stateDetail.value = result!!
        }
    }

    fun getReviewProduct(idProduct: String) {
        _stateReview.value = MainStateEvent.Loading(null)
        viewModelScope.launch {
            val result = repository.getReviewProduct(idProduct)
            when (result) {
                is MainStateEvent.Loading -> {
                }

                is MainStateEvent.Success -> {
                    _isSuccess.postValue(true)
                    _responseReviewProduct.postValue(result.data!!)
                }

                is MainStateEvent.Error -> {
                    _isSuccess.postValue(false)
                    _statusCode.postValue(result.statusCode)
                    _statusMessage.postValue(result.message)
                }

                is MainStateEvent.Exception -> {
                    _isSuccess.postValue(false)
                }
            }
            _stateReview.value = MainStateEvent.Loading(false)
            _stateReview.value = result
            _isLoading.postValue(false)
        }
    }
}
