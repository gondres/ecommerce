package com.phincon.ecommerce.pages.main.store

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.MutableLiveData
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.FragmentBottomSheetFilterBinding
import com.phincon.ecommerce.pages.main.store.view_model.StoreViewModel
import com.phincon.ecommerce.utils.FilterChipBody
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BottomSheetFilter : BottomSheetDialogFragment() {

    private var _binding: FragmentBottomSheetFilterBinding? = null
    private val binding get() = _binding!!

    val filterChipBody by lazy {
        MutableLiveData<FilterChipBody>()
    }
    private val viewModel: StoreViewModel by activityViewModels()

    private var search: String? = null
    private var sort: String? = null
    private var category: String? = null
    private var lowest: Int? = null
    private var highest: Int? = null

    private var chipIdSort: Int = 0
    private var chipIdBrand: Int = 0

    var selectedChips = mutableListOf<String>()

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    companion object {
        const val TAG = "ModalBottomSheet"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(STYLE_NO_FRAME, R.style.BottomSheetDialog)
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val contextThemeWrapper = ContextThemeWrapper(requireContext(), R.style.Theme_App)
        _binding = FragmentBottomSheetFilterBinding.inflate(
            inflater.cloneInContext(contextThemeWrapper),
            container,
            false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val stringArray = resources.getStringArray(R.array.filter_array)
        val chipSortList = stringArray.toList()
        val chipSortListIn = listOf("Ulasan", "Penjualan", "Harga Terendah", "Harga Tertinggi")
        val chipSortListEn = listOf("Review", "Sales", "Lowest Price", "Highest Price")
        val chipBrandList = listOf("Apple", "Asus", "Dell", "Lenovo")

        chipSortList.forEach {
            addChipSort(it)
        }
        chipBrandList.forEach {
            addChipBrand(it)
        }
        binding.chipGroupSort.isSingleSelection = true
        binding.chipGroupCategory.isSingleSelection = true

        filterChipBody.postValue(FilterChipBody())

        viewModel.searchResult.observe(viewLifecycleOwner) {
            search = it
        }
        viewModel.selectedChip.observe(viewLifecycleOwner) {
            sort = it.sort
            category = it.category
            filterChipBody.postValue(
                FilterChipBody(
                    sort = it.sort,
                    category = it.category,
                    lowest = it.lowest,
                    highest = it.highest
                )
            )
            if (it.highest != null) {
                binding.textInputHighest.setText(it.highest.toString())
            }
            if (it.lowest != null) {
                binding.textInputLowest.setText(it.lowest.toString())
            }
        }
        filterChipBody.observe(viewLifecycleOwner) {
            if (it.category.isNullOrBlank() && it.sort.isNullOrBlank() && it.highest == null && it.lowest == null) {
                binding.textButtonReset.visibility = View.INVISIBLE
            } else {
                binding.textButtonReset.visibility = View.VISIBLE
            }

            for (i in 0 until binding.chipGroupSort.childCount) {
                val chip = binding.chipGroupSort.getChildAt(i)
                if (chipSortListIn[i] == it.sort) {
                    if (chip is Chip && chip.text == chipSortListIn[i] || chip is Chip && chip.text == chipSortListEn[i]) {
                        Log.d("Chip text ", chip.text.toString())
                        chip.isChecked = true
                        break
                    }
                    Log.d("Chip index true at ", i.toString())
                }
                if (chipSortListEn[i] == it.sort) {
                    if (chip is Chip && chip.text == chipSortListIn[i] || chip is Chip && chip.text == chipSortListEn[i]) {
                        chip.isChecked = true
                        break
                    }
                    Log.d("Chip index true at ", i.toString())
                }
            }

            for (i in 0 until binding.chipGroupCategory.childCount) {
                val chip = binding.chipGroupCategory.getChildAt(i)
                if (chip is Chip && chip.text == it.category) {
                    chip.isChecked = true
                    break
                }
            }

//
        }

        binding.textButtonReset.setOnClickListener {
            resetFilter()
        }

        binding.chipGroupSort.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == View.NO_ID) {
                chipIdSort = 0
                selectedChips.remove(sort)
                sort = null
            } else {
                val chip: Chip = group.findViewById(checkedId)
                if (chip.isChecked) {
                    sort = chip.text.toString()
                    chipIdSort = checkedId
                    selectedChips.add(sort!!)
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                        param(FirebaseAnalytics.Param.CONTENT_TYPE, "Sort by $sort ")
                    }
                } else {
                    sort = null
                }
            }
            filterChipBody.postValue(
                FilterChipBody().copy(
                    sort = sort,
                    category = category,
                    lowest = lowest,
                    highest = highest
                )
            )
        }

        binding.chipGroupCategory.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == View.NO_ID) {
                chipIdBrand = 0
                selectedChips.remove(category)
                category = null
            } else {
                val chip: Chip = group.findViewById(checkedId)
                if (chip.isChecked) {
                    category = chip.text.toString()
                    chipIdBrand = checkedId
                    selectedChips.add(category!!)
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
                        param(FirebaseAnalytics.Param.CONTENT_TYPE, "Brand $category ")
                    }
                } else {
                    category = null
                }
            }

            filterChipBody.postValue(
                FilterChipBody().copy(
                    sort = sort,
                    category = category,
                    lowest = lowest,
                    highest = highest
                )
            )
        }

        binding.buttonFilter.setOnClickListener {
            submitFilter()
        }

        binding.textInputLowest.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                lowest = text.toString().toIntOrNull()
                filterChipBody.postValue(
                    FilterChipBody().copy(
                        sort = sort,
                        category = category,
                        lowest = lowest,
                        highest = highest
                    )
                )
            } else {
                lowest = null
            }
        }

        binding.textInputHighest.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                highest = text.toString().toIntOrNull()
                filterChipBody.postValue(
                    FilterChipBody().copy(
                        sort = sort,
                        category = category,
                        lowest = lowest,
                        highest = highest
                    )
                )
            } else {
                highest = null
            }
        }
    }

    private fun submitFilter() {
        lowest = binding.textInputLowest.text.toString().toIntOrNull()
        highest = binding.textInputHighest.text.toString().toIntOrNull()

        viewModel.selectChips.postValue(
            FilterChipBody(
                sort = sort,
                category = category,
                lowest = lowest,
                highest = highest
            )
        )

        viewModel.setQuery(
            search = search,
            sort = sort,
            brand = category,
            lowest = lowest,
            highest = highest
        )

        dismiss()
    }

    private fun resetFilter() {
        binding.textInputHighest.text!!.clear()
        binding.textInputLowest.text!!.clear()
        binding.textInputHighest.clearFocus()
        binding.textInputLowest.clearFocus()
        if (chipIdSort != 0) {
            chipIdSort.let { id ->
                val chip: Chip = binding.chipGroupSort.findViewById(id)
                chip.isChecked = false
            }
        }
        if (chipIdBrand != 0) {
            chipIdBrand.let { id ->
                val chip: Chip = binding.chipGroupCategory.findViewById(id)
                chip.isChecked = false
            }
        }

        val imm =
            requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.textInputLowest.windowToken, 0)

        filterChipBody.postValue(FilterChipBody())
//        selectedChips.clear()
    }

    @SuppressLint("ResourceType")
    private fun addChipSort(title: String) {
        val chip = Chip(requireContext())
        val colorStateList = context?.let {
            AppCompatResources.getColorStateList(
                it,
                R.drawable.background_chip_selector
            )
        }
        chip.chipBackgroundColor = colorStateList
        chip.text = title
        chip.isCheckable = true
        chip.isClickable = true
        binding.chipGroupSort.addView(chip)
    }

    @SuppressLint("ResourceType")
    private fun addChipBrand(title: String) {
        val chip = Chip(requireContext())
        val colorStateList = context?.let {
            AppCompatResources.getColorStateList(
                it,
                R.drawable.background_chip_selector
            )
        }
        chip.chipBackgroundColor = colorStateList
        chip.text = title
        chip.isCheckable = true
        chip.isClickable = true
        chip.setBackgroundColor(R.drawable.background_chip_selector)
        binding.chipGroupCategory.addView(chip)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
