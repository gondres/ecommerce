package com.phincon.ecommerce.pages.main.store

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.FragmentBottomSearchBinding
import com.phincon.ecommerce.pages.main.store.adapter.SearchAdapter
import com.phincon.ecommerce.pages.main.store.view_model.StoreViewModel
import com.phincon.ecommerce.utils.click_listener.ItemClickListener
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : BottomSheetDialogFragment(), ItemClickListener {

    private var _binding: FragmentBottomSearchBinding? = null
    private val binding get() = _binding!!

    private val adapter = SearchAdapter(this)

    private val viewModel: StoreViewModel by activityViewModels()

    private var search: String? = null

    var sort: String? = null
    var brand: String? = null
    var highest: Int? = null
    var lowest: Int? = null

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    companion object {
        const val TAG = "BottomSheetSearch"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)

        dialog.setOnShowListener {
            val bottomSheetDialog = it as BottomSheetDialog
            val parentLayout =
                bottomSheetDialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet)
            parentLayout?.let { it ->
                val behaviour = BottomSheetBehavior.from(it)
                setupFullHeight(it)
                behaviour.state = BottomSheetBehavior.STATE_EXPANDED
            }
        }
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentBottomSearchBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
        setupAdapter()

        showSoftKeyboard()

        binding.textInputSearch.doOnTextChanged { text, _, _, _ ->
            viewModel.searchOnInputChange(text.toString())
        }

        binding.textInputSearch.setOnEditorActionListener { _, actionId, _ ->
            search = binding.textInputSearch.text.toString()

            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SEARCH) {
                    param(FirebaseAnalytics.Param.SEARCH_TERM, search.toString())
                }
                viewModel.searchProduct.postValue(search)
                viewModel.setQuery(
                    search = search,
                    sort = sort,
                    brand = brand,
                    highest = highest,
                    lowest = lowest
                )
                dismiss()
                true
            } else {
                false
            }
        }

        viewModel.selectedChip.observe(viewLifecycleOwner) {
            sort = it.sort
            brand = it.category
            highest = it.highest
            lowest = it.lowest
        }
    }

    private fun setupAdapter() {
        binding.rvProductList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.rvProductList.adapter = adapter
    }

    private fun setupObserver() {
        viewModel.responseSearch.observe(viewLifecycleOwner) {
            adapter.submitList(it.data)
        }

        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.GONE
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun getTheme(): Int {
        return R.style.FullScreenBottomSheetStyle
    }

    private fun setupFullHeight(bottomSheet: View) {
        val layoutParams = bottomSheet.layoutParams
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
        bottomSheet.layoutParams = layoutParams
    }

    override fun onItemClicked(data: String) {
        viewModel.searchQuery(data)
        viewModel.searchProduct.postValue(data)
        dismiss()
    }

    override fun navigateToProductDetail(id: String) {
    }

    fun showSoftKeyboard() {
        binding.textInputSearch.setText(viewModel.searchResult.value)
        binding.textInputSearch.requestFocus()
        if (binding.textInputSearch.requestFocus()) {
            val imm = requireContext().getSystemService(InputMethodManager::class.java)
            imm.showSoftInput(binding.textInputSearch, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}
