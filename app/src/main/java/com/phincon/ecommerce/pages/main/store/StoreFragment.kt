package com.phincon.ecommerce.pages.main.store

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.chip.Chip
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.FragmentStoreBinding
import com.phincon.ecommerce.pages.main.store.adapter.LoadStateAdapter
import com.phincon.ecommerce.pages.main.store.adapter.StorePagingAdapter
import com.phincon.ecommerce.pages.main.store.view_model.StoreViewModel
import com.phincon.ecommerce.utils.FilterChipBody
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.click_listener.ItemClickListener
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import retrofit2.HttpException
import java.io.IOException

@AndroidEntryPoint
class StoreFragment : Fragment(), ItemClickListener {

    private var _binding: FragmentStoreBinding? = null
    private val binding get() = _binding!!

    private val storeViewModel: StoreViewModel by activityViewModels()

    private val adapterPaging = StorePagingAdapter(this)

    var search: String? = null
    var brand: String? = null
    val chipSortListIn = listOf("Ulasan", "Penjualan", "Harga Terendah", "Harga Tertinggi")
    val chipSortListEn = listOf("Review", "Sales", "Lowest Price", "Highest Price")

    var isGrid = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObserver()
        setupLayoutList()
        setupSwipeRefresh()
        setupResultState()
        setupSelectedChipsFilter()
        observeSearchText()

        binding.textInputSearch.setOnClickListener {
            showDialogSearch()
        }

        binding.ivFilter.setOnClickListener {
            changeListLayoutStyle()
        }

        binding.chipFilter.setOnClickListener {
            showDialogFilter()
        }
    }

    private fun observeSearchText() {
        storeViewModel.searchResult.observe(viewLifecycleOwner) {
            binding.textInputSearch.setText(it)
        }
        viewLifecycleOwner.lifecycleScope.launch {
            storeViewModel.filterData.collectLatest {
            }
        }
    }

    private fun setupSelectedChipsFilter() {
        val stringArray = resources.getStringArray(R.array.filter_array)
        val stringList = stringArray.toList()

        viewLifecycleOwner.lifecycleScope.launch {
            storeViewModel.selectedChip.observe(viewLifecycleOwner) {
                binding.selectedChip.removeAllViews()
                if (it.sort != null) {
                    for (i in 0 until chipSortListIn.size) {
                        if (it.sort == chipSortListIn[i]) {
                            addChips(stringList[i])
                        }
                        if (it.sort == chipSortListEn[i]) {
                            addChips(stringList[i])
                        }
                    }
                }
                if (it.category != null) {
                    addChips(it.category.toString())
                }
                if (it.lowest != null) {
                    addChips("< ${formatCurrency(it.lowest)}")
                }
                if (it.highest != null) {
                    addChips("> ${formatCurrency(it.highest)}")
                }
            }
        }
    }

    @SuppressLint("ResourceType")
    private fun addChips(text: String) {
        val chip = Chip(requireContext())
        val colorStateList = context?.let {
            AppCompatResources.getColorStateList(
                it,
                R.drawable.background_chip_selector
            )
        }
        chip.chipBackgroundColor = colorStateList
        chip.text = text
        chip.isClickable = false
        chip.isCheckable = true
        binding.selectedChip.addView(chip)
    }

    private fun changeListLayoutStyle() {
        isGrid = !isGrid
        storeViewModel._isGrid.postValue(isGrid)
    }

    private fun showDialogFilter() {
        val modalBottomSheet = BottomSheetFilter()
        modalBottomSheet.show(childFragmentManager, BottomSheetFilter.TAG)
    }

    private fun showDialogSearch() {
        val modalBottomSheet = SearchFragment()
        modalBottomSheet.show(childFragmentManager, SearchFragment.TAG)
    }

    private fun setupResultState() {
        viewLifecycleOwner.lifecycleScope.launch {
            adapterPaging.loadStateFlow.collectLatest { loadStates ->
                if (isGrid) {
                    binding.shimmerViewContainerGrid.isVisible =
                        loadStates.refresh is LoadState.Loading
                    binding.shimmerViewContainerLinear.visibility = View.GONE
                }
                if (!isGrid) {
                    binding.shimmerViewContainerGrid.visibility = View.GONE
                    binding.shimmerViewContainerLinear.isVisible =
                        loadStates.refresh is LoadState.Loading
                }

                binding.filterBar.isVisible =
                    loadStates.refresh !is LoadState.Loading && loadStates.refresh !is LoadState.Error
                binding.layoutError.errorState.isVisible = loadStates.refresh is LoadState.Error
                binding.rvProductList.isVisible =
                    loadStates.refresh !is LoadState.Loading && loadStates.refresh !is LoadState.Error

                if (loadStates.refresh is LoadState.Error) {
                    val error = (loadStates.refresh as LoadState.Error).error

                    if (error is HttpException) {
                        val httpStatusCode = error.code()
                        if (httpStatusCode == 401) {
                            adapterPaging.refresh()
                        }
                        if (httpStatusCode == 404) {
                            binding.layoutError.tvStatusCode.text =
                                context?.getString(R.string.kosong)
                            binding.layoutError.tvStatusMessage.text =
                                context?.getString(R.string.data_kosong)
                            binding.layoutError.buttonErrorState.text = "Reset"
                            binding.layoutError.buttonErrorState.setOnClickListener {
                                storeViewModel.resetChips()
                                adapterPaging.refresh()
                                storeViewModel.resetQuery()
                                storeViewModel.searchProduct.postValue("")
                                storeViewModel.selectChips.postValue(
                                    FilterChipBody(
                                        sort = null,
                                        category = null,
                                        lowest = null,
                                        highest = null
                                    )
                                )
                                storeViewModel._selectedFilter.postValue(emptyList())
                            }
                        }
                        if (httpStatusCode == 500) {
                            binding.layoutError.tvStatusCode.text = "500"
                            binding.layoutError.tvStatusMessage.text =
                                context?.getString(R.string.server_error)
                            binding.layoutError.buttonErrorState.text =
                                context?.getString(R.string.muat_ulang)
                            binding.layoutError.buttonErrorState.setOnClickListener {
                                storeViewModel.resetChips()
                            }
                        }
                    }

                    if (error is IOException) {
                        binding.layoutError.tvStatusCode.text = context?.getString(R.string.koneksi)
                        binding.layoutError.tvStatusMessage.text =
                            context?.getString(R.string.koneksi_kosong)
                        binding.layoutError.buttonErrorState.text =
                            context?.getString(R.string.muat_ulang)
                        binding.layoutError.buttonErrorState.setOnClickListener {
                            storeViewModel.resetChips()
                            storeViewModel.resetQuery()
                            storeViewModel.searchProduct.postValue("")
                            adapterPaging.refresh()
                        }
                    }
                }
            }
        }
    }

    private fun setupSwipeRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            adapterPaging.refresh()
            binding.swipeRefresh.isRefreshing = false
        }
    }

    private fun setupLayoutList() {
        val footerAdapter = LoadStateAdapter()

        val concatAdapter = adapterPaging.withLoadStateFooter(
            footer = footerAdapter
        )
        binding.rvProductList.adapter = concatAdapter

        storeViewModel.isGrid.observe(viewLifecycleOwner) { grid ->
            isGrid = grid
            if (grid) {
                binding.ivFilter.setImageResource(R.drawable.ic_grid_state)
                adapterPaging.isGrid = 1
                val layoutManager = GridLayoutManager(requireContext(), 2)
                binding.rvProductList.layoutManager = layoutManager
                layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                    override fun getSpanSize(position: Int): Int {
                        return if (position == concatAdapter.itemCount - 1 && footerAdapter.itemCount > 0) {
                            2
                        } else {
                            1
                        }
                    }
                }
            } else {
                binding.ivFilter.setImageResource(R.drawable.ic_list_format)
                adapterPaging.isGrid = 0
                val gridLayoutManager = GridLayoutManager(requireContext(), 1)
                binding.rvProductList.layoutManager = gridLayoutManager
            }
        }
    }

    private fun setupObserver() {
        viewLifecycleOwner.lifecycleScope.launch {
            storeViewModel.fetchProduct.collectLatest { pagingData ->
                adapterPaging.submitData(pagingData)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onItemClicked(data: String) {
    }

    override fun navigateToProductDetail(id: String) {
        (requireActivity() as MainActivity).navigateToProductDetail(id)
    }
}
