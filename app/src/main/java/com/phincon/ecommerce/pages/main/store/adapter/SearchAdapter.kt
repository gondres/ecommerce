package com.phincon.ecommerce.pages.main.store.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.phincon.ecommerce.databinding.CardSearchBinding
import com.phincon.ecommerce.utils.click_listener.ItemClickListener

class SearchAdapter(private val itemClickListener: ItemClickListener) :
    ListAdapter<String, SearchAdapter.ItemViewHolder>(
        ItemDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            CardSearchBinding.inflate(inflater, parent, false) // Replace with your item layout
        return ItemViewHolder(binding, itemClickListener)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
            itemClickListener.onItemClicked(getItem(position))
        }
    }

    class ItemViewHolder(
        private val binding: CardSearchBinding,
        private val itemClickListener: ItemClickListener
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: String) {
            binding.tvProduct.text = item
        }
    }

    class ItemDiffCallback : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String, newItem: String): Boolean {
            return oldItem == newItem
        }
    }
}
