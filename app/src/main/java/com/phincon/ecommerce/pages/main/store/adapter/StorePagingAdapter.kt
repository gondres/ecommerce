package com.phincon.ecommerce.pages.main.store.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.network.model.ProductResponse
import com.phincon.ecommerce.databinding.CardGridBinding
import com.phincon.ecommerce.databinding.CardLinearBinding
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.click_listener.ItemClickListener

class StorePagingAdapter(private val itemClickListener: ItemClickListener) :
    PagingDataAdapter<ProductResponse.Data.Item, ViewHolder>(StorePagingCallBack()) {

    companion object {
        const val ITEM_GRID = 0
        const val ITEM_LINEAR = 1
    }

    var isGrid = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (isGrid) {
            0 -> StoreLinearHolder(
                CardLinearBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                parent.context
            )

            else -> {
                StoreGridHolder(
                    CardGridBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ),
                    parent.context
                )
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { data ->
            if (holder is StoreLinearHolder) {
                holder.bind(data)
            }
            if (holder is StoreGridHolder) {
                holder.bind(data)
            }
            holder.itemView.setOnClickListener {
                itemClickListener.navigateToProductDetail(data.productId)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (isGrid) {
            0 -> ITEM_GRID
            else -> ITEM_LINEAR
        }
    }
}

class StorePagingCallBack : DiffUtil.ItemCallback<ProductResponse.Data.Item>() {
    override fun areItemsTheSame(
        oldItem: ProductResponse.Data.Item,
        newItem: ProductResponse.Data.Item
    ): Boolean {
        return oldItem.productId == newItem.productId
    }

    override fun areContentsTheSame(
        oldItem: ProductResponse.Data.Item,
        newItem: ProductResponse.Data.Item
    ): Boolean {
        return oldItem == newItem
    }
}

class StoreLinearHolder(
    val binding: CardLinearBinding,
    val context: Context
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ProductResponse.Data.Item) {
        binding.tvTitleProduct.text = item.productName
        binding.tvPrice.text = "${formatCurrency(item.productPrice)}"
        binding.tvSeller.text = item.store
        binding.tvSalesTotal.text = "${context.getString(R.string.terjual)} ${item.sale}"
        binding.tvRating.text = item.productRating.toString()

        Glide.with(itemView.context)
            .load(item.image)
            .placeholder(R.drawable.ic_load_image)
            .timeout(10000)
            .into(binding.ivProduct)
    }
}

class StoreGridHolder(
    val binding: CardGridBinding,
    val context: Context
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: ProductResponse.Data.Item) {
        binding.tvTitleProduct.text = item.productName
        binding.tvPrice.text = "${formatCurrency(item.productPrice)}"
        binding.tvSeller.text = item.store
        binding.tvSalesTotal.text = "${context.getString(R.string.terjual)} ${item.sale}"
        binding.tvRating.text = item.productRating.toString()

        Glide.with(itemView.context)
            .load(item.image)
            .placeholder(R.drawable.ic_load_image)
            .timeout(10000)
            .into(binding.ivProduct)
    }
}
