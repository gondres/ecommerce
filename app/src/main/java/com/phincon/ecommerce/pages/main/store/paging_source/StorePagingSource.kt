package com.phincon.ecommerce.pages.main.store.paging_source

import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.phincon.ecommerce.core.network.model.ProductResponse
import com.phincon.ecommerce.core.network.model.StoreFilterBody
import com.phincon.ecommerce.core.network.service.ApiService
import retrofit2.HttpException
import javax.inject.Inject

class StorePagingSource @Inject constructor(
    private val apiService: ApiService,
    private val filter: StoreFilterBody
) : PagingSource<Int, ProductResponse.Data.Item>() {
    override suspend fun load(
        params: LoadParams<Int>
    ): LoadResult<Int, ProductResponse.Data.Item> {
        try {
            var dataProduct: ProductResponse? = null
            val page = params.key ?: 1
            val limit = 10
            var nextPageNumber = page + 1

            val response = apiService.getProductsPaging(
                search = filter.search,
                brand = filter.brand,
                highest = filter.highest,
                lowest = filter.lowest,
                sort = filter.sort,
                page = page,
                limit = limit,
            )

            response.body()?.let {
                dataProduct = it
            }

            if (!response.isSuccessful) {
                return LoadResult.Error(HttpException(response))
            }

            return LoadResult.Page(
                data = dataProduct?.data!!.items,
                prevKey = null,
                nextKey = if (nextPageNumber <= dataProduct?.data!!.totalPages) nextPageNumber else null
            )
        } catch (e: Throwable) {
            return LoadResult.Error(e)
        }
    }

    @ExperimentalPagingApi
    override fun getRefreshKey(state: PagingState<Int, ProductResponse.Data.Item>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }
}
