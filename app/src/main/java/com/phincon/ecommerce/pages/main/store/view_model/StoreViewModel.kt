package com.phincon.ecommerce.pages.main.store.view_model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.phincon.ecommerce.core.network.model.SearchResponse
import com.phincon.ecommerce.core.network.model.StoreFilterBody
import com.phincon.ecommerce.repository.service_repository.StoreRepository
import com.phincon.ecommerce.utils.FilterChipBody
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class StoreViewModel @Inject constructor(
    private val storeRepository: StoreRepository,
) :
    ViewModel() {

    private val _responseSearch = MutableLiveData<SearchResponse>()
    private val _errorResponse =
        MutableLiveData<com.phincon.ecommerce.core.network.model.ErrorResponse>()
    private val _isLoading = MutableLiveData<Boolean>()
    private val _isSuccess = MutableLiveData<Boolean>()

    val searchProduct = MutableLiveData<String>()
    val _selectedFilter = MutableLiveData<List<String>>()
    val selectChips = MutableLiveData<FilterChipBody>()
    val _isGrid = MutableLiveData<Boolean>(false)
    val _filterData = MutableStateFlow(StoreFilterBody())

    val filterData = _filterData.asStateFlow()
    val responseSearch: LiveData<SearchResponse> get() = _responseSearch
    val isLoading: LiveData<Boolean> get() = _isLoading
    val isGrid: LiveData<Boolean> get() = _isGrid
    val selectedChip: LiveData<FilterChipBody> get() = selectChips
    val searchResult: LiveData<String> get() = searchProduct

    val fetchProduct = filterData.flatMapLatest { filter ->
        storeRepository.getProduct(filter)
    }.cachedIn(viewModelScope)

    var debounceJob: Job? = null

    fun resetChips() {
        selectChips.postValue(FilterChipBody())
    }

    fun resetQuery() {
        _filterData.update {
            it.copy(
                search = null,
                sort = null,
                brand = null,
                lowest = null,
                highest = null
            )
        }
    }

    fun searchQuery(search: String?) {
        _filterData.update {
            it.copy(
                search = search,
                sort = null,
                brand = null,
                lowest = null,
                highest = null
            )
        }
    }

    fun setQuery(search: String?, sort: String?, brand: String?, lowest: Int?, highest: Int?) {
        _filterData.update {
            it.copy(
                search = search,
                sort = sort,
                brand = brand,
                lowest = lowest,
                highest = highest
            )
        }
    }

    fun searchOnInputChange(search: String?) {
        _isLoading.postValue(true)
        debounceJob?.cancel()
        debounceJob = viewModelScope.launch {
            delay(1000)
            storeRepository.searchProduct(
                search,
                onSuccess = {
                    _responseSearch.postValue(it)
                    _isSuccess.postValue(true)
                },
                onError = {
                    _isSuccess.postValue(false)
                    _errorResponse.postValue(it)
                    Log.d("StoreViewModel", it.toString())
                },
                onException = {
                    _isSuccess.postValue(false)
                }
            )
            _isLoading.postValue(false)
        }
    }
}
