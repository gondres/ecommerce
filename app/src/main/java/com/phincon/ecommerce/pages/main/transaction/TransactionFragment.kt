package com.phincon.ecommerce.pages.main.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.core.network.model.TransactionModel
import com.phincon.ecommerce.databinding.FragmentTransactionBinding
import com.phincon.ecommerce.pages.main.transaction.adapter.TransactionAdapter
import com.phincon.ecommerce.pages.main.transaction.view_model.TransactionViewModel
import com.phincon.ecommerce.utils.click_listener.TransactionClickListener
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TransactionFragment : Fragment(), TransactionClickListener {

    private var _binding: FragmentTransactionBinding? = null
    private val binding get() = _binding!!

    private val viewModel: TransactionViewModel by viewModels()

    private val adapter = TransactionAdapter(this)

    val args by lazy {
        arguments
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentTransactionBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getTransactionList()

        viewModel.state.observe(viewLifecycleOwner) { state ->
            when (state) {
                is MainStateEvent.Loading -> {
                    binding.rvTransaction.visibility = View.INVISIBLE
                    binding.errorState.visibility = View.GONE
                    binding.progressBar.visibility = View.VISIBLE
                }

                is MainStateEvent.Success -> {
                    binding.rvTransaction.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                }

                is MainStateEvent.Error -> {
                    binding.rvTransaction.visibility = View.INVISIBLE
                    binding.errorState.visibility = View.VISIBLE
                    binding.progressBar.visibility = View.GONE
                }

                is MainStateEvent.Exception -> {
                    binding.rvTransaction.visibility = View.INVISIBLE
                    binding.progressBar.visibility = View.GONE
                    binding.errorState.visibility = View.VISIBLE
                }
            }
        }
        viewModel.responsePaymentList.observe(viewLifecycleOwner) {
            adapter.submitList(it.data)

            if (it.data.isEmpty()) {
                binding.errorState.visibility = View.VISIBLE
            } else {
                binding.errorState.visibility = View.GONE
            }
        }

        binding.rvTransaction.adapter = adapter

        binding.buttonErrorState.setOnClickListener {
            viewModel.getTransactionList()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun getReviewProduct(id: String) {
    }

    override fun toRatingProduct(transactionResponse: TransactionModel) {
        (requireActivity() as MainActivity).navigateToCheckoutStatus(
            transactionResponse,
            true
        )
    }
}
