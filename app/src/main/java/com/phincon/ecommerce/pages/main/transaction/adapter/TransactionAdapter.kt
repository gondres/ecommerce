package com.phincon.ecommerce.pages.main.transaction.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.network.model.TransactionHistoryResponse.Data
import com.phincon.ecommerce.core.network.model.toTransactionModel
import com.phincon.ecommerce.databinding.CardTransactionBinding
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.click_listener.TransactionClickListener

class TransactionAdapter(private val itemCLickListener: TransactionClickListener) :
    ListAdapter<Data, TransactionAdapter.ItemViewHolder>(
        ItemDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding =
            CardTransactionBinding.inflate(inflater, parent, false) // Replace with your item layout
        return ItemViewHolder(binding, itemCLickListener, parent.context)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener {
        }
    }

    class ItemViewHolder(
        private val binding: CardTransactionBinding,
        private val itemCLickListener: TransactionClickListener,
        private var context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Data) {
            binding.tvTitleProduct.text = item.name
            binding.tvPrice.text = formatCurrency(item.total)
            binding.tvDate.text = item.date
            binding.tvQuantity.text = "${item.items.size} ${context.getString(R.string.barang)}"
            binding.buttonReview.isVisible = item.review.isNullOrEmpty() || item.rating!! == 0
            binding.buttonReview.setOnClickListener {
                itemCLickListener.toRatingProduct(item.toTransactionModel())
            }

            Glide.with(itemView.context).load(item.image).placeholder(R.drawable.ic_load_image)
                .timeout(10000).into(binding.ivProduct)
        }
    }

    class ItemDiffCallback : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem.invoiceId == newItem.invoiceId
        }

        override fun areContentsTheSame(oldItem: Data, newItem: Data): Boolean {
            return oldItem == newItem
        }
    }
}
