package com.phincon.ecommerce.pages.main.transaction.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phincon.ecommerce.core.network.model.TransactionHistoryResponse
import com.phincon.ecommerce.repository.service_repository.CheckoutRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class TransactionViewModel @Inject constructor(private val repository: CheckoutRepository) :
    ViewModel() {

    private val _responseTransactionList = MutableLiveData<TransactionHistoryResponse>()
    private val _state = MutableLiveData<MainStateEvent<Any>>()

    val responsePaymentList: LiveData<TransactionHistoryResponse> = _responseTransactionList
    val state: LiveData<MainStateEvent<Any>> = _state

    fun getTransactionList() {
        _state.value = MainStateEvent.Loading(true)
        viewModelScope.launch {
            val result = repository.getTransaction()
            when (result) {
                is MainStateEvent.Loading -> {
                }

                is MainStateEvent.Success -> {
                    _responseTransactionList.value = result.data!!
                }

                is MainStateEvent.Error -> {
                }

                is MainStateEvent.Exception -> {
                }

                else -> {}
            }
            _state.value = result
        }
    }
}
