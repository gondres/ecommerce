package com.phincon.ecommerce.pages.main.wishlist

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.phincon.ecommerce.MainActivity
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import com.phincon.ecommerce.databinding.FragmentWishlistBinding
import com.phincon.ecommerce.pages.main.cart.view_model.CartViewModel
import com.phincon.ecommerce.pages.main.wishlist.adapter.WishlistAdapter
import com.phincon.ecommerce.pages.main.wishlist.view_model.WishlistViewModel
import com.phincon.ecommerce.utils.SnackBar.showSnackBar
import com.phincon.ecommerce.utils.click_listener.WishlistClickListener
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class WishlistFragment : Fragment(), WishlistClickListener {
    private var _binding: FragmentWishlistBinding? = null
    private val binding get() = _binding!!

    private val viewModel: WishlistViewModel by viewModels()
    private val cartViewModel: CartViewModel by viewModels()

    val adapter = WishlistAdapter(this)

    var isGrid = false

    private var idProduct: String? = null

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentWishlistBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupLayoutList()
        setupObserver()

        binding.ivFilter.setOnClickListener {
            changeListLayoutStyle()
        }




    }

    private fun setupObserver() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.fetchWishlist().collect {
                if (it.isEmpty()) {
                    binding.filterBar.visibility = View.GONE
                    binding.errorState.visibility = View.VISIBLE
                    binding.rvWishlist.visibility = View.GONE
                } else {
                    binding.filterBar.visibility = View.VISIBLE
                    binding.errorState.visibility = View.GONE
                    binding.rvWishlist.visibility = View.VISIBLE
                    adapter.submitList(it)
                    binding.tvTotal.text = "${it.size} ${getString(R.string.barang)}"
                }
            }
        }
    }

    private fun setupLayoutList() {
        binding.rvWishlist.adapter = adapter
        viewModel.isGrid.observe(viewLifecycleOwner) { grid ->
            isGrid = grid

            if (grid) {
                binding.ivFilter.setImageResource(R.drawable.ic_grid_state)
                adapter.isGrid = 1
                val layoutManager = GridLayoutManager(requireContext(), 2)
                binding.rvWishlist.layoutManager = layoutManager
            } else {
                binding.ivFilter.setImageResource(R.drawable.ic_list_format)
                adapter.isGrid = 0
                val gridLayoutManager = GridLayoutManager(requireContext(), 1)
                binding.rvWishlist.layoutManager = gridLayoutManager
            }
        }
    }

    private fun changeListLayoutStyle() {
        isGrid = !isGrid
        viewModel._isGrid.postValue(isGrid)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun addToCart(cartEntity: CartEntity) {
        idProduct = cartEntity.productId
        val itemProduct = Bundle().apply {
            putString(FirebaseAnalytics.Param.ITEM_ID, cartEntity.productId)
            putString(FirebaseAnalytics.Param.ITEM_NAME, cartEntity.productName)
            putString(FirebaseAnalytics.Param.ITEM_VARIANT, cartEntity.productVariant)
            putString(FirebaseAnalytics.Param.ITEM_BRAND, cartEntity.brand)
            putDouble(
                FirebaseAnalytics.Param.PRICE,
                (cartEntity.productPrice + cartEntity.productPrice).toDouble()
            )
        }
        val itemProductCart = Bundle(itemProduct).apply {
            putLong(FirebaseAnalytics.Param.QUANTITY, 1)
        }

        viewLifecycleOwner.lifecycleScope.launch {
            val result = cartViewModel.getProduct(idProduct!!)

            if(result == null){
                cartViewModel.insertCartWishlist(cartEntity)
            showSnackBar(requireView(), getString(R.string.berhasil_keranjang))
            }else{
                if(result.stock > result.quantity){
                    cartViewModel.updateQuantity(idProduct!!,result.quantity+1)
                    showSnackBar(requireView(), getString(R.string.berhasil_tambah_produk))
                }else{
                    showSnackBar(requireView(), getString(R.string.stok_habis))
                }
            }
        }

        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ADD_TO_CART) {
            param(FirebaseAnalytics.Param.CURRENCY, "IDR")
            param(
                FirebaseAnalytics.Param.VALUE,
                (cartEntity.productPrice + cartEntity.productPrice).toDouble()
            )
            param(FirebaseAnalytics.Param.ITEMS, arrayOf(itemProductCart))
        }

        firebaseAnalytics.logEvent("BUTTON_CLICK") {
            param("BUTTON_NAME", "Wishlist_AddToCart")
        }



    }

    override fun deleteFromWishlist(wishlistEntity: WishlistEntity) {
        viewModel.deleteWishlist(wishlistEntity)
        firebaseAnalytics.logEvent("BUTTON_CLICK") {
            param("BUTTON_NAME", "Wishlist_Delete")
        }
    }

    override fun navigateToProductDetail(productId: String) {
        (requireActivity() as MainActivity).navigateToProductDetail(productId)
    }
}
