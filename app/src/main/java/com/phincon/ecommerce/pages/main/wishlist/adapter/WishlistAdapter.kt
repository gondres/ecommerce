package com.phincon.ecommerce.pages.main.wishlist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import com.phincon.ecommerce.core.database.entity.toCartEntity
import com.phincon.ecommerce.databinding.CardWishlistBinding
import com.phincon.ecommerce.databinding.CardWishlistGridBinding
import com.phincon.ecommerce.pages.main.store.adapter.StorePagingAdapter
import com.phincon.ecommerce.utils.StringUtils.formatCurrency
import com.phincon.ecommerce.utils.click_listener.WishlistClickListener

class WishlistAdapter(private val itemClickListener: WishlistClickListener) :
    ListAdapter<WishlistEntity, ViewHolder>(
        ItemDiffCallback()
    ) {

    companion object {
        const val ITEM_GRID = 0
        const val ITEM_LINEAR = 1
    }

    var isGrid = 0
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return when (isGrid) {
            0 -> WishlistLinearHolder(
                CardWishlistBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                itemClickListener,
                parent.context
            )

            else -> {
                WishlistGridHolder(
                    CardWishlistGridBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ),
                    itemClickListener,
                    parent.context
                )
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { data ->
            if (holder is WishlistLinearHolder) {
                holder.bind(data)
            }
            if (holder is WishlistGridHolder) {
                holder.bind(data)
            }
            holder.itemView.setOnClickListener {
                itemClickListener.navigateToProductDetail(data.productId)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (isGrid) {
            0 -> StorePagingAdapter.ITEM_GRID
            else -> StorePagingAdapter.ITEM_LINEAR
        }
    }

    class WishlistLinearHolder(
        private val binding: CardWishlistBinding,
        private val itemClickListener: WishlistClickListener,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: WishlistEntity) {
            binding.tvTitleProduct.text = item.productName
            binding.tvPrice.text = "${formatCurrency(item.productPrice)}"
            binding.tvSeller.text = item.store
            binding.tvSalesTotal.text = "${context.getString(R.string.terjual)} ${item.sale}"
            binding.tvRating.text = item.productRating.toString()
            binding.delete.setOnClickListener {
                itemClickListener.deleteFromWishlist(item)
            }
            binding.buttonAddToCart.setOnClickListener {
                val body = item.toCartEntity()
                itemClickListener.addToCart(body)
            }
            Glide.with(itemView.context).load(item.image).placeholder(R.drawable.ic_load_image)
                .timeout(10000).into(binding.ivProduct)
        }
    }

    class WishlistGridHolder(
        private val binding: CardWishlistGridBinding,
        private val itemClickListener: WishlistClickListener,
        private val context: Context
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: WishlistEntity) {
            binding.tvTitleProduct.text = item.productName
            binding.tvPrice.text = "${formatCurrency(item.productPrice)}"
            binding.tvSeller.text = item.store
            binding.tvSalesTotal.text = "${context.getString(R.string.terjual)} ${item.sale}"
            binding.tvRating.text = item.productRating.toString()
            binding.delete.setOnClickListener {
                itemClickListener.deleteFromWishlist(item)
            }
            binding.buttonAddToCart.setOnClickListener {
                val body = item.toCartEntity()
                itemClickListener.addToCart(body)
            }
            Glide.with(itemView.context).load(item.image).placeholder(R.drawable.ic_load_image)
                .timeout(10000).into(binding.ivProduct)
        }
    }

    class ItemDiffCallback : DiffUtil.ItemCallback<WishlistEntity>() {
        override fun areItemsTheSame(oldItem: WishlistEntity, newItem: WishlistEntity): Boolean {
            return oldItem.productId == newItem.productId
        }

        override fun areContentsTheSame(oldItem: WishlistEntity, newItem: WishlistEntity): Boolean {
            return oldItem.productId == newItem.productId
        }
    }
}
