package com.phincon.ecommerce.pages.main.wishlist.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import com.phincon.ecommerce.repository.database_repository.WishlistRepository
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.phincon.ecommerce.core.network.model.ProductDetailResponse.Data as ProductDetail

@HiltViewModel
class WishlistViewModel @Inject constructor(
    private val wishlistRepository: WishlistRepository
) : ViewModel() {

    private val _state = MutableLiveData<DatabaseStateEvent>()
    private val _cartEntity = MutableLiveData<CartEntity>()
    val _isGrid = MutableLiveData<Boolean>(false)
    val isGrid: LiveData<Boolean> get() = _isGrid
    val state: LiveData<DatabaseStateEvent> = _state
    val cartEntity: LiveData<CartEntity> = _cartEntity

    fun totalData() = wishlistRepository.getCountOfWishlist()
    fun fetchWishlist() = wishlistRepository.getWishList()

    fun getWishlistById(productId: String) = wishlistRepository.getWishlistById(productId)

    fun insertToWishlist(productDetail: ProductDetail) {
        _state.value = DatabaseStateEvent.Loading
        viewModelScope.launch {
            val result = wishlistRepository.insertWishlist(productDetail)
            _state.value = result
        }
    }

    fun deleteWishlist(wishlistEntity: WishlistEntity) {
        viewModelScope.launch {
            wishlistRepository.deleteWishlist(wishlistEntity)
        }
    }
}
