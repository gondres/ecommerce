package com.phincon.ecommerce.pages.prelogin.login

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.ktx.messaging
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.databinding.FragmentLoginBinding
import com.phincon.ecommerce.pages.prelogin.login.view_model.LoginViewModel
import com.phincon.ecommerce.utils.StringUtils
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private var email: String = ""
    private var password: String = ""

    private val loginViewModel: LoginViewModel by viewModels()

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setSpanWord(view)

        checkEmailPassword()

        binding.buttonLogin.setOnClickListener {
            Firebase.messaging.token.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val token = task.result
                    val body =
                        RegisterBody(email = email, password = password, firebaseToken = token)
                    loginViewModel.login(body)
                }
            }
        }

        loginViewModel.stateLogin.observe(viewLifecycleOwner) { state ->
            when (state) {
                is MainStateEvent.Loading -> {
                    binding.textInputEmail.isEnabled = false
                    binding.textInputPassword.isEnabled = false
                    binding.progressBar.visibility = View.VISIBLE
                    binding.buttonLogin.visibility = View.INVISIBLE
                }

                is MainStateEvent.Success -> {
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN) {
                        param(FirebaseAnalytics.Param.METHOD, "email")
                    }
                    findNavController().navigate(R.id.action_prelogin_to_main)
                }

                is MainStateEvent.Error -> {
                    binding.textInputPassword.isEnabled = true
                    binding.textInputEmail.isEnabled = true
                    binding.progressBar.visibility = View.GONE
                    binding.buttonLogin.visibility = View.VISIBLE
                    Toast.makeText(requireActivity(), state.message, Toast.LENGTH_SHORT).show()
                }

                is MainStateEvent.Exception -> {
                    binding.textInputPassword.isEnabled = true
                    binding.textInputEmail.isEnabled = true
                    binding.progressBar.visibility = View.GONE
                    binding.buttonLogin.visibility = View.VISIBLE
                    Toast.makeText(requireActivity(), state.errorMessage, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }

        binding.buttonRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }
    }

    private fun setSpanWord(view: View) {
        val spanWord = StringUtils
        binding.textViewTnc.text = spanWord.spanText(view, requireContext())
    }

    private fun checkEmailPassword() {
        binding.textInputEmail.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                if (!Patterns.EMAIL_ADDRESS.matcher(text).matches() && text.isNotEmpty()) {
                    binding.textFieldEmail.error =
                        requireContext().getString(R.string.email_tidak_valid)

                    loginViewModel.validateEmail.postValue(false)
                } else {
                    binding.textFieldEmail.helperText =
                        requireContext().getString(R.string.email_helper_text)

                    loginViewModel.validateEmail.postValue(true)
                }
            }
            email = text.toString()
        }
        binding.textInputPassword.doOnTextChanged { text, start, before, count ->
            if (text != null) {
                if (text.length < 8 && text.isNotEmpty()) {
                    binding.textFieldPassword.error =
                        requireContext().getString(R.string.password_tidak_valid)
                    loginViewModel.validatePassword.postValue(false)
                } else {
                    binding.textFieldPassword.helperText =
                        requireContext().getString(R.string.password_helper_text)
                    loginViewModel.validatePassword.postValue(true)
                }
            }
            password = text.toString()
        }

        loginViewModel.validateEmail.observe(viewLifecycleOwner) { email ->
            loginViewModel.validatePassword.observe(viewLifecycleOwner) { password ->
                binding.buttonLogin.isEnabled = email && password
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
