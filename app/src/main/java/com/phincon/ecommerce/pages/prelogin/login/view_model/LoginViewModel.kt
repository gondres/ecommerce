package com.phincon.ecommerce.pages.prelogin.login.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.repository.service_repository.PreloginRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val repository: PreloginRepository,
) : ViewModel() {

    private val _stateLogin =
        MutableLiveData<MainStateEvent<com.phincon.ecommerce.core.network.model.LoginResponse>>()

    var validateEmail: MutableLiveData<Boolean> = MutableLiveData()
    var validatePassword: MutableLiveData<Boolean> = MutableLiveData()
    val stateLogin: LiveData<MainStateEvent<com.phincon.ecommerce.core.network.model.LoginResponse>> =
        _stateLogin

    fun login(body: RegisterBody) {
        _stateLogin.value = MainStateEvent.Loading(true)
        viewModelScope.launch {
            val result = repository.login(body)
            _stateLogin.value = result
        }
    }
}
