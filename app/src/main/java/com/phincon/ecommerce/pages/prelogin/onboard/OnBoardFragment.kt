package com.phincon.ecommerce.pages.prelogin.onboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.FragmentOnBoardBinding
import com.phincon.ecommerce.pages.prelogin.onboard.adapter.OnBoardAdapter
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class OnBoardFragment() : Fragment() {

    private var imagesOnBoard = mutableListOf<Int>()
    private var _binding: FragmentOnBoardBinding? = null
    private val binding get() = _binding!!
    private var index: Int = 0

    @Inject
    lateinit var sharedPref: com.phincon.ecommerce.core.PrefManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        checkUser()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentOnBoardBinding.inflate(inflater, container, false)
        val view = binding.root

        setImageOnBoard()
        setupSlider()

        binding.textButtonSkip.setOnClickListener {
            findNavController().navigate(R.id.action_onBoardFragment_to_loginFragment)
            sharedPref.isFirstLogin(true)
        }

        binding.buttonJoin.setOnClickListener {
            findNavController().navigate(R.id.action_onBoardFragment_to_registerFragment)
            sharedPref.isFirstLogin(true)
        }

        return view
    }

    private fun setImageOnBoard() {
        imagesOnBoard.add(R.drawable.onboard_one)
        imagesOnBoard.add(R.drawable.onboard_two)
        imagesOnBoard.add(R.drawable.onboard_three)
    }

    private fun setupSlider() {
        binding.apply {
            viewPager.adapter = OnBoardAdapter(imagesOnBoard)
            viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
            dotsIndicator.attachTo(viewPager)
            viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                }

                override fun onPageSelected(position: Int) {
                    index = position
                    if (position == 0) {
                        textButtonNext.visibility = View.VISIBLE
                    }
                    if (position == 1) {
                        textButtonNext.visibility = View.VISIBLE
                    }
                    if (position == 2) {
                        textButtonNext.visibility = View.INVISIBLE
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })

            textButtonNext.setOnClickListener {
                if (index < 2) {
                    index += 1
                }
                viewPager.setCurrentItem(index)
            }
        }
    }

    private fun checkUser() {
        val isLoggedIn = sharedPref.getIsFirstLogin()
        val token = sharedPref.getAccessToken()
        val username = sharedPref.getUsername()
        if (isLoggedIn) {
//            if (token.isNotEmpty() && username.isNotEmpty() ) return findNavController().navigate(R.id.action_prelogin_to_main)
            if (token.isEmpty()) return findNavController().navigate(R.id.action_onBoardFragment_to_loginFragment)
            if (username.isEmpty()) return findNavController().navigate(R.id.action_onBoardFragment_to_profileFragment)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
