package com.phincon.ecommerce.pages.prelogin.onboard.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.OnboardScreenBinding

class OnBoardAdapter(private var image: List<Int>) :
    RecyclerView.Adapter<OnBoardAdapter.Pager2ViewHolder>() {

    inner class Pager2ViewHolder(val binding: OnboardScreenBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val ivIntro: ImageView = itemView.findViewById(R.id.ivIntro)

        init {
            ivIntro.setOnClickListener { v: View ->
                val position = adapterPosition
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Pager2ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = OnboardScreenBinding.inflate(inflater, parent, false)
        return Pager2ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return image.size
    }

    override fun onBindViewHolder(holder: Pager2ViewHolder, position: Int) {
        holder.binding.ivIntro.setImageResource(image[position])
    }
}
