package com.phincon.ecommerce.pages.prelogin.profile

import android.Manifest
import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.phincon.ecommerce.R
import com.phincon.ecommerce.databinding.FragmentProfileBinding
import com.phincon.ecommerce.pages.prelogin.profile.view_model.ProfileViewModel
import com.phincon.ecommerce.utils.BitmapUtils
import com.phincon.ecommerce.utils.StringUtils
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale
import javax.inject.Inject

@AndroidEntryPoint
class ProfileFragment() : Fragment() {

    private var _binding: FragmentProfileBinding? = null

    private val binding get() = _binding!!

    private val REQUEST_IMAGE_CAPTURE = 1
    private val REQUEST_GALLERY = 1
    private val CAMERA_PERMISSION_REQUEST_CODE = 100

    var imageProfile: Bitmap? = null
    var imageFile: File? = null
    var username: String = ""

    @Inject
    lateinit var prefManager: com.phincon.ecommerce.core.PrefManager

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics

    private val profileViewModel: ProfileViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSpanWord(view)

        binding.containerProfile.setOnClickListener {
            selectImage()
        }
        binding.buttonDone.setOnClickListener {
            if (imageProfile != null) {
                imageFile = BitmapUtils.convertBitmapToFile(
                    requireContext(),
                    imageProfile!!,
                    generateImageFileName()
                )
            }

            profileViewModel.postProfile(imageFile, username)
        }

        binding.textInputName.doOnTextChanged { text, start, before, count ->
            binding.buttonDone.isEnabled = !text.isNullOrEmpty()
            username = text.toString()
        }

        profileViewModel.stateProfile.observe(viewLifecycleOwner) { state ->
            when (state) {
                is MainStateEvent.Loading -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.textInputName.isEnabled = false
                    binding.buttonDone.visibility = View.INVISIBLE
                }

                is MainStateEvent.Success -> {
                    firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP) {
                        param(FirebaseAnalytics.Param.METHOD, "username")
                    }
                    findNavController().navigate(R.id.action_prelogin_to_main)
                }

                is MainStateEvent.Error -> {
                    binding.progressBar.visibility = View.GONE
                    binding.textInputName.isEnabled = true
                    binding.buttonDone.visibility = View.VISIBLE
                    Toast.makeText(requireActivity(), state.message, Toast.LENGTH_SHORT).show()
                }

                is MainStateEvent.Exception -> {
                    binding.progressBar.visibility = View.GONE
                    binding.textInputName.isEnabled = true
                    binding.buttonDone.visibility = View.VISIBLE
                    Toast.makeText(requireActivity(), state.errorMessage, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        val view = binding.root

        return view
    }

    private fun setSpanWord(view: View) {
        val spanWord = StringUtils
        binding.textViewTnc.text = spanWord.spanText(view, requireContext())
    }

    private fun openCamera() {
        val permission = Manifest.permission.CAMERA
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                permission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
            }
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(permission),
                CAMERA_PERMISSION_REQUEST_CODE
            )
        }
    }

    private fun openGallery() {
        val permission = Manifest.permission.CAMERA
        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                permission
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                startActivityForResult(intent, REQUEST_GALLERY)
            }
        } else {
            // Request camera permission
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(permission),
                CAMERA_PERMISSION_REQUEST_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val image = data?.extras?.get("data") as? Bitmap

            image?.let {
                binding.imageViewProfile.visibility = View.GONE
                imageProfile = image
                Glide.with(requireActivity()).load(image).circleCrop()
                    .into(binding.containerProfile)
            }
        }

        if (requestCode == REQUEST_GALLERY && resultCode == RESULT_OK) {
            val selectedImageUri = data?.data
            selectedImageUri?.let {
                binding.imageViewProfile.visibility = View.GONE
                val bitmap = BitmapUtils.uriToBitmap(requireContext(), selectedImageUri)
                imageProfile = bitmap
                Glide.with(requireActivity()).load(selectedImageUri).circleCrop()
                    .into(binding.containerProfile)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                openCamera()
                val intent =
                    Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(intent, REQUEST_GALLERY)
            } else {
                // Permission denied, handle this case (show a message, etc.)
            }
        }
    }

    private fun selectImage() {
        val choice = arrayOf<CharSequence>(getString(R.string.kamera), getString(R.string.galeri))
        val myAlertDialog: AlertDialog.Builder = AlertDialog.Builder(requireActivity())
        myAlertDialog.setTitle(getString(R.string.pilih_gambar))
        myAlertDialog.setItems(
            choice,
            DialogInterface.OnClickListener { dialog, item ->
                when {
                    choice[item] == getString(R.string.kamera) -> {
                        openCamera()
                    }

                    choice[item] == getString(R.string.galeri) -> {
                        openGallery()
                    }

                    choice[item] == getString(R.string.batal) -> {
                    }
                }
            }
        )
        myAlertDialog.show()
    }

    private fun generateImageFileName(): String {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        return "IMG_$timeStamp"
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
