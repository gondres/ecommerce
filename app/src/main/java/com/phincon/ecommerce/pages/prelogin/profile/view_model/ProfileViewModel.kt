package com.phincon.ecommerce.pages.prelogin.profile.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phincon.ecommerce.core.network.model.ProfileResponse
import com.phincon.ecommerce.repository.service_repository.PreloginRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import java.io.File
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repository: PreloginRepository,
) : ViewModel() {
    private val _stateProfile = MutableLiveData<MainStateEvent<ProfileResponse>>()
    val stateProfile: LiveData<MainStateEvent<ProfileResponse>> = _stateProfile
    var imagePart: MultipartBody.Part? = null

    fun postProfile(
        userImage: File?,
        userName: String
    ) {
        if (userImage != null) {
            imagePart = MultipartBody.Part.createFormData(
                "userImage",
                userImage.name,
                userImage.asRequestBody("image/*".toMediaType())
            )
        }

        val usernamePart = MultipartBody.Part.createFormData(
            "userName",
            userName
        )
        _stateProfile.value = MainStateEvent.Loading(true)
        viewModelScope.launch {
            val result = repository.postProfile(imagePart, usernamePart)
            _stateProfile.value = result
        }
    }
}
