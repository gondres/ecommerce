package com.phincon.ecommerce.pages.prelogin.register

import android.os.Bundle
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.messaging.FirebaseMessaging
import com.phincon.ecommerce.R
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.databinding.FragmentRegisterBinding
import com.phincon.ecommerce.pages.prelogin.register.view_model.RegisterViewModel
import com.phincon.ecommerce.utils.StringUtils
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val registerViewModel: RegisterViewModel by viewModels()

    private var email: String = ""
    private var password: String = ""
    private var validateEmail: Boolean = false
    private var validatePassword: Boolean = false

    @Inject
    lateinit var firebaseAnalytics: FirebaseAnalytics
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        val view = binding.root

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setSpanWord(view)
        checkEmailPassword()
        binding.buttonRegister.setOnClickListener {
            binding.progressBar.visibility = View.VISIBLE
            FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val token = task.result
                    FirebaseMessaging.getInstance().subscribeToTopic("promo")
                    val body = RegisterBody(
                        email = email,
                        password = password,
                        firebaseToken = token
                    )
                    registerViewModel.register(body)
                } else {
                }
            }

            registerViewModel.stateRegister.observe(viewLifecycleOwner) { state ->
                when (state) {
                    is MainStateEvent.Loading -> {
                        binding.textInputEmail.isEnabled = false
                        binding.textInputPassword.isEnabled = false
                        binding.progressBar.visibility = View.VISIBLE
                        binding.buttonRegister.visibility = View.INVISIBLE
                    }

                    is MainStateEvent.Success -> {
                        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP) {
                            param(FirebaseAnalytics.Param.METHOD, "email")
                        }
                        findNavController().navigate(R.id.action_prelogin_to_main)
                    }

                    is MainStateEvent.Error -> {
                        binding.textInputPassword.isEnabled = true
                        binding.textInputEmail.isEnabled = true
                        binding.progressBar.visibility = View.GONE
                        binding.buttonRegister.visibility = View.VISIBLE
                        Toast.makeText(requireActivity(), state.message, Toast.LENGTH_SHORT).show()
                    }

                    is MainStateEvent.Exception -> {
                        binding.textInputPassword.isEnabled = true
                        binding.textInputEmail.isEnabled = true
                        binding.progressBar.visibility = View.GONE
                        binding.buttonRegister.visibility = View.VISIBLE
                        Toast.makeText(requireActivity(), state.errorMessage, Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }
        }
        binding.buttonLogin.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }

    private fun setSpanWord(view: View) {
        val spanWord = StringUtils
        binding.textViewTnc.text = spanWord.spanText(view, requireContext())
    }

    private fun checkEmailPassword() {
        binding.textInputEmail.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                if (!Patterns.EMAIL_ADDRESS.matcher(text).matches() && text.isNotEmpty()) {
                    binding.textFieldEmail.error =
                        requireContext().getString(R.string.email_tidak_valid)
                    registerViewModel.validateEmail.postValue(false)
                    validateEmail = true
                } else {
                    binding.textFieldEmail.helperText =
                        requireContext().getString(R.string.email_helper_text)
                    registerViewModel.validateEmail.postValue(true)
                    validateEmail = false
                }
            }
            email = text.toString()
        }
        binding.textInputPassword.doOnTextChanged { text, _, _, _ ->
            if (text != null) {
                if (text.length < 8 && text.isNotEmpty()) {
                    binding.textFieldPassword.error =
                        requireContext().getString(R.string.password_tidak_valid)
                    registerViewModel.validatePassword.postValue(false)
                    validatePassword = true
                } else {
                    binding.textFieldPassword.helperText =
                        requireContext().getString(R.string.password_helper_text)
                    registerViewModel.validatePassword.postValue(true)
                    validatePassword = false
                }
            }
            password = text.toString()
        }

        registerViewModel.validateEmail.observe(viewLifecycleOwner) { email ->
            registerViewModel.validatePassword.observe(viewLifecycleOwner) { password ->
                binding.buttonRegister.isEnabled = email && password
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
