package com.phincon.ecommerce.pages.prelogin.register.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.core.network.model.RegisterResponse
import com.phincon.ecommerce.repository.service_repository.PreloginRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RegisterViewModel @Inject constructor(
    private val repository: PreloginRepository
) : ViewModel() {

    private val _stateRegister = MutableLiveData<MainStateEvent<RegisterResponse>>()
    var validateEmail: MutableLiveData<Boolean> = MutableLiveData()
    var validatePassword: MutableLiveData<Boolean> = MutableLiveData()
    val stateRegister: LiveData<MainStateEvent<RegisterResponse>> = _stateRegister

    fun register(body: RegisterBody) {
        _stateRegister.value = MainStateEvent.Loading(true)
        viewModelScope.launch {
            val result = repository.register(body)
            _stateRegister.value = result
        }
    }
}
