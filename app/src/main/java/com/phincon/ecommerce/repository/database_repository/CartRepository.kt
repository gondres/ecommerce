package com.phincon.ecommerce.repository.database_repository

import androidx.lifecycle.LiveData
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.core.network.model.toCartEntity
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.phincon.ecommerce.core.network.model.ProductDetailResponse.Data as ProductDetail

class CartRepository @Inject constructor(private val appDatabase: AppDatabase) {

    suspend fun insertCart(productDetailResponse: ProductDetail): DatabaseStateEvent {
        try {
            var cartEntity = productDetailResponse.toCartEntity()

            appDatabase.cartDao().insertAll(cartEntity)
            return DatabaseStateEvent.Success("Sukses menambahkan ke keranjang")
        } catch (e: Exception) {
            return DatabaseStateEvent.Error(e.localizedMessage)
        }
    }

    suspend fun insertCartFromWishlist(
        cartEntity: CartEntity
    ): DatabaseStateEvent {
        try {
            appDatabase.cartDao().insertAll(cartEntity)
            return DatabaseStateEvent.Success("Sukses menambahkan ke keranjang")
        } catch (e: Exception) {
            return DatabaseStateEvent.Error(e.localizedMessage)
        }
    }

    suspend fun insertFromWishlist(productId: String): CartEntity? {
       return appDatabase.cartDao().getCartById(productId)
    }

//    suspend fun insertFromWishlist(cartEntity: CartEntity, productId: String): DatabaseStateEvent {
//        try {
//            val result = appDatabase.cartDao().getCartById(productId)
//            if (result == null) {
//                appDatabase.cartDao().insertAll(cartEntity)
//                return DatabaseStateEvent.Success("insert")
//            } else {
//                if (result.stock > result.quantity) {
//                    appDatabase.cartDao().updateQuantity(productId, result.quantity + 1)
//                    return DatabaseStateEvent.Success("update")
//                } else {
//                    return DatabaseStateEvent.Error("empty")
//                }
//
//            }
//        } catch (e: Exception) {
//            return DatabaseStateEvent.Error(e.localizedMessage)
//        }
//    }

    suspend fun updateAll(cartEntity: List<CartEntity>) {
        var data = cartEntity.toTypedArray()
        return appDatabase.cartDao().updateAll(*data)
    }

    suspend fun updateSingle(cartEntity: CartEntity) {
        return appDatabase.cartDao().updateAll(cartEntity)
    }

    fun getCartList(): Flow<List<CartEntity>> {
        return appDatabase.cartDao().getCartList()
    }

    suspend fun deleteCart(cartEntity: CartEntity) {
        return appDatabase.cartDao().delete(cartEntity)
    }

    suspend fun updateQuantity(productId: String, quantity: Int): DatabaseStateEvent {
        try {
            appDatabase.cartDao().updateQuantity(productId, quantity)
            return DatabaseStateEvent.Success("Sukses menambah item produk")
        } catch (e: Exception) {
            return DatabaseStateEvent.Error(e.localizedMessage)
        }
    }

    fun getCountOfCartItems(): LiveData<Int> {
        return appDatabase.cartDao().countAllCartItems()
    }

    fun getTotalPrice(): LiveData<Int> {
        return appDatabase.cartDao().getTotalCheckedValue()
    }

    fun getProductById(productId: String): Flow<CartEntity?> {
        return appDatabase.cartDao().getCartEntityById(productId)
    }
}
