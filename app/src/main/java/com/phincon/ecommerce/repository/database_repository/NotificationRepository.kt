package com.phincon.ecommerce.repository.database_repository

import androidx.lifecycle.LiveData
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class NotificationRepository @Inject constructor(private val appDatabase: AppDatabase) {

    suspend fun insertNotification(notificationEntity: NotificationEntity): DatabaseStateEvent {
        try {
            appDatabase.notifcationDao().insertAll(notificationEntity)
            return DatabaseStateEvent.Success("Added to list notif")
        } catch (e: Exception) {
            return DatabaseStateEvent.Error(e.localizedMessage)
        }
    }

    fun getNotifList(): Flow<List<NotificationEntity>> {
        return appDatabase.notifcationDao().getNotificationList()
    }

    suspend fun updateNotification(notificationEntity: NotificationEntity): DatabaseStateEvent {
        try {
            appDatabase.notifcationDao().updateNotification(notificationEntity)
            return DatabaseStateEvent.Success("Added to list notif")
        } catch (e: Exception) {
            return DatabaseStateEvent.Error(e.localizedMessage)
        }
    }

    fun getCountOfNotification(): LiveData<Int> {
        return appDatabase.notifcationDao().countAllNotifItems()
    }
}
