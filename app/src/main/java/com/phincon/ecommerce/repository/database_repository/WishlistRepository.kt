package com.phincon.ecommerce.repository.database_repository

import androidx.lifecycle.LiveData
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import com.phincon.ecommerce.core.network.model.toWishlistEntity
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import com.phincon.ecommerce.core.network.model.ProductDetailResponse.Data as ProductDetail

class WishlistRepository @Inject constructor(private val appDatabase: AppDatabase) {

    suspend fun insertWishlist(productDetailResponse: ProductDetail): DatabaseStateEvent {
        try {
            var wishlistEntity = productDetailResponse.toWishlistEntity()
            appDatabase.wishlistDao().insertAll(wishlistEntity)
            return DatabaseStateEvent.Success("Sukses menambahkan ke wishlist")
        } catch (e: Exception) {
            return DatabaseStateEvent.Error(e.localizedMessage)
        }
    }

    fun getWishList(): Flow<List<WishlistEntity>> {
        return appDatabase.wishlistDao().getWishList()
    }

    suspend fun deleteWishlist(wishlistEntity: WishlistEntity) {
        return appDatabase.wishlistDao().delete(wishlistEntity)
    }

    fun getCountOfWishlist(): LiveData<Int> {
        return appDatabase.wishlistDao().countAllWishlist()
    }

    fun getWishlistById(productId: String): Flow<WishlistEntity> {
        return appDatabase.wishlistDao().getWishlistEntityById(productId)
    }
}
