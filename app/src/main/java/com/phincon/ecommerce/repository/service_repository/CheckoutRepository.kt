package com.phincon.ecommerce.repository.service_repository

import com.phincon.ecommerce.core.network.model.PaymentListResponse
import com.phincon.ecommerce.core.network.model.RatingBody
import com.phincon.ecommerce.core.network.model.RatingResponse
import com.phincon.ecommerce.core.network.model.TransactionBody
import com.phincon.ecommerce.core.network.model.TransactionHistoryResponse
import com.phincon.ecommerce.core.network.model.TransactionResponse
import com.phincon.ecommerce.core.network.service.ApiService
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class CheckoutRepository @Inject constructor(private val apiService: ApiService) {

    suspend fun getPaymentList(): MainStateEvent<PaymentListResponse> {
        try {
            val response = apiService.getPaymentList()
            val data = response.body()
            if (response.isSuccessful) {
                if (data != null) {
                    return MainStateEvent.Success(data)
                }
            }
            return MainStateEvent.Success(data!!)
        } catch (e: Exception) {
            return MainStateEvent.Exception(e.message ?: "Unknown Error")
        } catch (e: HttpException) {
            val statusCode = e.code().toString()
            val statusMessage = e.message()
            return MainStateEvent.Error(statusCode, statusMessage)
        } catch (e: IOException) {
            return MainStateEvent.Exception("No Internet Connection")
        }
    }

    suspend fun postTransaction(body: TransactionBody): MainStateEvent<TransactionResponse> {
        try {
            val response = apiService.postTransaction(body)
            val data = response.body()
            if (response.isSuccessful) {
                if (data != null) {
                    return MainStateEvent.Success(data)
                }
            }
            return MainStateEvent.Success(data!!)
        } catch (e: HttpException) {
            val statusCode = e.code().toString()
            val statusMessage = e.message()
            return MainStateEvent.Error(statusCode, statusMessage)
        } catch (e: IOException) {
            return MainStateEvent.Exception("No Internet Connection")
        } catch (e: Exception) {
            return MainStateEvent.Exception(e.message ?: "Unknown Error")
        }
    }

    suspend fun postRatingTransaction(body: RatingBody): MainStateEvent<RatingResponse> {
        try {
            val response = apiService.postRatingTransaction(body)
            val data = response.body()
            if (response.isSuccessful) {
                if (data != null) {
                    return MainStateEvent.Success(data)
                }
            }
            return MainStateEvent.Success(data!!)
        } catch (e: HttpException) {
            val statusCode = e.code().toString()
            val statusMessage = e.message()
            return MainStateEvent.Error(statusCode, statusMessage)
        } catch (e: IOException) {
            return MainStateEvent.Exception("No Internet Connection")
        } catch (e: Exception) {
            return MainStateEvent.Exception(e.message ?: "Unknown Error")
        }
    }

    suspend fun getTransaction(): MainStateEvent<TransactionHistoryResponse> {
        try {
            val response = apiService.getTransaction()
            val data = response.body()
            if (response.isSuccessful) {
                if (data != null) {
                    return MainStateEvent.Success(data)
                }
            }
            return MainStateEvent.Success(data!!)
        } catch (e: HttpException) {
            val statusCode = e.code().toString()
            val statusMessage = e.message()
            return MainStateEvent.Error(statusCode, statusMessage)
        } catch (e: IOException) {
            return MainStateEvent.Exception("No Internet Connection")
        } catch (e: Exception) {
            return MainStateEvent.Exception(e.message ?: "Unknown Error")
        }
    }
}
