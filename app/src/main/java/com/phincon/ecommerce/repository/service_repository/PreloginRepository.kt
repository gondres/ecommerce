package com.phincon.ecommerce.repository.service_repository

import com.phincon.ecommerce.core.network.model.ProfileResponse
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.core.network.model.RegisterResponse
import com.phincon.ecommerce.core.network.service.ApiService
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import okhttp3.MultipartBody
import java.io.IOException
import javax.inject.Inject

class PreloginRepository @Inject constructor(
    private val apiService: ApiService,
    private val prefManager: com.phincon.ecommerce.core.PrefManager
) {

    suspend fun register(body: RegisterBody): MainStateEvent<RegisterResponse> {
        return try {
            val response = apiService.register(body)

            if (response.isSuccessful && response.body() != null) {
                prefManager.setAccessToken(response.body()?.data?.accessToken.toString())
                prefManager.setRefreshToken(response.body()?.data?.refreshToken.toString())
                MainStateEvent.Success(response.body()!!)
            } else {
                if (response.code() == 400) {
                    MainStateEvent.Error("400", "Email is already taken")
                } else {
                    MainStateEvent.Error("${response.code()}", "${response.message()}")
                }
            }
        } catch (e: IOException) {
            MainStateEvent.Exception("No Internet Connection")
        } catch (e: Exception) {
            MainStateEvent.Exception(e.message ?: "Unknown Error")
        }
    }

    suspend fun login(body: RegisterBody): MainStateEvent<com.phincon.ecommerce.core.network.model.LoginResponse> {
        return try {
            val response = apiService.login(body)

            if (response.isSuccessful && response.body() != null) {
                prefManager.setAccessToken(response.body()?.data?.accessToken.toString())
                prefManager.setRefreshToken(response.body()?.data?.refreshToken.toString())
                prefManager.setUsername(response.body()?.data?.userName.toString())
                MainStateEvent.Success(response.body()!!)
            } else {
                if (response.code() == 400) {
                    MainStateEvent.Error("400", "Email or password is not valid")
                } else {
                    MainStateEvent.Error("${response.code()}", "${response.message()}")
                }
            }
        } catch (e: IOException) {
            MainStateEvent.Exception("No Internet Connection")
        } catch (e: Exception) {
            MainStateEvent.Exception(e.message ?: "Unknown Error")
        }
    }

    suspend fun postProfile(
        userImage: MultipartBody.Part?,
        userName: MultipartBody.Part,
    ): MainStateEvent<ProfileResponse> {
        return try {
            val response = apiService.postProfile(userName = userName, userImage = userImage)

            if (response.isSuccessful && response.body() != null) {
                prefManager.setUsername(response.body()?.data?.userName.toString())
                MainStateEvent.Success(response.body()!!)
            } else {
                if (response.code() == 400) {
                    MainStateEvent.Error("400", "Username cannot be null")
                } else if (response.code() == 403) {
                    MainStateEvent.Error("${response.code()}", "User Invalid")
                } else {
                    MainStateEvent.Error("${response.code()}", "${response.message()}")
                }
            }
        } catch (e: IOException) {
            MainStateEvent.Exception("No Internet Connection")
        } catch (e: Exception) {
            MainStateEvent.Exception(e.message ?: "Unknown Error")
        }
    }
}
