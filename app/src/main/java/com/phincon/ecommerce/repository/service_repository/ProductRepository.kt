package com.phincon.ecommerce.repository.service_repository

import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.ReviewProductResponse
import com.phincon.ecommerce.core.network.service.ApiService
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

class ProductRepository @Inject constructor(
    private val apiService: ApiService,
) {

    suspend fun getDetailProduct(
        idProduct: String?
    ): MainStateEvent<ProductDetailResponse>? {
        return try {
            val response = apiService.getDetailProduct(idProduct)
            if (response.isSuccessful && response.body() != null) {
                MainStateEvent.Success(response.body()!!)
            } else {
                MainStateEvent.Exception("Response not successful or body is null")
            }
        } catch (e: HttpException) {
            val statusCode = e.code().toString()
            val statusMessage = e.message()
            MainStateEvent.Error(statusCode, statusMessage)
        } catch (e: IOException) {
            MainStateEvent.Exception("No Internet Connection")
        } catch (e: Exception) {
            MainStateEvent.Exception(e.message ?: "Unknown error")
        }
    }

    suspend fun getReviewProduct(
        idProduct: String?,
    ): MainStateEvent<ReviewProductResponse> {
        try {
            val response = apiService.getReviewProduct(idProduct)
            val data = response.body()
            if (response.isSuccessful) {
                if (data != null) {
                    return MainStateEvent.Success(data)
                }
            }
            return MainStateEvent.Success(data!!)
        } catch (e: HttpException) {
            val statusCode = e.code().toString()
            val statusMessage = e.message()
            return MainStateEvent.Error(statusCode, statusMessage)
        } catch (e: IOException) {
            return MainStateEvent.Exception(e.message ?: "No Internet Connection")
        } catch (e: Exception) {
            return MainStateEvent.Exception(e.message ?: "Unknown error")
        }
    }
}
