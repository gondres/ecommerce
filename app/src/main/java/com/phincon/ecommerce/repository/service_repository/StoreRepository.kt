package com.phincon.ecommerce.repository.service_repository

import android.util.Log
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.phincon.ecommerce.core.network.model.ProductResponse
import com.phincon.ecommerce.core.network.model.SearchResponse
import com.phincon.ecommerce.core.network.model.StoreFilterBody
import com.phincon.ecommerce.core.network.service.ApiClient
import com.phincon.ecommerce.core.network.service.ApiService
import com.phincon.ecommerce.pages.main.store.paging_source.StorePagingSource
import com.phincon.ecommerce.utils.ErrorUtils
import com.skydoves.sandwich.message
import com.skydoves.sandwich.onError
import com.skydoves.sandwich.onException
import com.skydoves.sandwich.suspendOnSuccess
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class StoreRepository @Inject constructor(
    private val apiClient: ApiClient,
    private val errorUtil: ErrorUtils,
    private val apiService: ApiService
) {

    suspend fun searchProduct(
        search: String?,
        onSuccess: (SearchResponse) -> Unit,
        onError: (com.phincon.ecommerce.core.network.model.ErrorResponse) -> Unit,
        onException: (String) -> Unit
    ) {
        val response = apiClient.searchProducts(search)
        response.suspendOnSuccess {
            onSuccess(data)
        }.onError {
            val error = errorUtil.convertStringToObject(message())
            onError(error)
            Log.d("Repo Store Search Product Failed", message())
        }.onException {
            onException(message())
            Log.d("Repo Store Search Product Exception", message())
        }
    }

    fun getProduct(filter: StoreFilterBody): Flow<PagingData<ProductResponse.Data.Item>> {
        return Pager(
            PagingConfig(pageSize = 10, initialLoadSize = 10, prefetchDistance = 1)
        ) {
            StorePagingSource(apiService, filter)
        }.flow
    }
}
