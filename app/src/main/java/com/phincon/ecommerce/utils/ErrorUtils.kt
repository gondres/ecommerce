package com.phincon.ecommerce.utils

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.HttpException
import javax.inject.Inject

class ErrorUtils @Inject constructor() {

    fun extractErrorMessageUsingRegex(response: String): String {
        val pattern = "\"message\":\"(.*?)\"".toRegex()
        val matchResult = pattern.find(response)
        return matchResult?.groupValues?.getOrNull(1) ?: ""
    }

    fun convertStringToObject(message: String): com.phincon.ecommerce.core.network.model.ErrorResponse {
        val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val jsonAdapter =
            moshi.adapter(com.phincon.ecommerce.core.network.model.ErrorResponse::class.java)
        val error = jsonAdapter.fromJson(message)
        return error!!
    }

    fun convertException(e: HttpException): com.phincon.ecommerce.core.network.model.ErrorResponse {
        val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        val jsonAdapter =
            moshi.adapter(com.phincon.ecommerce.core.network.model.ErrorResponse::class.java)
        val error = jsonAdapter.fromJson(e.message)
        return error!!
    }
}
