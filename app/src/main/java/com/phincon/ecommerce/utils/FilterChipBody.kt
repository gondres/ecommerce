package com.phincon.ecommerce.utils

data class FilterChipBody(
    var sort: String? = null,
    val category: String? = null,
    val lowest: Int? = null,
    val highest: Int? = null
)
