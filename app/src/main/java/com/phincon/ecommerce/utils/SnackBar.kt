package com.phincon.ecommerce.utils

import android.view.View
import com.google.android.material.snackbar.Snackbar

object SnackBar {

    fun showSnackBar(view: View, message: String) {
        val snackbar = Snackbar.make(
            view,
            message,
            Snackbar.LENGTH_LONG
        )
        snackbar.show()
    }
}
