package com.phincon.ecommerce.utils

import android.content.Context
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import com.google.android.material.color.MaterialColors
import com.phincon.ecommerce.R
import java.text.NumberFormat
import java.util.Currency
import java.util.Locale

object StringUtils {

    fun spanText(view: View, context: Context): SpannableString {
        val colorPrimary = MaterialColors.getColor(
            view,
            com.google.android.material.R.attr.colorPrimary
        )
        val text = context.getString(R.string.term_and_condition)
        var spanWord = SpannableString(text)

        if (AppCompatDelegate.getApplicationLocales().get(0)?.language != null) {
            if (AppCompatDelegate.getApplicationLocales().get(0)!!.language == "in") {
                spanWord.setSpan(
                    ForegroundColorSpan(colorPrimary),
                    37,
                    55,
                    Spannable.SPAN_EXCLUSIVE_INCLUSIVE
                )
                spanWord.setSpan(
                    ForegroundColorSpan(colorPrimary),
                    61,
                    79,
                    Spannable.SPAN_EXCLUSIVE_INCLUSIVE
                )
            } else {
                // Highlight "Terms & Conditions"
                val termsStart = text.indexOf("Terms & Conditions")
                val termsEnd = termsStart + "Terms & Conditions".length
                spanWord.setSpan(
                    ForegroundColorSpan(colorPrimary),
                    termsStart,
                    termsEnd,
                    SpannableString.SPAN_EXCLUSIVE_INCLUSIVE
                )

                // Highlight "Privacy Policy"
                val privacyStart = text.indexOf("Privacy Policy")
                val privacyEnd = privacyStart + "Privacy Policy".length
                spanWord.setSpan(
                    ForegroundColorSpan(colorPrimary),
                    privacyStart,
                    privacyEnd,
                    SpannableString.SPAN_EXCLUSIVE_INCLUSIVE
                )
            }
        } else {
            val termsStart = text.indexOf("Terms & Conditions")
            val termsEnd = termsStart + "Terms & Conditions".length
            spanWord.setSpan(
                ForegroundColorSpan(colorPrimary),
                termsStart,
                termsEnd,
                SpannableString.SPAN_EXCLUSIVE_INCLUSIVE
            )

            // Highlight "Privacy Policy"
            val privacyStart = text.indexOf("Privacy Policy")
            val privacyEnd = privacyStart + "Privacy Policy".length
            spanWord.setSpan(
                ForegroundColorSpan(colorPrimary),
                privacyStart,
                privacyEnd,
                SpannableString.SPAN_EXCLUSIVE_INCLUSIVE
            )
        }

        return spanWord
    }

    fun formatCurrency(value: Int): String {
        val localeID = Locale("in", "ID") // Indonesian locale
        val currencyFormat = NumberFormat.getCurrencyInstance(localeID)
        currencyFormat.currency = Currency.getInstance("IDR") // Set currency to IDR

        return currencyFormat.format(value)
    }
}
