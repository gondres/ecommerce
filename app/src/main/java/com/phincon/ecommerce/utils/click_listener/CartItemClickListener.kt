package com.phincon.ecommerce.utils.click_listener

import com.phincon.ecommerce.core.database.entity.CartEntity

interface CartItemClickListener {

    fun deleteCart(cartEntity: CartEntity, position: Int)
    fun updateChecked(cartEntity: CartEntity, isChecked: Boolean)
    fun updateQuantity(productId: String, quantity: Int)
    fun navigateToProductDetail(productId: String)
}
