package com.phincon.ecommerce.utils.click_listener

interface CheckoutListener {

    fun sendPaymentMethod(image: String, paymentName: String)
    fun updateTotalCheckout(id: String, quantity: Int)
}
