package com.phincon.ecommerce.utils.click_listener

interface ItemClickListener {
    fun onItemClicked(data: String)
    fun navigateToProductDetail(id: String)
}
