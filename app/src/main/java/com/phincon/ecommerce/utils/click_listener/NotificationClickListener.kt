package com.phincon.ecommerce.utils.click_listener

import com.phincon.ecommerce.core.database.entity.NotificationEntity

interface NotificationClickListener {
    fun updateNotification(notifcationEntity: NotificationEntity)
}
