package com.phincon.ecommerce.utils.click_listener

import com.phincon.ecommerce.core.network.model.TransactionModel

interface TransactionClickListener {

    fun getReviewProduct(id: String)
    fun toRatingProduct(transactionResponse: TransactionModel)
}
