package com.phincon.ecommerce.utils.click_listener

import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.core.database.entity.WishlistEntity

interface WishlistClickListener {

    fun addToCart(cartEntity: CartEntity)
    fun deleteFromWishlist(wishlistEntity: WishlistEntity)
    fun navigateToProductDetail(productId: String)
}
