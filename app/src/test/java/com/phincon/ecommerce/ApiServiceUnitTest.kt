package com.phincon.ecommerce

import com.phincon.ecommerce.core.network.model.LoginResponse
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.ProfileResponse
import com.phincon.ecommerce.core.network.model.RatingBody
import com.phincon.ecommerce.core.network.model.RatingResponse
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.core.network.model.RegisterResponse
import com.phincon.ecommerce.core.network.model.ReviewProductResponse
import com.phincon.ecommerce.core.network.model.SearchResponse
import com.phincon.ecommerce.core.network.model.TransactionBody
import com.phincon.ecommerce.core.network.model.TransactionHistoryResponse
import com.phincon.ecommerce.core.network.model.TransactionResponse
import com.phincon.ecommerce.core.network.service.ApiService
import com.skydoves.sandwich.adapters.ApiResponseCallAdapterFactory
import com.skydoves.sandwich.suspendOnSuccess
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.runBlocking
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import utils.EnqueueResponse.enqueueResponse

@RunWith(JUnit4::class)
class ApiServiceUnitTest {
    private lateinit var mockWebServer: MockWebServer
    private lateinit var apiService: ApiService
    private val moshi =
        Moshi.Builder()
            .addLast(KotlinJsonAdapterFactory())
            .build()

    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        val okHttpClient = OkHttpClient()

        apiService = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(ApiResponseCallAdapterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @After
    fun shutdownMock() {
        mockWebServer.shutdown()
    }


//    @Test
//    fun `test login service`() {
//        mockWebServer.enqueueResponse("login_service_response.json", 200)
//        val expected = LoginResponse(
//            code = 200,
//            message = "OK",
//            data = LoginResponse.LoginData(
//                "image",
//                "name",
//                "1234",
//                600,
//                "2222"
//            )
//        )
//        runBlocking {
//            val actual = apiService.login(
//                RegisterBody(
//                    email = "m@m.m",
//                    password = "12345678",
//                    firebaseToken = ""
//                )
//            )
//            assertEquals(expected, actual.body())
//        }
//    }

    @Test
    fun `test register service`() {
        mockWebServer.enqueueResponse("register_response.json", 200)
        val expected = RegisterResponse(
            code = 200,
            message = "OK",
            data = RegisterResponse.Data(
                accessToken = "111",
                refreshToken = "222",
                expiresAt = 600
            )
        )
        runBlocking {
            val actual = apiService.register(
                RegisterBody(
                    email = "m@m.m",
                    password = "12345678",
                    firebaseToken = ""
                )
            )
            assertEquals(expected, actual.body())
        }
    }

    @Test
    fun `test profile`() {
        mockWebServer.enqueueResponse("profile_response.json", 200)
        val expected = ProfileResponse(
            code = 200,
            message = "OK",
            data = ProfileResponse.Data(
                userImage = "image",
                userName = "username",

                )
        )
        runBlocking {
            val imagePart = MultipartBody.Part.createFormData(
                "userImage",
                "userImage"
            )

            val usernamePart = MultipartBody.Part.createFormData(
                "userName",
                "userName"
            )

            val actual = apiService.postProfile(
                userName = usernamePart,
                userImage = imagePart
            )
            assertEquals(expected, actual.body())
        }
    }

    @Test
    fun `test get product detail`() {
        mockWebServer.enqueueResponse("product_detail_response.json", 200)

        val expected =
            ProductDetailResponse(
                code = 200,
                message = "OK",
                data = ProductDetailResponse.Data(
                    "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                    "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                    24499000,
                    image = listOf(
                        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                    ),
                    "Asus",
                    "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
                    "AsusStore",
                    12,
                    2,
                    7,
                    5,
                    100,
                    5.0,

                    productVariant = listOf(
                        ProductDetailResponse.Data.ProductVariant(
                            "RAM 16GB",
                            0
                        ),
                        ProductDetailResponse.Data.ProductVariant(
                            "RAM 32GB",
                            1000000
                        )
                    )

                )
            )

        runBlocking {
            val actual = apiService.getDetailProduct("1987")
            assertEquals(expected, actual.body())
        }
    }

    @Test
    fun `test get search api`() {
        mockWebServer.enqueueResponse("search_response.json", 200)
        val expected = SearchResponse(
            code = 200,
            message = "OK",
            data = listOf(
                "Lenovo Legion 3",
                "Lenovo Legion 5",
                "Lenovo Legion 7",
                "Lenovo Ideapad 3",
                "Lenovo Ideapad 5",
                "Lenovo Ideapad 7"
            )
        )
        runBlocking {
            val actual = apiService.searchProduct("Lenovo")
            actual.suspendOnSuccess {
                assertEquals(expected, response.body())
            }
        }
    }

    @Test
    fun `test get review product`() {
        mockWebServer.enqueueResponse("review_product_response.json", 200)
        val expected = ReviewProductResponse(
            code = 200,
            message = "OK",
            data = listOf(
                ReviewProductResponse.Data(
                    "John",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                    4,
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                ),
                ReviewProductResponse.Data(
                    "Doe",

                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                    5,
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
                )
            )
        )

        runBlocking {
            val actual = apiService.getReviewProduct("Lenovo")
            assertEquals(expected, actual.body())
        }
    }

    @Test
    fun `test post fullfilment`() {
        mockWebServer.enqueueResponse("fullfilment_response.json", 200)
        val expected = TransactionResponse(
            code = 200,
            message = "OK",
            data = TransactionResponse.Data(
                "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
                true,
                "09 Jun 2023",
                "08:53",
                "Bank BCA",
                48998000
            )
        )

        runBlocking {
            val body = TransactionBody(
                payment = "BCA",
                items = listOf(TransactionBody.Item("1", variantName = "name", quantity = 2))
            )
            val actual = apiService.postTransaction(body)
            assertEquals(expected, actual.body())
        }
    }

    @Test
    fun `test post rating`() {
        mockWebServer.enqueueResponse("rating_response.json", 200)
        val expected = RatingResponse(
            code = 200,
            message = "Fulfillment rating and review success"
        )

        val body = RatingBody(
            "6d2ed50f-0c87-4a57-b873-8a4addd68949",
            0,
            ""

        )
        runBlocking {
            val actual = apiService.postRatingTransaction(body)
            assertEquals(expected, actual.body())
        }
    }

    @Test
    fun `test get history transaction`() {
        mockWebServer.enqueueResponse("transaction_history_response.json", 200)
        val expected = TransactionHistoryResponse(
            200,
            "OK",
            data = listOf(
                TransactionHistoryResponse.Data(
                    "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
                    true,
                    "09 Jun 2023",
                    "09:05",
                    "Bank BCA",
                    48998000,
                    items = listOf(
                        TransactionHistoryResponse.Data.Item(
                            "bee98108-660c-4ac0-97d3-63cdc1492f53",
                            "RAM 16GB",
                            2
                        )
                    ),
                    4,
                    "LGTM",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray"
                )
            )

        )

        runBlocking {
            val actual = apiService.getTransaction()
            assertEquals(expected, actual.body())
        }
    }
}
