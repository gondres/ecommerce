package com.phincon.ecommerce.unit_test_preference

import android.content.Context
import android.content.SharedPreferences
import androidx.test.core.app.ApplicationProvider
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE)
class SharedPrefUnitTest {

    private lateinit var mySharedPreferences: com.phincon.ecommerce.core.PrefManager
    private lateinit var sharedPreferences: SharedPreferences

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        sharedPreferences =
            context.getSharedPreferences(
                com.phincon.ecommerce.core.BaseString.ecommerceSharedPref,
                Context.MODE_PRIVATE
            )
        mySharedPreferences = com.phincon.ecommerce.core.PrefManager(context)
    }

    @Test
    fun testSaveToken() {
        val key = com.phincon.ecommerce.core.BaseString.access_token_pref
        val value = "1234124122131"

        mySharedPreferences.setAccessToken(value)
        val retrievedValue = sharedPreferences.getString(key, value)

        assert(retrievedValue == value)
    }

    @Test
    fun testGetToken() {
        val key = com.phincon.ecommerce.core.BaseString.access_token_pref
        val expectedValue = "1234124122131"

        sharedPreferences.edit().putString(key, expectedValue).apply()
        val result = mySharedPreferences.getAccessToken()

        assert(result == expectedValue)
    }

    @Test
    fun testSetRefreshToken() {
        val key = com.phincon.ecommerce.core.BaseString.refresh_token_pref
        val value = "refresh_token"

        mySharedPreferences.setRefreshToken(value)
        val retrievedValue = sharedPreferences.getString(key, value)

        assert(retrievedValue == value)
    }

    @Test
    fun testGetRefreshToken() {
        val key = com.phincon.ecommerce.core.BaseString.refresh_token_pref
        val expectedValue = "refresh_token"

        sharedPreferences.edit().putString(key, expectedValue).apply()
        val result = mySharedPreferences.getRefreshToken()

        assert(result == expectedValue)
    }

    @Test
    fun testIsFirstLogin() {
        val key = com.phincon.ecommerce.core.BaseString.is_first_pref
        val value = true

        mySharedPreferences.isFirstLogin(value)
        val retrievedValue = sharedPreferences.getBoolean(key, value)

        assert(retrievedValue == value)
    }

    @Test
    fun testGetIsFirstLogin() {
        val key = com.phincon.ecommerce.core.BaseString.is_first_pref
        val expectedValue = true

        sharedPreferences.edit().putBoolean(key, expectedValue).apply()
        val result = mySharedPreferences.getIsFirstLogin()

        assert(result == expectedValue)
    }

    @Test
    fun testSetUsername() {
        val key = com.phincon.ecommerce.core.BaseString.username_pref
        val value = "der"

        mySharedPreferences.setUsername(value)
        val retrievedValue = sharedPreferences.getString(key, value)

        assert(retrievedValue == value)
    }

    @Test
    fun testGetUsername() {
        val key = com.phincon.ecommerce.core.BaseString.username_pref
        val expectedValue = "der"

        sharedPreferences.edit().putString(key, expectedValue).apply()
        val result = mySharedPreferences.getUsername()

        assert(result == expectedValue)
    }

    @Test
    fun testSetFirebaseToken() {
        val key = com.phincon.ecommerce.core.BaseString.firebase_token_pref
        val value = "token"

        mySharedPreferences.setFirebaseToken(value)
        val retrievedValue = sharedPreferences.getString(key, value)

        assert(retrievedValue == value)
    }

    @Test
    fun testSetSessionExpired() {
        val key = com.phincon.ecommerce.core.BaseString.session_expired_pref
        val value = true
        mySharedPreferences.setSessionExpired(value)
        val retrievedValue = sharedPreferences.getBoolean(key, value)
        assert(retrievedValue == value)
    }
}
