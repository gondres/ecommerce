package com.phincon.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.dao.CartDao
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.repository.database_repository.CartRepository
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import java.util.concurrent.Executors
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
class CartRepositoryUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var cartDao: CartDao
    private lateinit var cartRepository: CartRepository

    val productDetail =
        ProductDetailResponse(
            code = 200,
            message = "OK",
            data = ProductDetailResponse.Data(
                "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                24499000,
                image = listOf(
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                ),
                "Asus",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
                "AsusStore",
                12,
                2,
                7,
                5,
                100,
                5.0,

                productVariant = listOf(
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 16GB",
                        0
                    ),
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 32GB",
                        1000000
                    )
                )

            )
        )

    private val cartEntity = CartEntity(
        productId = "1",
        productName = "Laptop",
        image = "http:www.google.com",
        productRating = 5.0,
        totalSatisfaction = 5,
        quantity = 2,
        brand = "Lenovo",
        productVariant = "16 GB",
        productPrice = 15000000,
        description = "Lenovo mantap",
        isChecked = false,
        sale = 100,
        stock = 20,
        store = "Prodell",
        totalRating = 2,
        totalReview = 3
    )

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        )
            .setTransactionExecutor(Executors.newSingleThreadExecutor())
            .build()
        cartDao = database.cartDao()
        cartRepository = mock()
    }

    @After
    fun closeDB() {
        database.close()
    }

    @Test
    fun `insert cart repo then SUCCESS`() = runBlocking {
        whenever(cartRepository.insertCart(productDetail.data)).thenReturn(
            DatabaseStateEvent.Success(
                "Sukses"
            )
        )
        val result = cartRepository.insertCart(productDetail.data)
        assertEquals(DatabaseStateEvent.Success("Sukses"), result)
    }

    @Test
    fun `insert cart repo then ERROR`() = runBlocking {
        whenever(cartRepository.insertCart(productDetail.data)).thenReturn(
            DatabaseStateEvent.Error(
                ""
            )
        )
        val result = cartRepository.insertCart(productDetail.data)
        assertEquals(DatabaseStateEvent.Error(""), result)
    }

    @Test
    fun `update cart all repo`() = runBlocking {
        whenever(cartRepository.updateAll(listOf(cartEntity))).thenReturn(Unit)
        val result = cartRepository.updateSingle(cartEntity)
        assertEquals(Unit, result)
    }

    @Test
    fun `update cart single repo`() = runBlocking {
        whenever(cartRepository.updateSingle(cartEntity)).thenReturn(Unit)
        val result = cartRepository.updateSingle(cartEntity)
        assertEquals(Unit, result)
    }

    @Test
    fun `get cart list repo`() = runBlocking {
        whenever(cartRepository.getCartList()).thenReturn(flowOf(listOf(cartEntity)))
        val result = cartRepository.getCartList().first()
        assertEquals(listOf(cartEntity), result)
    }

    @Test
    fun `delete cart repo`() = runBlocking {
        whenever(cartRepository.deleteCart(cartEntity)).thenReturn(Unit)
        val result = cartRepository.deleteCart(cartEntity)
        assertEquals(Unit, result)
    }

    @Test
    fun `update quantity cart repo then SUCCESS`() = runBlocking {
        whenever(
            cartRepository.updateQuantity(
                "123",
                2
            )
        ).thenReturn(DatabaseStateEvent.Success("Sukses"))
        val result = cartRepository.updateQuantity("123", 2)
        assertEquals(DatabaseStateEvent.Success("Sukses"), result)
    }

    @Test
    fun `update quantity cart repo then ERROR`() = runBlocking {
        whenever(cartRepository.updateQuantity("123", 2)).thenReturn(DatabaseStateEvent.Error(""))
        val result = cartRepository.updateQuantity("123", 2)
        assertEquals(DatabaseStateEvent.Error(""), result)
    }

    @Test
    fun `get count cart items cart repo`() = runBlocking {
        val liveData = MutableLiveData<Int>()
        liveData.value = 1
        cartDao.insertAll(cartEntity)
        whenever(cartRepository.getCountOfCartItems()).thenReturn(liveData)
        val result = withContext(Dispatchers.IO) {
            cartDao.countAllCartItems().getOrAwaitValue()
        }
        assertEquals(1, result)
    }

    @Test
    fun `get total price cart repo`() = runBlocking {
        cartDao.insertAll(cartEntity)
        val liveData = MutableLiveData<Int>()
        liveData.value = 1
        whenever(cartRepository.getTotalPrice()).thenReturn(liveData)
        val result = cartRepository.getTotalPrice().getOrAwaitValue()
        assertEquals(1, result)
    }

    @Test
    fun `get product by id repo`() = runBlocking {
        whenever(cartRepository.getProductById("123")).thenReturn(flowOf(cartEntity))
        val result = cartRepository.getProductById("123").first()
        assertEquals(cartEntity, result)
    }
}
