package com.phincon.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.dao.NotificationDao
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.repository.database_repository.NotificationRepository
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import java.util.concurrent.Executors
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
class NotificationRepositoryUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var notificationDao: NotificationDao
    private lateinit var notificationRepository: NotificationRepository

    private val notificationEntity = NotificationEntity(
        title = "Test",
        image = "Test",
        read = false,
        id = 2,
        type = "Test",
        date = "Test",
        body = "Test",
        time = "Test",
    )

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        )
            .setTransactionExecutor(Executors.newSingleThreadExecutor())
            .build()
        notificationDao = database.notifcationDao()
        notificationRepository = mock()
    }

    @After
    fun closeDB() {
        database.close()
    }

    @Test
    fun `insert notification repo then SUCCESS`() = runBlocking {
        whenever(notificationRepository.insertNotification(notificationEntity)).thenReturn(
            DatabaseStateEvent.Success(
                "Sukses"
            )
        )
        val result = notificationRepository.insertNotification(notificationEntity)
        assertEquals(DatabaseStateEvent.Success("Sukses"), result)
    }

    @Test
    fun `insert notification repo then ERROR`() = runBlocking {
        whenever(notificationRepository.insertNotification(notificationEntity)).thenReturn(
            DatabaseStateEvent.Error(
                ""
            )
        )
        val result = notificationRepository.insertNotification(notificationEntity)
        assertEquals(DatabaseStateEvent.Error(""), result)
    }

    @Test
    fun `update notification repo then SUCCESS`() = runBlocking {
        whenever(notificationRepository.updateNotification(notificationEntity)).thenReturn(
            DatabaseStateEvent.Success("")
        )
        val result = notificationRepository.updateNotification(notificationEntity)
        assertEquals(DatabaseStateEvent.Success(""), result)
    }

    @Test
    fun `update notification repo then ERROR`() = runBlocking {
        whenever(notificationRepository.updateNotification(notificationEntity)).thenReturn(
            DatabaseStateEvent.Error("")
        )
        val result = notificationRepository.updateNotification(notificationEntity)
        assertEquals(DatabaseStateEvent.Error(""), result)
    }

    @Test
    fun `get notification list repo`() = runBlocking {
        whenever(notificationRepository.getNotifList()).thenReturn(flowOf(listOf(notificationEntity)))
        val result = notificationRepository.getNotifList().first()
        assertEquals(listOf(notificationEntity), result)
    }

    @Test
    fun `get count cart items cart repo`() = runBlocking {
        val liveData = MutableLiveData<Int>()
        liveData.value = 1
        notificationDao.insertAll(notificationEntity)
        whenever(notificationRepository.getCountOfNotification()).thenReturn(liveData)
        val result = withContext(Dispatchers.IO) {
            notificationRepository.getCountOfNotification().getOrAwaitValue()
        }
        assertEquals(1, result)
    }
}
