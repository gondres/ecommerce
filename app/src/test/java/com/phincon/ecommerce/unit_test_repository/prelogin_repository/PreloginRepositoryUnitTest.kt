package com.phincon.ecommerce.unit_test_repository.prelogin_repository

import com.phincon.ecommerce.core.network.model.ProfileResponse
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.core.network.model.RegisterResponse
import com.phincon.ecommerce.core.network.service.ApiClient
import com.phincon.ecommerce.core.network.service.ApiService
import com.phincon.ecommerce.repository.service_repository.PreloginRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import kotlinx.coroutines.test.runTest
import okhttp3.MultipartBody
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import retrofit2.Response
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class PreloginRepositoryUnitTest {
    private lateinit var preloginRepository: PreloginRepository
    private lateinit var apiService: ApiService
    private lateinit var apiClient: ApiClient
    private lateinit var prefManager: com.phincon.ecommerce.core.PrefManager

    @Before
    fun setup() {
        apiService = mock()
        apiClient = mock()
        prefManager = mock()
        preloginRepository = PreloginRepository(apiService, prefManager)
    }

    @Test
    fun `test register repository then SUCCESS`() = runTest {
        val body = RegisterBody(
            email = "m@m.m",
            password = "12345678",
            firebaseToken = ""
        )

        val expected = RegisterResponse(

            code = 200,
            message = "OK",
            data = RegisterResponse.Data(
                "Test",
                200
            )
        )
        whenever(
            apiService.register(body)
        ).thenReturn(Response.success(expected))
        val result = preloginRepository.register(body)
        assertEquals(expected, (result as MainStateEvent.Success).data)
    }

    @Test
    fun `test login repository`() = runTest {
        val body = RegisterBody(
            email = "m@m.m",
            password = "12345678",
            firebaseToken = ""
        )
        val expected = com.phincon.ecommerce.core.network.model.LoginResponse(
            code = 200,
            message = "OK",
            data = com.phincon.ecommerce.core.network.model.LoginResponse.LoginData(
                userName = "name",
                userImage = "image",
                accessToken = "1234",
                refreshToken = "2222",
                expiresAt = 600
            )
        )
        whenever(
            apiService.login(body)
        ).thenReturn(Response.success(expected))
        val result = preloginRepository.login(body)
        assertEquals(expected, (result as MainStateEvent.Success).data)
    }

    @Test
    fun `test profile repository`() = runTest {
        val imagePart = MultipartBody.Part.createFormData(
            "userImage",
            "userImage"
        )

        val usernamePart = MultipartBody.Part.createFormData(
            "userName",
            "userName"
        )

        val expected = ProfileResponse(
            code = 200,
            message = "OK",
            data = ProfileResponse.Data(
                "Test",
                "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png"
            )
        )

        whenever(
            apiService.postProfile(imagePart, usernamePart)
        ).thenReturn(Response.success(expected))
        val result = preloginRepository.postProfile(imagePart, usernamePart)
        assertEquals(expected, (result as MainStateEvent.Success).data)
    }
}
