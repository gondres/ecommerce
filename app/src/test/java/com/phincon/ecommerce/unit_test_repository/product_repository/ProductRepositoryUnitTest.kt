package com.phincon.ecommerce.unit_test_repository.product_repository

import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.ReviewProductResponse
import com.phincon.ecommerce.core.network.service.ApiService
import com.phincon.ecommerce.repository.service_repository.ProductRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import kotlinx.coroutines.test.runTest
import okhttp3.MediaType.Companion.toMediaType
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import retrofit2.HttpException
import retrofit2.Response
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class ProductRepositoryUnitTest {
    private lateinit var apiService: ApiService
    private lateinit var productRepository: ProductRepository

    @Before
    fun setup() {
        apiService = mock()
        productRepository = ProductRepository(apiService)
    }

    @Test
    fun `test get detail product repository then SUCCESS`() = runTest {
        val expected =
            ProductDetailResponse(
                code = 200,
                message = "OK",
                data = ProductDetailResponse.Data(
                    "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                    "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                    24499000,
                    image = listOf(
                        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                        "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                    ),
                    "Asus",
                    "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
                    "AsusStore",
                    12,
                    2,
                    7,
                    5,
                    100,
                    5.0,

                    productVariant = listOf(
                        ProductDetailResponse.Data.ProductVariant(
                            "RAM 16GB",
                            0
                        ),
                        ProductDetailResponse.Data.ProductVariant(
                            "RAM 32GB",
                            1000000
                        )
                    )

                )
            )

        whenever(
            apiService.getDetailProduct("123")
        ).thenReturn(Response.success(expected))
        val result = productRepository.getDetailProduct("123")
        assertEquals(expected, (result as MainStateEvent.Success).data)
    }

    @Test
    fun `test get detail product repository then HTTP ERROR`() = runTest {
        val expected = MainStateEvent.Error("404", "Response.error()")
        val errorMessage = "Response.error()"
        val response = Response.error<Nothing>(
            404,
            okhttp3.ResponseBody.create(("plain/text".toMediaType()), errorMessage)
        )

        whenever(
            apiService.getDetailProduct("123")
        ).thenThrow(HttpException(response))
        val result = productRepository.getDetailProduct("123")
        assertEquals(expected, (result as MainStateEvent.Error))
    }

    @Test
    fun `test get detail product repository then EXCEPTION`() = runTest {
        val expected = MainStateEvent.Exception(
            "Unknown error"
        )
        whenever(
            apiService.getDetailProduct("123")
        ).thenThrow(RuntimeException())
        val result = productRepository.getDetailProduct("123")
        assertEquals(expected, (result as MainStateEvent.Exception))
    }

    @Test
    fun `test get detail product review repository then SUCCESS`() = runTest {
        val expected = ReviewProductResponse(
            code = 200,
            message = "OK",
            data = listOf(
                ReviewProductResponse.Data(
                    "John",
                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                    4,
                    "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                ),
                ReviewProductResponse.Data(
                    "Doe",

                    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                    5,
                    "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
                )
            )
        )

        whenever(
            apiService.getReviewProduct("123")
        ).thenReturn(Response.success(expected))
        val result = productRepository.getReviewProduct("123")
        assertEquals(expected, (result as MainStateEvent.Success).data)
    }

    @Test
    fun `test get detail product review repository then HTTP ERROR`() = runTest {
        val expected = MainStateEvent.Error("404", "Response.error()")
        val errorMessage = "Response.error()"
        val response = Response.error<Nothing>(
            404,
            okhttp3.ResponseBody.create(("plain/text".toMediaType()), errorMessage)
        )

        whenever(
            apiService.getReviewProduct("123")
        ).thenThrow(HttpException(response))
        val result = productRepository.getReviewProduct("123")
        assertEquals(expected, (result as MainStateEvent.Error))
    }

    @Test
    fun `test get detail product review repository then EXCEPTION`() = runTest {
        val expected = MainStateEvent.Exception(
            "Unknown error"
        )

        whenever(
            apiService.getReviewProduct("123")
        ).thenThrow(RuntimeException())
        val result = productRepository.getReviewProduct("123")
        assertEquals(expected, (result as MainStateEvent.Exception))
    }
}
