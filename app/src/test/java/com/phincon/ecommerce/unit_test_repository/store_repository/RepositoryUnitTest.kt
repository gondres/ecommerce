package com.phincon.ecommerce.unit_test_repository.store_repository

import com.phincon.ecommerce.core.network.model.RatingBody
import com.phincon.ecommerce.core.network.model.RatingResponse
import com.phincon.ecommerce.core.network.model.TransactionBody
import com.phincon.ecommerce.core.network.model.TransactionHistoryResponse
import com.phincon.ecommerce.core.network.model.TransactionResponse
import com.phincon.ecommerce.core.network.service.ApiService
import com.phincon.ecommerce.repository.service_repository.CheckoutRepository
import com.phincon.ecommerce.repository.service_repository.ProductRepository
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import retrofit2.Response
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class RepositoryUnitTest {
    private lateinit var productRepository: ProductRepository
    private lateinit var checkoutRepository: CheckoutRepository
    private lateinit var apiService: ApiService

    @Before
    fun setup() {
        apiService = mock()
        productRepository = ProductRepository(apiService)
        checkoutRepository = CheckoutRepository(apiService)
    }

    @Test
    fun `test post fullfilment repository`() = runTest {
        val expected = TransactionResponse(
            code = 200,
            message = "OK",
            data = TransactionResponse.Data(
                "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
                true,
                "09 Jun 2023",
                "08:53",
                "Bank BCA",
                48998000
            )
        )

        val body = TransactionBody(
            payment = "BCA",
            items = listOf(TransactionBody.Item("1", variantName = "name", quantity = 2))
        )

        whenever(
            apiService.postTransaction(body)
        ).thenReturn(Response.success(expected))
        val result = checkoutRepository.postTransaction(body)
        assertEquals(expected, (result as MainStateEvent.Success).data)
    }

    @Test
    fun `test post rating repository`() = runTest {
        val expected = RatingResponse(
            code = 200,
            message = "Fulfillment rating and review success"
        )
        val body = RatingBody(
            "6d2ed50f-0c87-4a57-b873-8a4addd68949",
            0,
            ""

        )
        whenever(
            apiService.postRatingTransaction(body)
        ).thenReturn(Response.success(expected))
        val result = checkoutRepository.postRatingTransaction(body)
        assertEquals(expected, (result as MainStateEvent.Success).data)
    }

    @Test
    fun `test get transaction repository`() = runTest {
        val expected = TransactionHistoryResponse(
            200,
            "OK",
            data = listOf(
                TransactionHistoryResponse.Data(
                    "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
                    true,
                    "09 Jun 2023",
                    "09:05",
                    "Bank BCA",
                    48998000,
                    items = listOf(
                        TransactionHistoryResponse.Data.Item(
                            "bee98108-660c-4ac0-97d3-63cdc1492f53",
                            "RAM 16GB",
                            2
                        )
                    ),
                    4,
                    "LGTM",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray"
                )
            )
        )
        whenever(
            apiService.getTransaction()
        ).thenReturn(Response.success(expected))
        val result = checkoutRepository.getTransaction()
        assertEquals(expected, (result as MainStateEvent.Success).data)
    }
}
