package com.phincon.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.dao.WishlistDao
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.repository.database_repository.WishlistRepository
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.mock
import org.mockito.kotlin.whenever
import org.robolectric.RobolectricTestRunner
import java.util.concurrent.Executors
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
class WishlistRepositoryUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var wishlistDao: WishlistDao
    private lateinit var wishlistRepository: WishlistRepository

    private val wishlistEntity = WishlistEntity(
        productId = "1",
        productName = "Laptop",
        image = "http:www.google.com",
        productRating = 5.0,
        totalSatisfaction = 5,
        quantity = 2,
        brand = "Lenovo",
        productVariant = "16 GB",
        productPrice = 15000000,
        description = "Lenovo mantap",
        isChecked = false,
        sale = 100,
        stock = 20,
        store = "Prodell",
        totalRating = 2,
        totalReview = 3
    )

    val productDetail =
        ProductDetailResponse(
            code = 200,
            message = "OK",
            data = ProductDetailResponse.Data(
                "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                24499000,
                image = listOf(
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                ),
                "Asus",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
                "AsusStore",
                12,
                2,
                7,
                5,
                100,
                5.0,

                productVariant = listOf(
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 16GB",
                        0
                    ),
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 32GB",
                        1000000
                    )
                )

            )
        )

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        )
            .setTransactionExecutor(Executors.newSingleThreadExecutor())
            .build()
        wishlistDao = database.wishlistDao()
        wishlistRepository = mock()
    }

    @After
    fun closeDB() {
        database.close()
    }

    @Test
    fun `insert wishlist repo then SUCCESS`() = runBlocking {
        whenever(wishlistRepository.insertWishlist(productDetail.data)).thenReturn(
            DatabaseStateEvent.Success(
                "Sukses"
            )
        )
        val result = wishlistRepository.insertWishlist(productDetail.data)
        assertEquals(DatabaseStateEvent.Success("Sukses"), result)
    }

    @Test
    fun `insert wishlist repo then ERROR`() = runBlocking {
        whenever(wishlistRepository.insertWishlist(productDetail.data)).thenReturn(
            DatabaseStateEvent.Error(
                ""
            )
        )
        val result = wishlistRepository.insertWishlist(productDetail.data)
        assertEquals(DatabaseStateEvent.Error(""), result)
    }

    @Test
    fun `get wishlist list repo`() = runBlocking {
        whenever(wishlistRepository.getWishList()).thenReturn(flowOf(listOf(wishlistEntity)))
        val result = wishlistRepository.getWishList().first()
        assertEquals(listOf(wishlistEntity), result)
    }

    @Test
    fun `get wishlist by id`() = runBlocking {
        whenever(wishlistRepository.getWishlistById("123")).thenReturn(flowOf(wishlistEntity))
        val result = wishlistRepository.getWishlistById("123").first()
        assertEquals(wishlistEntity, result)
    }

    @Test
    fun `delete wishlist repo`() = runBlocking {
        whenever(wishlistRepository.deleteWishlist(wishlistEntity)).thenReturn(Unit)
        val result = withContext(Dispatchers.IO) {
            wishlistRepository.deleteWishlist(wishlistEntity)
        }
        assertEquals(Unit, result)
    }
}
