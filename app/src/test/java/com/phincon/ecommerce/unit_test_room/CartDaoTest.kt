package com.phincon.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.dao.CartDao
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.utils.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.util.concurrent.Executors
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
class CartDaoTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var cartDao: CartDao

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        )
            .setTransactionExecutor(Executors.newSingleThreadExecutor())
            .build()
        cartDao = database.cartDao()
    }

    @After
    fun closeDB() {
        database.close()
    }

    private val cartEntity = CartEntity(
        productId = "1",
        productName = "Laptop",
        image = "http:www.google.com",
        productRating = 5.0,
        totalSatisfaction = 5,
        quantity = 2,
        brand = "Lenovo",
        productVariant = "16 GB",
        productPrice = 15000000,
        description = "Lenovo mantap",
        isChecked = false,
        sale = 100,
        stock = 20,
        store = "Prodell",
        totalRating = 2,
        totalReview = 3
    )

    @Test
    fun insertCartAndGetList() = runBlocking {
        cartDao.insertAll(cartEntity)

        val result = cartDao.getCartList().first()
        assertEquals(listOf(cartEntity), result)
    }

    @Test
    fun updateCart() = runTest {
        cartDao.insertAll(cartEntity)
        cartDao.updateAll(cartEntity)
        val result = cartDao.getCartList().first()
        assertEquals(listOf(cartEntity), result)
    }

    @Test
    fun deleteCart() = runTest {
        cartDao.insertAll(cartEntity)
        cartDao.delete(cartEntity)
        val result = cartDao.getCartList().first()
        assertEquals(listOf(), result)
    }

    @Test
    fun updateQuantityCart() = runTest {
        val addedQuantity = 3

        cartDao.insertAll(cartEntity)
        cartDao.updateQuantity("123", addedQuantity)
        cartEntity.copy(
            quantity = addedQuantity
        )
        val result = cartDao.getCartList().first()
        assertEquals(listOf(cartEntity), result)
    }

    @Test
    fun getCartItemById() = runTest {
        cartDao.insertAll(cartEntity)
        val result = cartDao.getCartEntityById(cartEntity.productId).first()
        assertEquals(cartEntity, result)
    }

    @Test
    fun countCartItem() = runTest {
        withContext(Dispatchers.IO) {
            cartDao.insertAll(cartEntity)
        }

        val result = withContext(Dispatchers.IO) {
            cartDao.countAllCartItems().getOrAwaitValue()
        }
        assertEquals(1, result)
    }

    @Test
    fun getTotalPriceCheckedCart() = runTest {
        val cartEntity =
            cartEntity.copy(isChecked = true)
        val totalPriceChecked = cartEntity.productPrice * cartEntity.quantity
        withContext(Dispatchers.IO) {
            cartDao.insertAll(cartEntity)
        }

        val result = withContext(Dispatchers.IO) {
            cartDao.getTotalCheckedValue().getOrAwaitValue()
        }

        assertEquals(totalPriceChecked, result)
    }
}
