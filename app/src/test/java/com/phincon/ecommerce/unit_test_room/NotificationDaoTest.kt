package com.phincon.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.dao.NotificationDao
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.utils.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.util.concurrent.Executors
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
class NotificationDaoTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var notificationDao: NotificationDao

    private val notificationEntity = NotificationEntity(
        title = "Test",
        image = "Test",
        read = false,
        id = 2,
        type = "Test",
        date = "Test",
        body = "Test",
        time = "Test",
    )

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        )
            .setTransactionExecutor(Executors.newSingleThreadExecutor())
            .build()
        notificationDao = database.notifcationDao()
    }

    @After
    fun closeDB() {
        database.close()
    }

    @Test
    fun insertNotification() = runTest {
        notificationDao.insertAll(notificationEntity)
        val result = notificationDao.getNotificationList().first()
        assertEquals(listOf(notificationEntity), result)
    }

    @Test
    fun updateNotification() = runTest {
        notificationDao.insertAll(notificationEntity)
        notificationDao.updateNotification(notificationEntity)
        val result = notificationDao.getNotificationList().first()
        assertEquals(listOf(notificationEntity), result)
    }

    @Test
    fun countNotification() = runTest {
        withContext(Dispatchers.IO) {
            notificationDao.insertAll(notificationEntity)
        }

        val result = withContext(Dispatchers.IO) {
            notificationDao.countAllNotifItems().getOrAwaitValue()
        }
        assertEquals(1, result)
    }
}
