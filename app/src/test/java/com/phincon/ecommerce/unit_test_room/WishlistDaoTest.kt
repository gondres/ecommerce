package com.phincon.ecommerce

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.dao.WishlistDao
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import com.phincon.ecommerce.utils.getOrAwaitValue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.util.concurrent.Executors
import kotlin.test.assertEquals

@RunWith(RobolectricTestRunner::class)
class WishlistDaoTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var database: AppDatabase
    private lateinit var wishlistDao: WishlistDao

    private val wishlistEntity = WishlistEntity(
        productId = "1",
        productName = "Laptop",
        image = "http:www.google.com",
        productRating = 5.0,
        totalSatisfaction = 5,
        quantity = 2,
        brand = "Lenovo",
        productVariant = "16 GB",
        productPrice = 15000000,
        description = "Lenovo mantap",
        isChecked = false,
        sale = 100,
        stock = 20,
        store = "Prodell",
        totalRating = 2,
        totalReview = 3
    )

    @Before
    fun setup() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabase::class.java
        )
            .setTransactionExecutor(Executors.newSingleThreadExecutor())
            .build()
        wishlistDao = database.wishlistDao()
    }

    @After
    fun closeDB() {
        database.close()
    }

    @Test
    fun insertWishlistAndGetList() = runTest {
        wishlistDao.insertAll(wishlistEntity)
        val result = wishlistDao.getWishList().first()
        assertEquals(listOf(wishlistEntity), result)
    }

    @Test
    fun updateWishlist() = runTest {
        wishlistDao.insertAll(wishlistEntity)
        wishlistEntity.copy(productPrice = 0)
        wishlistDao.updateAll(wishlistEntity)
        val result = wishlistDao.getWishList().first()
        assertEquals(listOf(wishlistEntity), result)
    }

    @Test
    fun deleteWishlist() = runTest {
        wishlistDao.insertAll(wishlistEntity)
        wishlistDao.delete(wishlistEntity)
        val result = wishlistDao.getWishList().first()
        assertEquals(listOf(), result)
    }

    @Test
    fun countWishlist() = runTest {
        withContext(Dispatchers.IO) {
            wishlistDao.insertAll(wishlistEntity)
        }

        val result = withContext(Dispatchers.IO) {
            wishlistDao.countAllWishlist().getOrAwaitValue()
        }
        assertEquals(1, result)
    }

    @Test
    fun getWishlistById() = runTest {
        wishlistDao.insertAll(wishlistEntity)
        val result = wishlistDao.getWishlistEntityById(wishlistEntity.productId).first()
        assertEquals(wishlistEntity, result)
    }
}
