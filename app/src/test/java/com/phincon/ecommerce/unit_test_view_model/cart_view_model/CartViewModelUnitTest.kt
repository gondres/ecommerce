package com.phincon.ecommerce.unit_test_view_model.cart_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.toCartEntity
import com.phincon.ecommerce.pages.main.cart.view_model.CartViewModel
import com.phincon.ecommerce.repository.database_repository.CartRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class CartViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var cartRepository: CartRepository
    private lateinit var cartViewModel: CartViewModel

    val productDetail =
        ProductDetailResponse(
            code = 200,
            message = "OK",
            data = ProductDetailResponse.Data(
                "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                24499000,
                image = listOf(
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                ),
                "Asus",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
                "AsusStore",
                12,
                2,
                7,
                5,
                100,
                5.0,

                productVariant = listOf(
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 16GB",
                        0
                    ),
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 32GB",
                        1000000
                    )
                )

            )
        )

    @Before
    fun setup() = runTest {
        cartRepository = Mockito.mock()
        cartViewModel = CartViewModel(cartRepository)
    }

    @Test
    fun `test insert to cart view model`() = runTest {
        whenever(
            cartRepository.insertCart(productDetail.data)
        ).thenReturn(DatabaseStateEvent.Success(""))

        cartViewModel.insertToCart(productDetail.data)

        Assert.assertEquals(
            DatabaseStateEvent.Loading,
            cartViewModel.cartState.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            DatabaseStateEvent.Success(""),
            cartViewModel.cartState.getOrAwaitValue()
        )
    }

    @Test
    fun `test insert to cart from wishlist view model`() = runTest {
        whenever(
            cartRepository.insertCartFromWishlist(productDetail.data.toCartEntity())
        ).thenReturn(DatabaseStateEvent.Success(""))

        cartViewModel.insertToCartFromWishlist(productDetail.data.toCartEntity())

        Assert.assertEquals(
            DatabaseStateEvent.Loading,
            cartViewModel.cartState.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            DatabaseStateEvent.Success(""),
            cartViewModel.cartState.getOrAwaitValue()
        )
    }

    @Test
    fun `fetch cart view model`() = runBlocking {
        whenever(cartRepository.getCartList()).thenReturn(flowOf(listOf(productDetail.data.toCartEntity())))
        val result = cartViewModel.fetchCart().first()
        assertEquals(listOf(productDetail.data.toCartEntity()), result)
    }

    @Test
    fun `get product by id cart view model`() = runBlocking {
        whenever(cartRepository.getProductById("123")).thenReturn(flowOf(productDetail.data.toCartEntity()))
        val result = cartViewModel.getProductById("123").first()
        assertEquals(productDetail.data.toCartEntity(), result)
    }

    @Test
    fun `total data cart view model`() = runBlocking {
        val liveData = MutableLiveData<Int>()
        liveData.value = 1
        whenever(cartRepository.getCountOfCartItems()).thenReturn(liveData)
        val result = cartViewModel.totalData().getOrAwaitValue()
        assertEquals(1, result)
    }

    @Test
    fun `update all cart view model`() = runBlocking {
        whenever(cartRepository.updateAll(listOf(productDetail.data.toCartEntity()))).thenReturn(
            Unit
        )
        val result = cartViewModel.updateAll(listOf(productDetail.data.toCartEntity()))
        assertEquals(Unit, result)
    }

    @Test
    fun `update single cart view model`() = runBlocking {
        whenever(cartRepository.updateSingle(productDetail.data.toCartEntity())).thenReturn(Unit)
        val result = cartViewModel.updateSingle(productDetail.data.toCartEntity())
        assertEquals(Unit, result)
    }

    @Test
    fun `delete cart view model`() = runBlocking {
        whenever(cartRepository.deleteCart(productDetail.data.toCartEntity())).thenReturn(Unit)
        val result = cartViewModel.deleteCart(productDetail.data.toCartEntity())
        assertEquals(Unit, result)
    }
}
