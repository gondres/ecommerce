package com.phincon.ecommerce.unit_test_view_model.checkout_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.phincon.ecommerce.core.network.model.RatingBody
import com.phincon.ecommerce.core.network.model.RatingResponse
import com.phincon.ecommerce.core.network.model.TransactionBody
import com.phincon.ecommerce.core.network.model.TransactionResponse
import com.phincon.ecommerce.pages.main.checkout.view_model.CheckoutViewModel
import com.phincon.ecommerce.repository.service_repository.CheckoutRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class CheckoutViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var checkoutRepository: CheckoutRepository
    private lateinit var firebaseRemoteConfig: FirebaseRemoteConfig
    private lateinit var checkoutViewModel: CheckoutViewModel

    val bodyTransaction = TransactionBody(
        "Bank BCA",
        listOf(
            TransactionBody.Item(
                "5152a3dc-5c2f-4535-a06b-a9768a6c2add",
                "RAM 16GB",
                2
            )
        )
    )
    val bodyRating = RatingBody(
        "6d2ed50f-0c87-4a57-b873-8a4addd68949",
        0,
        ""

    )

    val expectedTransaction = TransactionResponse(
        code = 200,
        message = "OK",
        data =
        TransactionResponse.Data(
            "ba47402c-d263-49d3-a1f8-759ae59fa4a1",
            true,
            "09 Jun 2023",
            "08:53",
            "Bank BCA",
            48998000
        )
    )

    val expectedRating = RatingResponse(
        code = 200,
        message = "OK"
    )

    @Before
    fun setup() = runTest {
        checkoutRepository = Mockito.mock()
        firebaseRemoteConfig = Mockito.mock()
        checkoutViewModel = CheckoutViewModel(checkoutRepository, firebaseRemoteConfig)
    }

    @Test
    fun `test post transaction view model`() = runTest {
        whenever(
            checkoutRepository.postTransaction(bodyTransaction)
        ).thenReturn(MainStateEvent.Success(expectedTransaction))
        checkoutViewModel.postTransaction(bodyTransaction)
        Assert.assertEquals(
            MainStateEvent.Loading(true),
            checkoutViewModel.stateTransaction.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            MainStateEvent.Success(expectedTransaction),
            checkoutViewModel.stateTransaction.getOrAwaitValue()
        )
    }

    @Test
    fun `test post rating transaction view model`() = runTest {
        whenever(
            checkoutRepository.postRatingTransaction(bodyRating)
        ).thenReturn(MainStateEvent.Success(expectedRating))
        checkoutViewModel.postRatingTransaction(bodyRating)
        Assert.assertEquals(
            MainStateEvent.Loading(true),
            checkoutViewModel.stateRating.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            MainStateEvent.Success(expectedRating),
            checkoutViewModel.stateRating.getOrAwaitValue()
        )
    }
}
