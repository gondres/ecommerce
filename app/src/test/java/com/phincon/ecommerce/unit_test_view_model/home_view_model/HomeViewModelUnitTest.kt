package com.phincon.ecommerce.unit_test_view_model.home_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.module.SessionManager
import com.phincon.ecommerce.pages.main.home.view_model.HomeViewModel
import com.phincon.ecommerce.repository.database_repository.CartRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.doNothing
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class HomeViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var cartRepository: CartRepository
    private lateinit var homeViewModel: HomeViewModel
    private lateinit var prefManager: com.phincon.ecommerce.core.PrefManager
    private lateinit var sessionManager: SessionManager
    private lateinit var appDatabase: AppDatabase

    @Before
    fun setup() = runTest {
        prefManager = Mockito.mock()
        sessionManager = Mockito.mock()
        appDatabase = Mockito.mock()
        homeViewModel = HomeViewModel(prefManager, sessionManager, appDatabase)
    }

    @Test
    fun `test reset session view model`() = runTest {
        doNothing().whenever(sessionManager).resetSession()
        homeViewModel.resetSession()
        verify(sessionManager).resetSession()
    }

    @Test
    fun `test delete all data view model`() = runTest {
        doNothing().whenever(prefManager).deletePref()
        doNothing().whenever(appDatabase).clearAllTables()
        homeViewModel.deleteAllData()
        verify(prefManager).deletePref()
        verify(appDatabase).clearAllTables()
    }
}
