package com.phincon.ecommerce.unit_test_view_model.login_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.pages.prelogin.login.view_model.LoginViewModel
import com.phincon.ecommerce.repository.service_repository.PreloginRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class LoginViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var preloginRepository: PreloginRepository
    private lateinit var firebaseRemoteConfig: FirebaseRemoteConfig
    private lateinit var loginViewModel: LoginViewModel

    val body = RegisterBody(
        email = "m@m.m",
        password = "12345678",
        firebaseToken = ""
    )

    val expected = com.phincon.ecommerce.core.network.model.LoginResponse(
        code = 200,
        message = "OK",
        data = com.phincon.ecommerce.core.network.model.LoginResponse.LoginData(
            userName = "name",
            userImage = "image",
            accessToken = "1234",
            refreshToken = "2222",
            expiresAt = 600
        )
    )

    @Before
    fun setup() {
        preloginRepository = Mockito.mock()
        firebaseRemoteConfig = Mockito.mock()
        loginViewModel = LoginViewModel(preloginRepository)
    }

    @Test
    fun `test login user view model`() = runTest {
        whenever(
            preloginRepository.login(body)
        ).thenReturn(MainStateEvent.Success(expected))
        loginViewModel.login(body)
        Assert.assertEquals(
            MainStateEvent.Loading(true),
            loginViewModel.stateLogin.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            MainStateEvent.Success(expected),
            loginViewModel.stateLogin.getOrAwaitValue()
        )
    }
}
