package com.phincon.ecommerce.unit_test_view_model.notification_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.pages.main.notification.view_model.NotificationViewModel
import com.phincon.ecommerce.repository.database_repository.NotificationRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class NotificationViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var notificationRepository: NotificationRepository
    private lateinit var notificationViewModel: NotificationViewModel

    private val notificationEntity = NotificationEntity(
        title = "Test",
        image = "Test",
        read = false,
        id = 2,
        type = "Test",
        date = "Test",
        body = "Test",
        time = "Test",
    )

    @Before
    fun setup() = runTest {
        notificationRepository = Mockito.mock()
        notificationViewModel = NotificationViewModel(notificationRepository)
    }

    @Test
    fun `fetch notification list view model`() = runTest {
        whenever(notificationRepository.getNotifList()).thenReturn(flowOf(listOf(notificationEntity)))
        val result = notificationViewModel.fetchNotificationlist().first()
        assertEquals(listOf(notificationEntity), result)
    }

    @Test
    fun `total data notification view model`() = runTest {
        val liveData = MutableLiveData<Int>()
        liveData.value = 1
        whenever(notificationRepository.getCountOfNotification()).thenReturn(liveData)

        val result = notificationViewModel.totalData().getOrAwaitValue()
        assertEquals(1, result)
    }

    @Test
    fun `update notification view model`() = runTest {
        whenever(notificationRepository.updateNotification(notificationEntity)).thenReturn(
            DatabaseStateEvent.Success(notificationEntity)
        )
        notificationViewModel.updateNotification(notificationEntity)
        Assert.assertEquals(
            DatabaseStateEvent.Loading,
            notificationViewModel.state.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            DatabaseStateEvent.Success(notificationEntity),
            notificationViewModel.state.getOrAwaitValue()
        )
    }
}
