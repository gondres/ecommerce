package com.phincon.ecommerce.unit_test_view_model.product_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.SavedStateHandle
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.ReviewProductResponse
import com.phincon.ecommerce.pages.main.product.view_model.ProductViewModel
import com.phincon.ecommerce.repository.service_repository.ProductRepository
import com.phincon.ecommerce.repository.service_repository.StoreRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class ProductViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var productRepository: ProductRepository
    private lateinit var storeRepository: StoreRepository
    private lateinit var savedStateHandle: SavedStateHandle
    private lateinit var productViewModel: ProductViewModel

    val expected =
        ProductDetailResponse(
            code = 200,
            message = "OK",
            data = ProductDetailResponse.Data(
                "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                24499000,
                image = listOf(
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                ),
                "Asus",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
                "AsusStore",
                12,
                2,
                7,
                5,
                100,
                5.0,

                productVariant = listOf(
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 16GB",
                        0
                    ),
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 32GB",
                        1000000
                    )
                )

            )
        )

    val reviewExpected = ReviewProductResponse(
        code = 200,
        message = "OK",
        data = listOf(
            ReviewProductResponse.Data(
                "John",
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQM4VpzpVw8mR2j9_gDajEthwY3KCOWJ1tOhcv47-H9o1a-s9GRPxdb_6G9YZdGfv0HIg&usqp=CAU",
                4,
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
            ),
            ReviewProductResponse.Data(
                "Doe",

                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTR3Z6PN8QNVhH0e7rEINu_XJS0qHIFpDT3nwF5WSkcYmr3znhY7LOTkc8puJ68Bts-TMc&usqp=CAU",
                5,
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."
            )
        )
    )

    @Before
    fun setup() = runTest {
        productRepository = Mockito.mock()
        storeRepository = Mockito.mock()
        savedStateHandle = Mockito.mock()
        productViewModel = Mockito.mock()
    }

    @Test
    fun `test detail product view model`() = runTest {
        whenever(
            productRepository.getDetailProduct("")
        ).thenReturn(MainStateEvent.Success(expected))
        productViewModel = ProductViewModel(productRepository, savedStateHandle)
        productViewModel.getDetailProduct("")
        Assert.assertEquals(
            MainStateEvent.Loading(null),
            productViewModel.stateDetail.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            MainStateEvent.Success(expected),
            productViewModel.stateDetail.getOrAwaitValue()
        )
    }

    @Test
    fun `test review product view model`() = runTest {
        whenever(
            productRepository.getReviewProduct("")
        ).thenReturn(MainStateEvent.Success(reviewExpected))
        whenever(
            productRepository.getDetailProduct("")
        ).thenReturn(MainStateEvent.Success(expected))
        productViewModel = ProductViewModel(productRepository, savedStateHandle)
        productViewModel.getReviewProduct("")
        Assert.assertEquals(
            MainStateEvent.Loading(null),
            productViewModel.stateReview.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            MainStateEvent.Success(reviewExpected),
            productViewModel.stateReview.getOrAwaitValue()
        )
    }
}
