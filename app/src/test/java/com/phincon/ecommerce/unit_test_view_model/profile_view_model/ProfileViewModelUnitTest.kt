//package com.phincon.ecommerce.unit_test_view_model.profile_view_model
//
//import androidx.arch.core.executor.testing.InstantTaskExecutorRule
//import com.phincon.ecommerce.core.network.model.ProfileResponse
//import com.phincon.ecommerce.pages.prelogin.profile.view_model.ProfileViewModel
//import com.phincon.ecommerce.repository.service_repository.PreloginRepository
//import com.phincon.ecommerce.utils.MainDispatcherRule
//import com.phincon.ecommerce.utils.getOrAwaitValue
//import com.phincon.ecommerce.utils.state_event.MainStateEvent
//import kotlinx.coroutines.test.advanceTimeBy
//import kotlinx.coroutines.test.runTest
//import okhttp3.MediaType.Companion.toMediaType
//import okhttp3.MultipartBody
//import okhttp3.RequestBody.Companion.asRequestBody
//import org.junit.Assert
//import org.junit.Before
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//import org.junit.runners.JUnit4
//import org.mockito.Mockito
//import org.mockito.kotlin.whenever
//import java.io.File
//
//
//@RunWith(JUnit4::class)
// class ProfileViewModelUnitTest {
//
//    @get:Rule
//    val instantExecutorRule = InstantTaskExecutorRule()
//
//    @get:Rule
//    val mainDispatcherRule = MainDispatcherRule()
//
////    private lateinit var preloginRepository: PreloginRepository
////    private lateinit var profileViewModel: ProfileViewModel
//
//
//    val expected = ProfileResponse(
//        code = 200,
//        message = "OK",
//        data = ProfileResponse.Data(
//            "Test",
//            "1d32ba79-e879-4425-a011-2da4281f1c1b-test.png"
//        )
//    )
//
////    @Before
////    fun setup() {
////        preloginRepository = Mockito.mock()
////        profileViewModel = ProfileViewModel(preloginRepository)
////    }
//
////    @Test
////    fun `test post profile user view model`() = runTest {
////        val userImage: File? = null
////        val userName = ""
////        val imagePart = userImage?.let {
////            MultipartBody.Part.createFormData(
////                "userImage",
////                userImage?.name,
////                it.asRequestBody("image/*".toMediaType())
////            )
////        }
////        val usernamePart = MultipartBody.Part.createFormData(
////            "userName",
////            userName
////        )
////        whenever(
////            preloginRepository.postProfile(imagePart, usernamePart)
////        ).thenReturn(MainStateEvent.Success(expected))
////        profileViewModel.postProfile(userImage, userName)
////        Assert.assertEquals(
////            MainStateEvent.Loading(true),
////            profileViewModel.stateProfile.getOrAwaitValue()
////        )
////        advanceTimeBy(1)
////        Assert.assertEquals(
////            MainStateEvent.Success(expected),
////            profileViewModel.stateProfile.getOrAwaitValue()
////        )
////    }
//
// }
