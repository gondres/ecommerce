package com.phincon.ecommerce.unit_test_view_model.register_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.core.network.model.RegisterResponse
import com.phincon.ecommerce.pages.prelogin.register.view_model.RegisterViewModel
import com.phincon.ecommerce.repository.service_repository.PreloginRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class RegisterViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var preloginRepository: PreloginRepository
    private lateinit var firebaseRemoteConfig: FirebaseRemoteConfig
    private lateinit var registerViewModel: RegisterViewModel

    val body = RegisterBody(
        email = "m@m.m",
        password = "12345678",
        firebaseToken = ""
    )

    val expected = RegisterResponse(

        code = 200,
        message = "OK",
        data = RegisterResponse.Data(
            "Test",
            200
        )
    )

    @Before
    fun setup() {
        preloginRepository = Mockito.mock()
        firebaseRemoteConfig = Mockito.mock()
        registerViewModel = RegisterViewModel(preloginRepository)
    }

    @Test
    fun `test register user view model`() = runTest {
        whenever(
            preloginRepository.register(body)
        ).thenReturn(MainStateEvent.Success(expected))
        registerViewModel.register(body)
        Assert.assertEquals(
            MainStateEvent.Loading(true),
            registerViewModel.stateRegister.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            MainStateEvent.Success(expected),
            registerViewModel.stateRegister.getOrAwaitValue()
        )
    }
}
