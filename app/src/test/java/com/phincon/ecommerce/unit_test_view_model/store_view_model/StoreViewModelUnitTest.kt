package com.phincon.ecommerce.unit_test_view_model.store_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.phincon.ecommerce.pages.main.store.view_model.StoreViewModel
import com.phincon.ecommerce.repository.service_repository.StoreRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class StoreViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var storeRepository: StoreRepository
    private lateinit var storeViewModel: StoreViewModel

    @Before
    fun setup() = runTest {
        storeRepository = Mockito.mock()
        storeViewModel = StoreViewModel(storeRepository)
    }

    @Test
    fun `resetChips view model`() = runTest {
        val result = storeViewModel.resetChips()
        assertEquals(Unit, result)
    }

    @Test
    fun `resetQuery view model`() = runTest {
        val result = storeViewModel.resetQuery()
        assertEquals(Unit, result)
    }

    @Test
    fun `searchQuery view model`() = runTest {
        val result = storeViewModel.searchQuery("")
        assertEquals(Unit, result)
    }

    @Test
    fun `setQuery view model`() = runTest {
        val result = storeViewModel.setQuery("", "", "", 0, 0)
        assertEquals(Unit, result)
    }


}
