package com.phincon.ecommerce.unit_test_view_model.transaction_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.phincon.ecommerce.core.network.model.TransactionHistoryResponse
import com.phincon.ecommerce.pages.main.transaction.view_model.TransactionViewModel
import com.phincon.ecommerce.repository.service_repository.CheckoutRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.MainStateEvent
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever

@RunWith(JUnit4::class)
class TransactionViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var checkoutRepository: CheckoutRepository
    private lateinit var transactionViewModel: TransactionViewModel

    val expected = TransactionHistoryResponse(
        200,
        "OK",
        data = listOf(
            TransactionHistoryResponse.Data(
                "8cad85b1-a28f-42d8-9479-72ce4b7f3c7d",
                true,
                "09 Jun 2023",
                "09:05",
                "Bank BCA",
                48998000,
                items = listOf(
                    TransactionHistoryResponse.Data.Item(
                        "bee98108-660c-4ac0-97d3-63cdc1492f53",
                        "RAM 16GB",
                        2
                    )
                ),
                4,
                "LGTM",
                "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray"
            )
        )
    )

    @Before
    fun setup() = runTest {
        checkoutRepository = Mockito.mock()
        transactionViewModel = TransactionViewModel(checkoutRepository)
    }

    @Test
    fun `get transaction list view model`() = runTest {
        whenever(
            checkoutRepository.getTransaction()
        ).thenReturn(MainStateEvent.Success(expected))
        transactionViewModel.getTransactionList()
        Assert.assertEquals(
            MainStateEvent.Loading(true),
            transactionViewModel.state.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            MainStateEvent.Success(expected),
            transactionViewModel.state.getOrAwaitValue()
        )
    }
}
