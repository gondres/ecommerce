package com.phincon.ecommerce.unit_test_view_model.wishlist_view_model

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.toWishlistEntity
import com.phincon.ecommerce.pages.main.wishlist.view_model.WishlistViewModel
import com.phincon.ecommerce.repository.database_repository.WishlistRepository
import com.phincon.ecommerce.utils.MainDispatcherRule
import com.phincon.ecommerce.utils.getOrAwaitValue
import com.phincon.ecommerce.utils.state_event.DatabaseStateEvent
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.advanceTimeBy
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito
import org.mockito.kotlin.whenever
import kotlin.test.assertEquals

@RunWith(JUnit4::class)
class WishlistViewModelUnitTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    private lateinit var wishlistRepository: WishlistRepository
    private lateinit var wishlistViewModel: WishlistViewModel

    val productDetail =
        ProductDetailResponse(
            code = 200,
            message = "OK",
            data = ProductDetailResponse.Data(
                "17b4714d-527a-4be2-84e2-e4c37c2b3292",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray",
                24499000,
                image = listOf(
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/4/6/0a49c399-cf6b-47f5-91c9-8cbd0b86462d.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/0cc3d06c-b09d-4294-8c3f-1c37e60631a6.jpg",
                    "https://images.tokopedia.net/img/cache/900/VqbcmM/2022/3/25/33a06657-9f88-4108-8676-7adafaa94921.jpg"
                ),
                "Asus",
                "ASUS ROG Strix G17 G713RM-R736H6G-O - Eclipse Gray [AMD Ryzen™ 7 6800H / NVIDIA® GeForce RTX™ 3060 / 8G*2 / 512GB / 17.3inch / WIN11 / OHS]\n\nCPU : AMD Ryzen™ 7 6800H Mobile Processor (8-core/16-thread, 20MB cache, up to 4.7 GHz max boost)\nGPU : NVIDIA® GeForce RTX™ 3060 Laptop GPU\nGraphics Memory : 6GB GDDR6\nDiscrete/Optimus : MUX Switch + Optimus\nTGP ROG Boost : 1752MHz* at 140W (1702MHz Boost Clock+50MHz OC, 115W+25W Dynamic Boost)\nPanel : 17.3-inch FHD (1920 x 1080) 16:9 360Hz IPS-level 300nits sRGB % 100.00%",
                "AsusStore",
                12,
                2,
                7,
                5,
                100,
                5.0,

                productVariant = listOf(
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 16GB",
                        0
                    ),
                    ProductDetailResponse.Data.ProductVariant(
                        "RAM 32GB",
                        1000000
                    )
                )

            )
        )

    @Before
    fun setup() = runTest {
        wishlistRepository = Mockito.mock()
        wishlistViewModel = WishlistViewModel(wishlistRepository)
    }

    @Test
    fun `test insert to wishlist view model`() = runTest {
        whenever(
            wishlistRepository.insertWishlist(productDetail.data)
        ).thenReturn(DatabaseStateEvent.Success(""))

        wishlistViewModel.insertToWishlist(productDetail.data)

        Assert.assertEquals(
            DatabaseStateEvent.Loading,
            wishlistViewModel.state.getOrAwaitValue()
        )
        advanceTimeBy(1)
        Assert.assertEquals(
            DatabaseStateEvent.Success(""),
            wishlistViewModel.state.getOrAwaitValue()
        )
    }

    @Test
    fun `fetch cart view model`() = runBlocking {
        whenever(wishlistRepository.getWishList()).thenReturn(flowOf(listOf(productDetail.data.toWishlistEntity())))
        val result = wishlistViewModel.fetchWishlist().first()
        assertEquals(listOf(productDetail.data.toWishlistEntity()), result)
    }

    @Test
    fun `get wishlist by id cart view model`() = runTest {
        whenever(wishlistRepository.getWishlistById("123")).thenReturn(flowOf(productDetail.data.toWishlistEntity()))
        val result = wishlistViewModel.getWishlistById("123").first()
        assertEquals(productDetail.data.toWishlistEntity(), result)
    }

    @Test
    fun `total data wishlist view model`() = runTest {
        val liveData = MutableLiveData<Int>()
        liveData.value = 1
        whenever(wishlistRepository.getCountOfWishlist()).thenReturn(liveData)
        val result = wishlistViewModel.totalData().getOrAwaitValue()
        assertEquals(1, result)
    }

    @Test
    fun `delete wishlist view model`() = runTest {
        whenever(wishlistRepository.deleteWishlist(productDetail.data.toWishlistEntity())).thenReturn(
            Unit
        )
        val result = wishlistViewModel.deleteWishlist(productDetail.data.toWishlistEntity())
        assertEquals(Unit, result)
    }
}
