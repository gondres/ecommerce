package com.phincon.ecommerce.core

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(
    private val prefManager: PrefManager
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

//        val token = runBlocking {
//            prefManager.getAccessToken()
//        }
        val token = prefManager.token
        val url = chain.request().url.encodedPath

        val keyHeader =
            if (url.contains(BaseString.register_url) ||
                url.contains(BaseString.login_url) ||
                url.contains(BaseString.refresh_url)
            )
                BaseString.header_api_key else BaseString.header_authotrization
        val valueHeader =
            if (url.contains(BaseString.register_url) ||
                url.contains(BaseString.login_url) ||
                url.contains(BaseString.refresh_url)
            )
                BaseString.api_key else "Bearer $token"

        Log.d("Auth Interceptor", "${token}")
        val request = chain.request().newBuilder()
        request.addHeader(keyHeader, valueHeader)
        return chain.proceed(request.build())
    }

}