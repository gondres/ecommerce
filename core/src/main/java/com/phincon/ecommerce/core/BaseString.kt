package com.phincon.ecommerce.core

object BaseString {
    const val base_url = "http://192.168.153.125:5000"
    const val api_key = "6f8856ed-9189-488f-9011-0ff4b6c08edc"
    const val header_api_key = "API_KEY"
    const val header_authotrization = "Authorization"

    const val login_url = "login"
    const val register_url = "register"
    const val profile_url = "profile"
    const val refresh_url = "refresh"
    const val product_url = "products"
    const val review_url = "review"
    const val search_url = "search"
    const val payment_url = "payment"
    const val fulfillment_url = "fulfillment"
    const val rating_url = "rating"
    const val transaction_url = "transaction"

    const val ecommerceSharedPref = "ECOMMERCE_SHARED_PREF"
    const val is_first_pref = "is_first"
    const val username_pref = "username"
    const val access_token_pref = "access_token"
    const val refresh_token_pref = "refresh_token"
    const val firebase_token_pref = "firebase_token"
    const val session_expired_pref = "session_expired"
    const val theme_application_pref = "theme_application"

}