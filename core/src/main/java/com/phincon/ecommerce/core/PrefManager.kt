package com.phincon.ecommerce.core

import android.content.Context
import android.content.SharedPreferences
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject

class PrefManager @Inject constructor(
    @ApplicationContext context: Context
) {

    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(BaseString.ecommerceSharedPref, Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor = sharedPreferences.edit()

    fun isFirstLogin(isFirst: Boolean) {
        editor.putBoolean(BaseString.is_first_pref, isFirst)
        editor.apply()
        editor.commit()
    }

    var token: String?
        get() = sharedPreferences.getString(BaseString.access_token_pref, null)
        set(value) = sharedPreferences.edit().putString(BaseString.access_token_pref, value).apply()


    fun getIsFirstLogin(): Boolean {
        return sharedPreferences.getBoolean(BaseString.is_first_pref, false)
    }

    fun setUsername(username: String) {
        editor.putString(BaseString.username_pref, username)
        editor.apply()
        editor.commit()
    }

    fun setAccessToken(token: String) {
        editor.putString(BaseString.access_token_pref, token)
        editor.apply()
        editor.commit()
    }

    fun setRefreshToken(token: String) {
        editor.putString(BaseString.refresh_token_pref, token)
        editor.apply()
        editor.commit()
    }

    fun setSessionExpired(session: Boolean) {
        editor.putBoolean(BaseString.session_expired_pref, session)
        editor.apply()
        editor.commit()
    }

    fun setFirebaseToken(token: String) {
        editor.putString(BaseString.firebase_token_pref, token)
        editor.apply()
        editor.commit()
    }

    fun setThemeApplication(darkMode: Boolean) {
        editor.putBoolean(BaseString.theme_application_pref, darkMode)
        editor.apply()
        editor.commit()
    }

    fun getUsername(): String {
        return sharedPreferences.getString(BaseString.username_pref, "")!!
    }

    fun getAccessToken(): String {
        return sharedPreferences.getString(BaseString.access_token_pref, "")!!
    }

    fun getRefreshToken(): String {
        return sharedPreferences.getString(BaseString.refresh_token_pref, "")!!
    }


    fun getThemeApplication(): Boolean {
        return sharedPreferences.getBoolean(BaseString.theme_application_pref, false)
    }

    fun deletePref() {
        editor.remove(BaseString.access_token_pref)
        editor.remove(BaseString.refresh_token_pref)
        editor.remove(BaseString.username_pref)
        editor.apply()
        editor.commit()
    }
}