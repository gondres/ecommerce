package com.phincon.ecommerce.core

import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.phincon.ecommerce.core.module.SessionManager
import com.phincon.ecommerce.core.network.model.RefreshBody
import com.phincon.ecommerce.core.network.model.RegisterResponse
import com.phincon.ecommerce.core.network.service.ApiService
import com.skydoves.sandwich.adapters.ApiResponseCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Inject

class TokenAuthenticator @Inject constructor(
    private val prefManager: PrefManager,
    private val authInterceptor: AuthInterceptor,
    private val chucker: ChuckerInterceptor,
    private val sessionManager: SessionManager
) : Authenticator {

    override fun authenticate(route: Route?, response: Response): Request? {
        synchronized(this) {
            return runBlocking {
                try {
                    val body = RefreshBody(token = prefManager.getRefreshToken())
                    val newToken = getNewToken(body)
                    if (newToken.isSuccessful && newToken.body() != null) {
                        newToken?.body()?.let {
                            prefManager.token = it.data?.accessToken
                            response.request.newBuilder()
                                .header("Authorization", "Bearer ${it.data?.refreshToken}")
                                .build()
                        }
                    } else {
                        if (newToken.code() == 401) {
                            sessionManager.setTokenExpired()
                            prefManager.setSessionExpired(true)
                            null
                        } else {
                            null
                        }
                    }
                } catch (e: HttpException) {
                    null
                }
            }
        }

    }


    private suspend fun getNewToken(refreshToken: RefreshBody?): retrofit2.Response<RegisterResponse> {

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .addInterceptor(chucker)
            .addInterceptor(authInterceptor)
            .build()
        val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
        val retrofit = Retrofit.Builder()
            .baseUrl(BaseString.base_url)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addCallAdapterFactory(ApiResponseCallAdapterFactory.create()).client(okHttpClient)
            .build()

        val service = retrofit.create(ApiService::class.java)

        return service.refreshTokenService(refreshToken!!)
    }
}