package com.phincon.ecommerce.core.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.phincon.ecommerce.core.database.dao.CartDao
import com.phincon.ecommerce.core.database.dao.NotificationDao
import com.phincon.ecommerce.core.database.dao.WishlistDao
import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import com.phincon.ecommerce.core.database.entity.WishlistEntity

@Database(
    entities = [CartEntity::class, WishlistEntity::class, NotificationEntity::class],
    version = 3,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun cartDao(): CartDao
    abstract fun wishlistDao(): WishlistDao

    abstract fun notifcationDao(): NotificationDao
}