package com.phincon.ecommerce.core.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.phincon.ecommerce.core.database.entity.CartEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface CartDao {
    @Query("SELECT * FROM cartentity")
    fun getCartList(): Flow<List<CartEntity>>

    @Insert
    suspend fun insertAll(vararg cartEntity: CartEntity)

    @Update
    suspend fun updateAll(vararg cartEntity: CartEntity)

    @Delete
    suspend fun delete(vararg cartEntity: CartEntity)

    @Query("UPDATE cartentity SET quantity = :quantity WHERE productId = :productId")
    suspend fun updateQuantity(productId: String, quantity: Int)

    @Query("SELECT COUNT(*) FROM CartEntity")
    fun countAllCartItems(): LiveData<Int>

    @Query("SELECT * FROM cartentity WHERE productId = :productId")
    fun getCartEntityById(productId: String): Flow<CartEntity?>
    @Query("SELECT * FROM cartentity WHERE productId = :productId")
    suspend fun getCartById(productId: String): CartEntity?

    @Query("SELECT SUM(productPrice * quantity) FROM CartEntity WHERE isChecked = 1")
    fun getTotalCheckedValue(): LiveData<Int>

}