package com.phincon.ecommerce.core.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.phincon.ecommerce.core.database.entity.NotificationEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface NotificationDao {
    @Query("SELECT * FROM notificationentity")
    fun getNotificationList(): Flow<List<NotificationEntity>>

    @Insert
    suspend fun insertAll(vararg notificationEntity: NotificationEntity)

    @Update
    suspend fun updateNotification(notification: NotificationEntity)

    @Query("SELECT COUNT(*) FROM NotificationEntity WHERE read = 0")
    fun countAllNotifItems(): LiveData<Int>

}