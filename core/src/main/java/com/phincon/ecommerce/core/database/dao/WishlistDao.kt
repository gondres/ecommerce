package com.phincon.ecommerce.core.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface WishlistDao {
    @Query("SELECT * FROM wishlistentity")
    fun getWishList(): Flow<List<WishlistEntity>>

    @Insert
    suspend fun insertAll(vararg wishlistEntity: WishlistEntity)

    @Update
    suspend fun updateAll(vararg wishlistEntity: WishlistEntity)

    @Delete
    suspend fun delete(vararg wishlistEntity: WishlistEntity)

    @Query("SELECT COUNT(*) FROM WishlistEntity")
    fun countAllWishlist(): LiveData<Int>

    @Query("SELECT * FROM wishlistentity WHERE productId = :productId")
    fun getWishlistEntityById(productId: String): Flow<WishlistEntity>


}