package com.phincon.ecommerce.core.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
data class NotificationEntity(
    @PrimaryKey(autoGenerate = true) val id: Long?,
    val title: String?,
    val body: String?,
    val image: String?,
    val type: String?,
    val date: String?,
    val time: String?,
    val read: Boolean?
)
