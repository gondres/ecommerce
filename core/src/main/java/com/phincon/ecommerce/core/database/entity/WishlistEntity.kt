package com.phincon.ecommerce.core.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class WishlistEntity(
    @PrimaryKey val productId: String,

    val productName: String,

    val productPrice: Int,

    val image: String,

    val brand: String,

    val description: String,

    val store: String,

    val sale: Int,

    val stock: Int,

    val totalRating: Int,

    val totalReview: Int,

    val totalSatisfaction: Int,

    val productRating: Double,

    val productVariant: String,

    var quantity: Int = 1,

    var isChecked: Boolean
)

fun WishlistEntity.toCartEntity(): CartEntity {
    return CartEntity(
        productId = productId,
        productName = productName,
        productPrice = productPrice,
        image = image,
        brand = brand,
        description = description,
        store = store,
        sale = sale,
        stock = stock,
        totalRating = totalRating,
        totalReview = totalReview,
        totalSatisfaction = totalSatisfaction,
        productRating = productRating,
        productVariant = productVariant,
        isChecked = false
    )
}