package com.phincon.ecommerce.core.module

import android.content.Context
import androidx.room.Room
import com.phincon.ecommerce.core.database.AppDatabase
import com.phincon.ecommerce.core.database.dao.CartDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabaseModule(@ApplicationContext context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java, "ecommerce_db"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideDatabaseDao(appDatabase: AppDatabase): CartDao {
        return appDatabase.cartDao()
    }

}