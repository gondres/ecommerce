package com.phincon.ecommerce.core.network.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class CheckoutModel(
    val productId: String,

    val productName: String,

    val productPrice: Int,

    val image: String,

    val brand: String,

    val description: String,

    val store: String,

    val sale: Int,

    val stock: Int,

    val totalRating: Int,

    val totalReview: Int,

    val totalSatisfaction: Int,

    val productRating: Double,

    val productVariant: String,

    var quantity: Int = 1,

    var isChecked: Boolean
) : Parcelable

