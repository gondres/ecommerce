package com.phincon.ecommerce.core.network.model

data class ExampleCartModel(
    val name: String? = null,
    var isChecked: Boolean = false
)
