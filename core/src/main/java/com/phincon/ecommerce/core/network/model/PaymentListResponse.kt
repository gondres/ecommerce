package com.phincon.ecommerce.core.network.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PaymentListResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: List<Data>
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "title")
        val title: String,
        @Json(name = "item")
        val item: List<Item>
    ) {
        @JsonClass(generateAdapter = true)
        data class Item(
            @Json(name = "label")
            val label: String,
            @Json(name = "image")
            val image: String,
            @Json(name = "status")
            val status: Boolean
        )
    }
}