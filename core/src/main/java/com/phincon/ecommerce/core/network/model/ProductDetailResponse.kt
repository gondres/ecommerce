package com.phincon.ecommerce.core.network.model


import com.phincon.ecommerce.core.database.entity.CartEntity
import com.phincon.ecommerce.core.database.entity.WishlistEntity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductDetailResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: Data
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "productId")
        val productId: String,
        @Json(name = "productName")
        val productName: String,
        @Json(name = "productPrice")
        val productPrice: Int,
        @Json(name = "image")
        val image: List<String>,
        @Json(name = "brand")
        val brand: String,
        @Json(name = "description")
        val description: String,
        @Json(name = "store")
        val store: String,
        @Json(name = "sale")
        val sale: Int,
        @Json(name = "stock")
        val stock: Int,
        @Json(name = "totalRating")
        val totalRating: Int,
        @Json(name = "totalReview")
        val totalReview: Int,
        @Json(name = "totalSatisfaction")
        val totalSatisfaction: Int,
        @Json(name = "productRating")
        val productRating: Double,
        @Json(name = "productVariant")
        val productVariant: List<ProductVariant>
    ) {
        @JsonClass(generateAdapter = true)
        data class ProductVariant(
            @Json(name = "variantName")
            var variantName: String,
            @Json(name = "variantPrice")
            val variantPrice: Int
        )
    }
}

fun ProductDetailResponse.Data.toCartEntity(): CartEntity {
    return CartEntity(
        productId = productId,
        productName = productName,
        productPrice = productPrice,
        image = image.firstOrNull().toString(),
        brand = brand,
        description = description,
        store = store,
        sale = sale,
        stock = stock,
        totalRating = totalRating,
        totalReview = totalReview,
        totalSatisfaction = totalSatisfaction,
        productRating = productRating,
        productVariant = productVariant.firstOrNull()!!.variantName,
        isChecked = false
    )
}

fun ProductDetailResponse.Data.toWishlistEntity(): WishlistEntity {
    return WishlistEntity(
        productId = productId,
        productName = productName,
        productPrice = productPrice,
        image = image.firstOrNull().toString(),
        brand = brand,
        description = description,
        store = store,
        sale = sale,
        stock = stock,
        totalRating = totalRating,
        totalReview = totalReview,
        totalSatisfaction = totalSatisfaction,
        productRating = productRating,
        productVariant = productVariant.firstOrNull()!!.variantName,
        isChecked = false
    )
}

fun ProductDetailResponse.Data.toCheckoutModel(): CheckoutModel {
    return CheckoutModel(
        productId = productId,
        productName = productName,
        productPrice = productPrice,
        image = image.firstOrNull().toString(),
        brand = brand,
        description = description,
        store = store,
        sale = sale,
        stock = stock,
        totalRating = totalRating,
        totalReview = totalReview,
        totalSatisfaction = totalSatisfaction,
        productRating = productRating,
        productVariant = productVariant.firstOrNull()!!.variantName,
        isChecked = false
    )
}