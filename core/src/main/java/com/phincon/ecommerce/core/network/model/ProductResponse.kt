package com.phincon.ecommerce.core.network.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProductResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: Data
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "itemsPerPage")
        val itemsPerPage: Int,
        @Json(name = "currentItemCount")
        val currentItemCount: Int,
        @Json(name = "pageIndex")
        val pageIndex: Int,
        @Json(name = "totalPages")
        val totalPages: Int,
        @Json(name = "items")
        val items: List<Item>
    ) {
        @JsonClass(generateAdapter = true)
        data class Item(
            @Json(name = "productId")
            val productId: String,
            @Json(name = "productName")
            val productName: String,
            @Json(name = "productPrice")
            val productPrice: Int,
            @Json(name = "image")
            val image: String,
            @Json(name = "brand")
            val brand: String,
            @Json(name = "store")
            val store: String,
            @Json(name = "sale")
            val sale: Int,
            @Json(name = "productRating")
            val productRating: Double
        )
    }
}