package com.phincon.ecommerce.core.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ProfileResponse(

    @Json(name = "code")
    val code: Int? = null,

    @Json(name = "data")
    val data: Data? = null,

    @Json(name = "message")
    val message: String? = null
) {
    @JsonClass(generateAdapter = true)
    data class Data(

        @Json(name = "userImage")
        val userImage: String? = null,

        @Json(name = "userName")
        val userName: String? = null
    )

}

