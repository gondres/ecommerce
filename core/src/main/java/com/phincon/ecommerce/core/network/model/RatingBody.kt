package com.phincon.ecommerce.core.network.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RatingBody(
    @Json(name = "invoiceId")
    val invoiceId: String,
    @Json(name = "rating")
    val rating: Int,
    @Json(name = "review")
    val review: String
)