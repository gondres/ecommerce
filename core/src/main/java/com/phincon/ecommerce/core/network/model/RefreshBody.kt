package com.phincon.ecommerce.core.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RefreshBody(

    @Json(name = "token")
    val token: String? = null
)
