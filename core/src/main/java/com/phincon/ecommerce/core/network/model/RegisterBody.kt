package com.phincon.ecommerce.core.network.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RegisterBody(

    @Json(name = "password")
    val password: String? = null,

    @Json(name = "firebaseToken")
    val firebaseToken: String? = null,

    @Json(name = "email")
    val email: String? = null
)

