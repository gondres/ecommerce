package com.phincon.ecommerce.core.network.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ReviewProductResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: List<Data>
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "userName")
        val userName: String,
        @Json(name = "userImage")
        val userImage: String,
        @Json(name = "userRating")
        val userRating: Int,
        @Json(name = "userReview")
        val userReview: String
    )
}