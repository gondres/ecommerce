package com.phincon.ecommerce.core.network.model

data class StoreFilterBody(

    val search: String? = null,
    val brand: String? = null,
    val highest: Int? = null,
    val lowest: Int? = null,
    val sort: String? = null,

    )
