package com.phincon.ecommerce.core.network.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TransactionBody(
    @Json(name = "payment")
    val payment: String,
    @Json(name = "items")
    val items: List<Item>
) {
    @JsonClass(generateAdapter = true)
    data class Item(
        @Json(name = "productId")
        val productId: String,
        @Json(name = "variantName")
        val variantName: String,
        @Json(name = "quantity")
        val quantity: Int
    )
}