package com.phincon.ecommerce.core.network.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TransactionModel(
    var code: Int?,

    var message: String?,

    var `data`: Item
) : Parcelable

@Parcelize
data class Item(

    val invoiceId: String,

    val status: Boolean,

    val date: String,

    val time: String,

    val payment: String,

    val total: Int,

    val rating: Int?,

    val review: String?
) : Parcelable