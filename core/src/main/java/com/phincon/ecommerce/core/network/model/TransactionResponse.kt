package com.phincon.ecommerce.core.network.model


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TransactionResponse(
    @Json(name = "code")
    val code: Int,
    @Json(name = "message")
    val message: String,
    @Json(name = "data")
    val `data`: Data
) {
    @JsonClass(generateAdapter = true)
    data class Data(
        @Json(name = "invoiceId")
        val invoiceId: String,
        @Json(name = "status")
        val status: Boolean,
        @Json(name = "date")
        val date: String,
        @Json(name = "time")
        val time: String,
        @Json(name = "payment")
        val payment: String,
        @Json(name = "total")
        val total: Int
    )
}

fun TransactionResponse.Data.toTransactionData(): Item {
    return Item(
        invoiceId, status, date, time, payment, total, null, null
    )
}