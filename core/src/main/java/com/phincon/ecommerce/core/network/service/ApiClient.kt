package com.phincon.ecommerce.core.network.service

import javax.inject.Inject

class ApiClient @Inject constructor(private val apiService: ApiService) {


    // Main Client

    suspend fun searchProducts(search: String?) = apiService.searchProduct(search)

    suspend fun getReviewProduct(idProduct: String?) = apiService.getReviewProduct(idProduct)


}