package com.phincon.ecommerce.core.network.service

import com.phincon.ecommerce.core.BaseString
import com.phincon.ecommerce.core.network.model.LoginResponse
import com.phincon.ecommerce.core.network.model.ProductDetailResponse
import com.phincon.ecommerce.core.network.model.ProductResponse
import com.phincon.ecommerce.core.network.model.ProfileResponse
import com.phincon.ecommerce.core.network.model.RatingBody
import com.phincon.ecommerce.core.network.model.RatingResponse
import com.phincon.ecommerce.core.network.model.RefreshBody
import com.phincon.ecommerce.core.network.model.RegisterBody
import com.phincon.ecommerce.core.network.model.RegisterResponse
import com.phincon.ecommerce.core.network.model.ReviewProductResponse
import com.phincon.ecommerce.core.network.model.SearchResponse
import com.phincon.ecommerce.core.network.model.TransactionBody
import com.phincon.ecommerce.core.network.model.TransactionHistoryResponse
import com.phincon.ecommerce.core.network.model.TransactionResponse
import com.skydoves.sandwich.ApiResponse
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {


    @POST(BaseString.register_url)
    suspend fun register(
        @Body body: RegisterBody
    ): Response<RegisterResponse>

    @POST(BaseString.login_url)
    suspend fun login(
        @Body body: RegisterBody
    ): Response<LoginResponse>

    @POST(BaseString.refresh_url)
    suspend fun refreshTokenService(
        @Body refreshToken: RefreshBody
    ): Response<RegisterResponse>


    @Multipart
    @POST(BaseString.profile_url)
    suspend fun postProfile(
        @Part userImage: MultipartBody.Part?,
        @Part userName: MultipartBody.Part
    ): Response<ProfileResponse>


    @POST(BaseString.product_url)
    suspend fun getProductsPaging(
        @Query("search") search: String?,
        @Query("brand") brand: String?,
        @Query("lowest") lowest: Int?,
        @Query("highest") highest: Int?,
        @Query("sort") sort: String?,
        @Query("limit") limit: Int?,
        @Query("page") page: Int?,
    ): Response<ProductResponse>

    @POST(BaseString.search_url)
    suspend fun searchProduct(
        @Query("query") query: String?
    ): ApiResponse<SearchResponse>

    @GET("${BaseString.product_url}/{id}")
    suspend fun getDetailProduct(
        @Path("id") idProduct: String?
    ): Response<ProductDetailResponse>

    @GET("${BaseString.review_url}/{id}")
    suspend fun getReviewProduct(
        @Path("id") idProduct: String?
    ): Response<ReviewProductResponse>

    @GET("${BaseString.payment_url}")
    suspend fun getPaymentList(): Response<com.phincon.ecommerce.core.network.model.PaymentListResponse>

    @POST("${BaseString.fulfillment_url}")
    suspend fun postTransaction(@Body body: TransactionBody): Response<TransactionResponse>

    @POST("${BaseString.rating_url}")
    suspend fun postRatingTransaction(@Body body: RatingBody): Response<RatingResponse>

    @GET("${BaseString.transaction_url}")
    suspend fun getTransaction(): Response<TransactionHistoryResponse>
}